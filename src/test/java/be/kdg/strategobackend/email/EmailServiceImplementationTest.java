package be.kdg.strategobackend.email;

import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.notification.EmailSender;
import be.kdg.strategobackend.rest.jwt.JwtTokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailServiceImplementationTest {
    @Autowired
    private EmailSender emailSender;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    private String email;
    private int expirationInMillis;

    @Before
    public void setUp() {
        email = "jelle.vanderdonck@student.kdg.be";
        expirationInMillis = 10000000;
    }

    @Test
    public void testEmails() {
        Player player = new Player();
        player.setPlayerName("JumperSnipera");
        player.setEmail("haha@lol.com");
        emailSender.sendEmailConfirmation(email, jwtTokenProvider.generateToken(email, expirationInMillis));
        emailSender.sendPasswordReset(email, jwtTokenProvider.generateToken(email, expirationInMillis));
        emailSender.sendGameInvite(player, "Joske",UUID.randomUUID());
    }
}
package be.kdg.strategobackend;

import be.kdg.strategobackend.model.player.NotificationSettings;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.model.player.PlayerStats;
import be.kdg.strategobackend.service.NotificationSettingsService;
import be.kdg.strategobackend.service.PlayerService;
import be.kdg.strategobackend.service.PlayerStatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class RegisterHelper {
    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayerStatsService playerStatsService;
    @Autowired
    private NotificationSettingsService notificationSettingsService;

    public Player register(String playerName) throws Exception {
        try {
            return playerService.loadPlayerByPlayerName(playerName);
        } catch (UsernameNotFoundException ignored) {
            Player player = new Player();
            player.setPlayerName(playerName);
            player.setEmail(playerName + "@address.com");
            player.setPassword("password");
            player.setEmailConfirmed(true);
            PlayerStats playerStats = new PlayerStats();
            player.setFriends(new HashSet<>());
            playerStats = playerStatsService.save(playerStats);
            player.setPlayerStats(playerStats);
            NotificationSettings notificationSettings = new NotificationSettings();
            notificationSettings.setTurnExpired(true);
            notificationSettings.setTurnPlayed(true);
            notificationSettings.setYourTurn(true);
            notificationSettings = notificationSettingsService.save(notificationSettings);
            player.setNotificationSettings(notificationSettings);
            playerService.register(player);
            return playerService.loadPlayerByPlayerName(playerName);
        }
    }
}

package be.kdg.strategobackend.utility;

import be.kdg.strategobackend.model.history.GameTurn;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class TurnsToEventsConverterTest {
    @Test
    public void testSortingMechanism() {
        LocalDateTime date = LocalDateTime.now();
        List<GameTurn> gameTurns = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            date = date.minusSeconds(10);
            gameTurns.add(new GameTurn(date));
        }

        Collections.shuffle(gameTurns);
        Collections.sort(gameTurns);

        for (int i = 0; i < gameTurns.size(); i++) {
            if (i != gameTurns.size() - 1)
                Assert.assertTrue(gameTurns.get(i).getTime().isBefore(gameTurns.get(i + 1).getTime()));
        }
    }
}
package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.rest.dto.other.LoginDTO;
import be.kdg.strategobackend.rest.dto.other.RegisterDTO;
import be.kdg.strategobackend.rest.dto.other.TokenDTO;
import be.kdg.strategobackend.rest.jwt.JwtTokenProvider;
import be.kdg.strategobackend.service.PlayerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class AuthenticationControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private PlayerService playerService;

    private RegisterDTO registerDTO;
    private String convertedRegisterDTO;
    private String convertedLoginWithNameDTO;
    private String convertedLoginWithEmailDTO;

    @Before
    public void setUp() throws Exception {
        registerDTO = new RegisterDTO("Jelle", "jelle.vanderdonck@student.kdg.be", "jelle");
        convertedRegisterDTO = objectMapper.writeValueAsString(registerDTO);
        LoginDTO loginWithNameDTO = new LoginDTO(registerDTO.getPlayerName(), registerDTO.getPassword());
        convertedLoginWithNameDTO = objectMapper.writeValueAsString(loginWithNameDTO);
        LoginDTO loginWithEmailDTO = new LoginDTO(registerDTO.getEmail(), registerDTO.getPassword());
        convertedLoginWithEmailDTO = objectMapper.writeValueAsString(loginWithEmailDTO);
    }

    @Test
    public void testRegisterAndLogIn() throws Exception {
        register();
        confirmEmail();
        TokenDTO loginWithNameTokenDTO = objectMapper.readValue(logIn(convertedLoginWithNameDTO, HttpStatus.OK), TokenDTO.class);
        TokenDTO loginWithEmailTokenDTO = objectMapper.readValue(logIn(convertedLoginWithEmailDTO, HttpStatus.OK), TokenDTO.class);
        Assert.assertTrue(jwtTokenProvider.validateToken(loginWithNameTokenDTO.getToken()));
        Assert.assertTrue(jwtTokenProvider.validateToken(loginWithEmailTokenDTO.getToken()));
    }

    /*
    // TODO: 10/02/2019 EMAIL CONFIRMATION TEST
    @Test
    public void testRegisterAndLogInWithoutConfirmation() throws Exception {
        register();
        logIn(convertedLoginWithNameDTO, HttpStatus.FORBIDDEN);
    }
    */

    private void register() throws Exception {
        mockMvc.perform(post("/api/authentication/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedRegisterDTO))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("Jelle")));
    }

    private String logIn(String jsonString, HttpStatus expectedStatus) throws Exception {
        if (expectedStatus.equals(HttpStatus.OK)) {
            return mockMvc.perform(post("/api/authentication/login")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(jsonString))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();
        } else if (expectedStatus.equals(HttpStatus.UNAUTHORIZED)) {
            return mockMvc.perform(post("/api/authentication/login")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(jsonString))
                    .andExpect(status().isUnauthorized())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();
        } else if (expectedStatus.equals(HttpStatus.FORBIDDEN)) {
            return mockMvc.perform(post("/api/authentication/login")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(jsonString))
                    .andExpect(status().isForbidden())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();
        } else
            throw new HttpServerErrorException(expectedStatus);
    }

    private void confirmEmail() {
        playerService.confirmEmail(registerDTO.getPlayerName());
    }
}
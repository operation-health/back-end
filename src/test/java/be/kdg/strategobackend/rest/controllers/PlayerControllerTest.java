package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.rest.dto.other.*;
import be.kdg.strategobackend.rest.jwt.JwtTokenProvider;
import be.kdg.strategobackend.service.PlayerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpServerErrorException;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class PlayerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private RegisterHelper registerHelper;

    private RegisterDTO registerDTO;
    private String convertedRegisterDTO;
    private String convertedLoginWithNameDTO;
    private String convertedLoginWithEmailDTO;

    @Before
    public void setUp() throws Exception {
        registerDTO = new RegisterDTO("Jelle", "jelle.vanderdonck@student.kdg.be", "jelle");
        convertedRegisterDTO = objectMapper.writeValueAsString(registerDTO);
        LoginDTO loginWithNameDTO = new LoginDTO(registerDTO.getPlayerName(), registerDTO.getPassword());
        convertedLoginWithNameDTO = objectMapper.writeValueAsString(loginWithNameDTO);
        LoginDTO loginWithEmailDTO = new LoginDTO(registerDTO.getEmail(), registerDTO.getPassword());
        convertedLoginWithEmailDTO = objectMapper.writeValueAsString(loginWithEmailDTO);
        registerHelper.register("Joske");
        registerHelper.register("Joske2");

    }

    @Test
    public void testChangePassword() throws Exception {
        register();
        confirmEmail();
        ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO("newpassword");
        String convertedChangePasswordDTO = objectMapper.writeValueAsString(changePasswordDTO);
        String token = objectMapper.readValue(logIn(convertedLoginWithNameDTO, HttpStatus.OK), TokenDTO.class).getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + token);
        mockMvc.perform(patch("/api/player")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .headers(httpHeaders)
                .content(convertedChangePasswordDTO))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Jelle")));
        LoginDTO afterChangeLoginDTO = new LoginDTO(registerDTO.getPlayerName(), changePasswordDTO.getPassword());
        String convertedAfterChangeLoginDTO = objectMapper.writeValueAsString(afterChangeLoginDTO);
        Assert.assertTrue(jwtTokenProvider.validateToken(objectMapper.readValue(logIn(convertedAfterChangeLoginDTO, HttpStatus.OK), TokenDTO.class).getToken()));
    }

    @Test
    @WithMockUser("Joske")
    public void testDeleteFriend() throws Exception {
        mockMvc.perform(patch("/api/player/friends/Joske2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

        mockMvc.perform(patch("/api/player/friends/delete/Joske2")
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());

        mockMvc.perform(patch("/api/player/friends/NotExistingPlayerName")
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isNotFound());
    }

    @Test
    public void testEditAccount() throws Exception {
        AccountManagementDTO accountManagementDTO = new AccountManagementDTO("jellevdd@telenet.be", "newpassword");
        String convertedAccountManagementDTO = objectMapper.writeValueAsString(accountManagementDTO);
        register();
        confirmEmail();
        String token = objectMapper.readValue(logIn(convertedLoginWithNameDTO, HttpStatus.OK), TokenDTO.class).getToken();
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer " + token);
        mockMvc.perform(get("/api/player")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .headers(httpHeaders))
                .andExpect(content().string(containsString("Jelle")));
        mockMvc.perform(get("/api/player/email")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .headers(httpHeaders))
                .andExpect(content().string(containsString("jelle.vanderdonck@student.kdg.be")));
        mockMvc.perform(put("/api/player")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .headers(httpHeaders).content(convertedAccountManagementDTO))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Jelle")));
        mockMvc.perform(get("/api/player")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .headers(httpHeaders))
                .andExpect(content().string(containsString("Jelle")));
    }

    private void register() throws Exception {
        mockMvc.perform(post("/api/authentication/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(convertedRegisterDTO))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("Jelle")));
    }

    private String logIn(String jsonString, HttpStatus expectedStatus) throws Exception {
        if (expectedStatus.equals(HttpStatus.OK)) {
            return mockMvc.perform(post("/api/authentication/login")
                    .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonString))
                    .andExpect(status().isOk())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();
        } else if (expectedStatus.equals(HttpStatus.UNAUTHORIZED)) {
            return mockMvc.perform(post("/api/authentication/login")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(jsonString))
                    .andExpect(status().isUnauthorized())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();
        } else if (expectedStatus.equals(HttpStatus.FORBIDDEN)) {
            return mockMvc.perform(post("/api/authentication/login")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(jsonString))
                    .andExpect(status().isForbidden())
                    .andReturn()
                    .getResponse()
                    .getContentAsString();
        } else
            throw new HttpServerErrorException(expectedStatus);
    }

    private void confirmEmail() {
        Player player = playerService.loadPlayerByPlayerName(registerDTO.getPlayerName());
        player.setEmailConfirmed(true);
        playerService.confirmEmail(registerDTO.getPlayerName());
    }
}

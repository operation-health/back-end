package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.rest.dto.shared.UpdatePrivacyDTO;
import be.kdg.strategobackend.service.PlayerStatsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class PlayerStatsControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private PlayerStatsService playerStatsService;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private RegisterHelper registerHelper;

    private Player joske;

    @Before
    public void setUp() throws Exception {
        joske = registerHelper.register("Joske");
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testGetStatsByPlayerName() throws Exception {
        mockMvc.perform(get("/api/stats/" + joske.getPlayerName())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"gamesPlayed\":0")))
                .andExpect(content().string(containsString("\"gamesWon\":0")));

        playerStatsService.updatePlayerStats(joske.getPlayerName(), 5, 10);

        mockMvc.perform(get("/api/stats/" + joske.getPlayerName())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"gamesPlayed\":10")))
                .andExpect(content().string(containsString("\"gamesWon\":5")));
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testUpdatePlayerStats() throws Exception {
        UpdatePrivacyDTO updatePrivacyDTO = new UpdatePrivacyDTO(false);

        mockMvc.perform(patch("/api/stats")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(updatePrivacyDTO)))
                .andExpect(status().isOk());

        mockMvc.perform(get("/api/stats/" + joske.getPlayerName())
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"publicAccess\":false")));
    }
}
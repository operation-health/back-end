package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.rest.dto.game.MovePawnDTO;
import be.kdg.strategobackend.service.*;
import be.kdg.strategobackend.service.exception.BoardTileException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class GameControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private GameService gameService;
    @Autowired
    private GamePlayerService gamePlayerService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private BoardTileService boardTileService;
    @Autowired
    private RegisterHelper registerHelper;

    private Player joske;
    private Player joske2;
    private Player joske3;

    @Before
    public void setUp() throws Exception {
        joske = registerHelper.register("Joske");
        joske2 = registerHelper.register("Joske2");
        joske3 = registerHelper.register("Joske3");
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testStartPreparingPhase() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        mockMvc.perform(put("/api/game")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        Game game = gameService.loadCurrentGameByPlayerName(joske.getPlayerName());

        Assert.assertEquals(game.getBoard().getBoardTiles().size(), boardService.load(game.getBoard().getId()).getBoardTiles().size());
        game.getGamePlayers().forEach(gamePlayer -> Assert.assertEquals(24, gamePlayer.getPawns().size()));
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testStartGameWithUnreadyPlayers() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);

        mockMvc.perform(put("/api/game")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testOrganisePawn() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        mockMvc.perform(put("/api/game")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        Game game = gameService.loadCurrentGameByPlayerName(joske.getPlayerName());
        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        BoardTile boardTile = game.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() == gamePlayer.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn pawn = gamePlayer.getPawns().get(0);
        MovePawnDTO movePawnDTO = new MovePawnDTO(pawn.getId(), boardTile.getTileId());
        String convertedMovePawnDTO = objectMapper.writeValueAsString(movePawnDTO);

        mockMvc.perform(put("/api/game/organise")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedMovePawnDTO))
                .andExpect(status().isOk());

        boardTile = boardTileService.loadByBoardIdAndTileId(game.getBoard().getId(), boardTile.getTileId());

        Assert.assertEquals(pawn.getBoardTile().getTileId(), boardTile.getTileId());
        Assert.assertEquals(pawn.getBoardTile().getId(), boardTile.getId());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testOrganisePawnOnSameTile() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        mockMvc.perform(put("/api/game")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        Game game = gameService.loadCurrentGameByPlayerName(joske.getPlayerName());
        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        BoardTile boardTile = game.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() == gamePlayer.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn pawn = gamePlayer.getPawns().get(0);
        MovePawnDTO movePawnDTO = new MovePawnDTO(pawn.getId(), boardTile.getTileId());
        String convertedMovePawnDTO = objectMapper.writeValueAsString(movePawnDTO);

        mockMvc.perform(put("/api/game/organise")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedMovePawnDTO))
                .andExpect(status().isOk());

        pawn = gamePlayer.getPawns().get(1);
        movePawnDTO = new MovePawnDTO(pawn.getId(), boardTile.getTileId());
        convertedMovePawnDTO = objectMapper.writeValueAsString(movePawnDTO);

        mockMvc.perform(put("/api/game/organise")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedMovePawnDTO))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testOrganisePawnAndRemovePawn() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        mockMvc.perform(put("/api/game")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        Game game = gameService.loadCurrentGameByPlayerName(joske.getPlayerName());
        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        BoardTile boardTile = game.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() == gamePlayer.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn pawn = gamePlayer.getPawns().get(0);
        MovePawnDTO movePawnDTO = new MovePawnDTO(pawn.getId(), boardTile.getTileId());
        String convertedMovePawnDTO = objectMapper.writeValueAsString(movePawnDTO);

        mockMvc.perform(put("/api/game/organise")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedMovePawnDTO))
                .andExpect(status().isOk());

        movePawnDTO = new MovePawnDTO(pawn.getId(), -1);
        convertedMovePawnDTO = objectMapper.writeValueAsString(movePawnDTO);

        mockMvc.perform(put("/api/game/organise")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedMovePawnDTO))
                .andExpect(status().isOk());

        Assert.assertNull(pawn.getBoardTile());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testOrganisePawnOnOtherTeamTile() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        mockMvc.perform(put("/api/game")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        Game game = gameService.loadCurrentGameByPlayerName(joske.getPlayerName());
        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        BoardTile boardTile = game.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() != gamePlayer.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn pawn = gamePlayer.getPawns().get(0);
        MovePawnDTO movePawnDTO = new MovePawnDTO(pawn.getId(), boardTile.getTileId());
        String convertedMovePawnDTO = objectMapper.writeValueAsString(movePawnDTO);

        mockMvc.perform(put("/api/game/organise")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedMovePawnDTO))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testOrganisePawnFromOtherPlayer() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        mockMvc.perform(put("/api/game")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        Game game = gameService.loadCurrentGameByPlayerName(joske.getPlayerName());
        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(joske2.getPlayerName());
        BoardTile boardTile = game.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() != gamePlayer.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn pawn = gamePlayer.getPawns().get(0);
        MovePawnDTO movePawnDTO = new MovePawnDTO(pawn.getId(), boardTile.getTileId());
        String convertedMovePawnDTO = objectMapper.writeValueAsString(movePawnDTO);

        mockMvc.perform(put("/api/game/organise")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedMovePawnDTO))
                .andExpect(status().isForbidden());
    }
}
package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.service.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class BoardControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private RegisterHelper registerHelper;

    @Before
    public void setUp() throws Exception {
        registerHelper.register("Joske");
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testGetBoards() throws Exception {
        String result = mockMvc.perform(get("/api/board/boards")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        Assert.assertTrue(result.contains("\"type\":\"PASSENDALE\""));
        Assert.assertTrue(result.contains("\"playerCount\":3"));
        Assert.assertTrue(result.contains("\"type\":\"WATERLOO\""));
    }
}
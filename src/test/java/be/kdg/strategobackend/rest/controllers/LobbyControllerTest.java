package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.game.GameRules;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.rest.dto.lobby.DeletePlayerDTO;
import be.kdg.strategobackend.rest.dto.lobby.UpdateBoardDTO;
import be.kdg.strategobackend.rest.dto.other.PlayerNameDTO;
import be.kdg.strategobackend.rest.dto.shared.*;
import be.kdg.strategobackend.service.GamePlayerService;
import be.kdg.strategobackend.service.GameRulesService;
import be.kdg.strategobackend.service.LobbyService;
import be.kdg.strategobackend.service.PlayerService;
import be.kdg.strategobackend.service.exception.GameException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class LobbyControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private GamePlayerService gamePlayerService;
    @Autowired
    private RegisterHelper registerHelper;
    @Autowired
    private GameRulesService gameRulesService;

    private Player joske;
    private Player joske2;
    private Player joske3;
    private Player joske4;

    @Before
    public void setUp() throws Exception {
        joske = registerHelper.register("Joske");
        joske2 = registerHelper.register("Joske2");
        joske3 = registerHelper.register("Joske3");
        joske4 = registerHelper.register("Joske4");
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testCreateLobby() throws Exception {
        mockMvc.perform(post("/api/lobby")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(content().string(containsString("\"playerName\":\"" + joske.getPlayerName() + "\"")));
    }

    @Test
    @WithMockUser(username = "Joske2")
    public void testInvitePlayerAsNotHost() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());
        String inviteDTOJson = objectMapper.writeValueAsString(new PlayerNameDTO(joske3.getPlayerName()));

        mockMvc.perform(post("/api/lobby/invite")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(inviteDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testInviteToFullLobby() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());
        lobbyService.acceptInvite(joske3.getPlayerName(), lobby.getInviteUUID());

        String inviteDTOJson = objectMapper.writeValueAsString(new PlayerNameDTO(joske4.getPlayerName()));
        mockMvc.perform(post("/api/lobby/invite")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(inviteDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "Joske2")
    public void testJoinPublic() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        mockMvc.perform(get("/api/lobby/join")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        lobby = lobbyService.load(lobby.getId());
        Assert.assertEquals(2, lobby.getGamePlayers().stream().filter(gp -> !gp.isComputer()).count());
    }

    @Test
    @WithMockUser(username = "Joske2")
    public void testJoinPublicWithoutGames() throws Exception {
        mockMvc.perform(get("/api/lobby/join")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "Joske2")
    public void testJoinPublicWithOnlyPrivateGames() throws Exception {
        lobbyService.createLobby("Joske");
        lobbyService.updatePrivacy(joske.getPlayerName(), false);

        mockMvc.perform(get("/api/lobby/join")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "Joske2")
    public void testJoinPublicWithPublicAndPrivateGames() throws Exception {
        lobbyService.createLobby(joske.getPlayerName());
        lobbyService.createLobby(joske3.getPlayerName());
        lobbyService.createLobby(joske4.getPlayerName());
        lobbyService.updatePrivacy(joske.getPlayerName(), false);

        mockMvc.perform(get("/api/lobby/join")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("\"playerName\":\"" + joske2.getPlayerName() + "\"")));
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testUpdatePrivacy() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        UpdatePrivacyDTO updatePrivacyDTO = new UpdatePrivacyDTO(false);
        String convertedUpdatePrivacyDTO = objectMapper.writeValueAsString(updatePrivacyDTO);

        mockMvc.perform(patch("/api/lobby/privacy")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedUpdatePrivacyDTO))
                .andExpect(status().isOk());

        lobby = lobbyService.load(lobby.getId());
        Assert.assertFalse(lobby.isPublicAccess());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testUpdateBoard() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        UpdateBoardDTO updateBoardDTO = new UpdateBoardDTO("PASSENDALE");
        String convertedUpdateBoardDTO = objectMapper.writeValueAsString(updateBoardDTO);

        mockMvc.perform(patch("/api/lobby/board")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedUpdateBoardDTO))
                .andExpect(status().isOk());

        lobby = lobbyService.load(lobby.getId());
        Assert.assertEquals(lobby.getBoard().getType(), updateBoardDTO.getBoardType());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testUpdatePlayerStatus() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        UpdatePlayerReadyDTO updatePlayerReadyDTO = new UpdatePlayerReadyDTO(true);
        String convertedUpdatePlayerStatusDTO = objectMapper.writeValueAsString(updatePlayerReadyDTO);

        mockMvc.perform(patch("/api/lobby/ready")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedUpdatePlayerStatusDTO))
                .andExpect(status().isOk());

        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        Assert.assertTrue(gamePlayer.isReady());
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testUpdatePreparationTimer() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        PreparationTimerDTO preparationTimerDTO = new PreparationTimerDTO(100);
        String convertedPreparationTimerDTO = objectMapper.writeValueAsString(preparationTimerDTO);

        mockMvc.perform(put("/api/lobby/preparationtimer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedPreparationTimerDTO))
                .andExpect(status().isOk());

        GameRules gameRules = gameRulesService.load(lobby.getGameRules().getId());
        Assert.assertEquals(gameRules.getPreparationTimer(), 100);
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testUpdateTurnTimer() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        TurnTimerDTO turnTimerDTO = new TurnTimerDTO(100);
        String convertedTurnTimerDTO = objectMapper.writeValueAsString(turnTimerDTO);

        mockMvc.perform(put("/api/lobby/turntimer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedTurnTimerDTO))
                .andExpect(status().isOk());

        GameRules gameRules = gameRulesService.load(lobby.getGameRules().getId());
        Assert.assertEquals(gameRules.getTurnTimer(), 100);
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testUpdateAfkTimer() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        AfkTimerDTO afkTimerDTO = new AfkTimerDTO(100);
        String convertedAfkTimerDTO = objectMapper.writeValueAsString(afkTimerDTO);

        mockMvc.perform(put("/api/lobby/afktimer")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(convertedAfkTimerDTO))
                .andExpect(status().isOk());

        GameRules gameRules = gameRulesService.load(lobby.getGameRules().getId());
        Assert.assertEquals(gameRules.getAfkTimer(), 100);
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testLeaveLobbyWhileAlone() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        mockMvc.perform(delete("/api/lobby/leave")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
        try {
            lobbyService.load(lobby.getId());
        } catch (GameException e) {
            return; //The game shouldn't exist
        }
        fail("The game still exists");
    }

    @Test
    @WithMockUser(username = "Joske")
    public void testKickPlayer() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());
        lobbyService.acceptInvite(joske3.getPlayerName(), lobby.getInviteUUID());

        DeletePlayerDTO deletePlayerDTO = new DeletePlayerDTO(joske2.getPlayerName());
        String deletePlayerDTOJson = objectMapper.writeValueAsString(deletePlayerDTO);

        mockMvc.perform(delete("/api/lobby")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(deletePlayerDTOJson))
                .andExpect(status().isOk());

        lobby = lobbyService.load(lobby.getId());
        Assert.assertEquals(2, lobby.getGamePlayers().stream().filter(gp -> !gp.isComputer()).count());
    }

    @Test
    @WithMockUser(username = "Joske2")
    public void testKickPlayerAsNotHost() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());
        lobbyService.acceptInvite(joske3.getPlayerName(), lobby.getInviteUUID());

        DeletePlayerDTO deletePlayerDTO = new DeletePlayerDTO(joske3.getPlayerName());
        String deletePlayerDTOJson = objectMapper.writeValueAsString(deletePlayerDTO);

        mockMvc.perform(delete("/api/lobby")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(deletePlayerDTOJson))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "Joske2")
    public void testLeaveGame() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());
        lobbyService.acceptInvite(joske3.getPlayerName(), lobby.getInviteUUID());

        mockMvc.perform(delete("/api/lobby/leave")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        lobby = lobbyService.load(lobby.getId());
        Assert.assertEquals(2, lobby.getGamePlayers().stream().filter(gp -> !gp.isComputer()).count());
    }

    @Test(expected = GameException.class)
    @WithMockUser(username = "Joske")
    public void testLeaveGameAsLastNonAI() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.fillWithComputers(lobby);

        mockMvc.perform(delete("/api/lobby/leave")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        lobbyService.load(lobby.getId());
    }
}

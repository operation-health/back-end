package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.player.NotificationSettings;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.rest.dto.other.NotificationSettingsDTO;
import be.kdg.strategobackend.service.NotificationSettingsService;
import be.kdg.strategobackend.service.PlayerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class NotificationSettingsControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private RegisterHelper registerHelper;


    private Player joske;


    @Before
    public void setUp() throws Exception {
        joske = registerHelper.register("Joske");
    }

    @Test
    @WithMockUser("Joske")
    public void changeNotificationSettings() throws Exception {
        NotificationSettings notificationSettings = playerService.loadPlayerByPlayerName(joske.getPlayerName()).getNotificationSettings();
        boolean isGameInvite = notificationSettings.isGameInvite();
        NotificationSettingsDTO notificationSettingsDTO = new NotificationSettingsDTO(!isGameInvite, true, true, true, true);

        String result = mockMvc.perform(put("/api/notificationsettings")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(objectMapper.writeValueAsString(notificationSettingsDTO)))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        NotificationSettingsDTO updatedNotificationSettings = objectMapper.readValue(result, NotificationSettingsDTO.class);
        Assert.assertEquals(updatedNotificationSettings.isGameInvite(), !isGameInvite);

    }

    @Test
    @WithMockUser("Joske")
    public void getNotificationSettings() throws Exception {
        String result = mockMvc.perform(get("/api/notificationsettings")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();
        Assert.assertTrue(result.contains("turnExpired"));
    }
}
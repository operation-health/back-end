package be.kdg.strategobackend.service;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.game.GameState;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.service.exception.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.Assert.fail;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class LobbyServiceTest {
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private GameService gameService;
    @Autowired
    private RegisterHelper registerHelper;
    @Autowired
    private PlayerService playerService;

    private Player joske;
    private Player joske2;
    private Player joske3;
    private Player joske4;
    private Player joske5;

    @Before
    public void setUp() throws Exception {
        joske = registerHelper.register("Joske");
        joske2 = registerHelper.register("Joske2");
        joske3 = registerHelper.register("Joske3");
        joske4 = registerHelper.register("Joske4");
        joske5 = registerHelper.register("Joske5");
    }

    @Test
    public void testCreateLobby() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobby = lobbyService.load(lobby.getId());
        Assert.assertNotNull(lobby);
        Assert.assertEquals(lobby.getGameState(), GameState.LOBBY);
        Assert.assertNotNull(lobby.getBoard());
        Assert.assertNotNull(lobby.getGameRules());

        GamePlayer gamePlayer = lobby.getGamePlayers().get(0);
        Assert.assertEquals(gamePlayer.getPlayer().getPlayerName(), joske.getPlayerName());
    }

    @Test
    public void testSendInvite() throws Exception {
        Game game = lobbyService.createLobby(joske.getPlayerName());
        playerService.addFriend(joske.getPlayerName(), joske2.getPlayerName());
        playerService.addFriend(joske2.getPlayerName(), joske.getPlayerName());
        boolean invited = lobbyService.canSendInvite(joske.getPlayerName(), joske2, game);
        Assert.assertTrue(invited);
    }

    @Test(expected = HostException.class)
    public void testSendInviteForFullGame() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());
        lobbyService.acceptInvite(joske3.getPlayerName(), lobby.getInviteUUID());

        lobbyService.canSendInvite(joske.getPlayerName(), joske4, lobby);
    }

    @Test(expected = HostException.class)
    public void testSendInviteAsNotHost() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());

        lobbyService.canSendInvite(joske2.getPlayerName(), joske3, lobby);
    }

    @Test
    public void testAcceptInvite() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());

        lobby = lobbyService.load(lobby.getId());
        Assert.assertEquals(lobby.getBoard().getPlayerCount(), lobby.getGamePlayers().size());

        Optional<GamePlayer> optionalGamePlayerHost = lobby.getGamePlayers()
                .stream()
                .filter(gamePlayer -> gamePlayer.getPlayer().getPlayerName().equals(joske.getPlayerName()))
                .findFirst();
        Assert.assertTrue(optionalGamePlayerHost.isPresent());
        GamePlayer gamePlayerHost = optionalGamePlayerHost.get();
        Assert.assertTrue(gamePlayerHost.isHost());

        Optional<GamePlayer> optionalGamePlayer = lobby.getGamePlayers()
                .stream()
                .filter(gamePlayer -> !gamePlayer.isComputer())
                .filter(gamePlayer -> gamePlayer.getPlayer().getPlayerName().equals(joske2.getPlayerName()))
                .findFirst();
        Assert.assertTrue(optionalGamePlayer.isPresent());
        GamePlayer gamePlayer = optionalGamePlayer.get();
        Assert.assertFalse(gamePlayer.isHost());
    }

    @Test
    public void testRejoinPublic() throws BoardException, PlayerAlreadyInGameException, GameJoinException, GameException, GamePlayerException, MissingHostException, HostException {
        Game lobby1 = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.addPlayerToGame(lobby1, joske2.getPlayerName());
        Game game1 = lobbyService.addPlayerToGame(lobby1, joske3.getPlayerName());
        Assert.assertEquals(3, game1.getGamePlayers().size());

        Game lobby2 = lobbyService.createLobby(joske4.getPlayerName());
        Game game2 = lobbyService.addPlayerToGame(lobby2, joske5.getPlayerName());
        Assert.assertEquals(3, game2.getGamePlayers().size());

        game1 = lobbyService.leaveGame(joske3.getPlayerName());
        Assert.assertEquals(3, game1.getGamePlayers().size());

        game2 = lobbyService.loadPublicLobby(joske5.getPlayerName());
        Assert.assertNotEquals(game1.getId(), game2.getId());
    }

    @Test
    public void testRejoinNonPublicAndNonLobby() throws BoardException, PlayerAlreadyInGameException, GameJoinException, GameException, GamePlayerException, MissingHostException, GameStartException, HostException {
        lobbyService.createLobby(joske.getPlayerName());
        Game lobby = lobbyService.loadPublicLobby(joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobby = lobbyService.loadPublicLobby(joske3.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        Game game2 = lobbyService.createLobby(joske4.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        gameService.startPreparationPhase(joske.getPlayerName());
        Game game1 = lobbyService.loadPublicLobby(joske3.getPlayerName());
        Assert.assertNotEquals(game1.getId(), game2.getId());

        gameService.startPlayingPhase(game1);
        game1 = lobbyService.loadPublicLobby(joske3.getPlayerName());
        Assert.assertNotEquals(game1.getId(), game2.getId());
    }

    @Test
    public void testJoinPublicAfterPlayingAGame() throws BoardException, PlayerAlreadyInGameException, GameJoinException, GameException, GameStartException, MissingHostException, GamePlayerException, HostException {
        lobbyService.createLobby(joske.getPlayerName());
        Game lobby = lobbyService.loadPublicLobby(joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobby = lobbyService.loadPublicLobby(joske3.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        Game game1 = gameService.startPreparationPhase(joske.getPlayerName());
        gameService.startPlayingPhase(game1);
        game1 = gameService.endGame(game1);
        Assert.assertSame(game1.getGameState(), GameState.FINISHED);

        lobbyService.createLobby(joske.getPlayerName());
        lobbyService.loadPublicLobby(joske2.getPlayerName());
        Game game2 = lobbyService.loadPublicLobby(joske3.getPlayerName());

        Assert.assertNotEquals(game1.getId(), game2.getId());
        Assert.assertSame(game2.getGameState(), GameState.LOBBY);
        Assert.assertEquals(3, game2.getGamePlayers().size());
    }

    @Test
    public void testLeaveLobbyWhileAlone() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.leaveGame(joske.getPlayerName());
        try {
            lobbyService.load(lobby.getId());
        } catch (GameException e) {
            return; //The game shouldn't exist
        }
        fail("The game still exists");
    }

    @Test
    public void testKickPlayer() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());
        lobbyService.acceptInvite(joske3.getPlayerName(), lobby.getInviteUUID());

        Assert.assertEquals(lobby.getBoard().getPlayerCount(), lobbyService.kickPlayer(joske.getPlayerName(), joske2.getPlayerName()).getGamePlayers().size());
        Assert.assertFalse(lobbyService.load(lobby.getId()).getGamePlayers()
                .stream()
                .filter(gamePlayer -> !gamePlayer.isComputer())
                .anyMatch(gamePlayer -> gamePlayer.getPlayer().getPlayerName().equals(joske2.getPlayerName())));
    }

    @Test(expected = HostException.class)
    public void testKickPlayerAsNotHost() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());
        lobbyService.acceptInvite(joske2.getPlayerName(), lobby.getInviteUUID());
        lobbyService.acceptInvite(joske3.getPlayerName(), lobby.getInviteUUID());

        lobbyService.kickPlayer(joske2.getPlayerName(), joske3.getPlayerName());

        Assert.assertTrue(lobbyService.load(lobby.getId()).getGamePlayers()
                .stream()
                .anyMatch(gamePlayer -> gamePlayer.getPlayer().getPlayerName().equals(joske2.getPlayerName())));
    }
}

package be.kdg.strategobackend.service;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.service.exception.FriendException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class PlayerServiceTest {
    @Autowired
    private RegisterHelper registerHelper;

    @Autowired
    private PlayerService playerService;

    private Player joske;
    private Player joske2;

    @Before
    public void setUp() throws Exception {
        joske = registerHelper.register("Joske");
        joske2 = registerHelper.register("Joske2");
    }

    @Test
    public void testAddFriend() throws FriendException {
        playerService.addFriend(joske.getPlayerName(), joske2.getPlayerName());
        playerService.addFriend(joske2.getPlayerName(), joske.getPlayerName());
        joske = playerService.loadPlayerByPlayerName(joske.getPlayerName());
        joske2 = playerService.loadPlayerByPlayerName(joske2.getPlayerName());
        Assert.assertTrue(joske.getFriends().stream().anyMatch(friend -> friend.getPlayerName().equals(joske2.getPlayerName())));
        Assert.assertTrue(joske2.getFriends().stream().anyMatch(friend -> friend.getPlayerName().equals(joske.getPlayerName())));
    }

    @Test
    public void testRemoveFriend() throws FriendException {
        playerService.addFriend(joske.getPlayerName(), joske2.getPlayerName());
        playerService.addFriend(joske2.getPlayerName(), joske.getPlayerName());
        joske = playerService.loadPlayerByPlayerName(joske.getPlayerName());
        joske2 = playerService.loadPlayerByPlayerName(joske2.getPlayerName());
        playerService.removeFriend(joske.getPlayerName(), joske2.getPlayerName());
        Assert.assertTrue(joske.getFriends().stream().noneMatch(friend -> friend.getPlayerName().equals(joske2.getPlayerName())));
        Assert.assertTrue(joske2.getFriends().stream().noneMatch(friend -> friend.getPlayerName().equals(joske.getPlayerName())));
    }
}

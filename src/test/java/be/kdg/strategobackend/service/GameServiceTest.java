package be.kdg.strategobackend.service;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.game.InitialOccupation;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.service.exception.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class GameServiceTest {
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private GameService gameService;
    @Autowired
    private GamePlayerService gamePlayerService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private PawnService pawnService;
    @Autowired
    private GameTurnService gameTurnService;
    @Autowired
    private RegisterHelper registerHelper;

    private Player joske;
    private Player joske2;
    private Player joske3;
    private Player joske4;

    private GamePlayer gamePlayerJoske;
    private GamePlayer gamePlayerJoske2;

    private Game game;

    @Before
    public void setUp() throws Exception {
        joske = registerHelper.register("e");
        joske2 = registerHelper.register("Joske2");
        joske3 = registerHelper.register("Joske3");
        joske4 = registerHelper.register("Joske4");
    }

    @Test
    public void testStartPreparationPhase() throws BoardException, PlayerAlreadyInGameException, MissingHostException, GameException, GameStartException, GamePlayerException, GameJoinException, HostException {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        gameService.startPreparationPhase(joske.getPlayerName());

        Game game = gameService.load(lobby.getId());

        Assert.assertEquals(game.getBoard().getBoardTiles().size(), boardService.load(game.getBoard().getId()).getBoardTiles().size());
        game.getGamePlayers().forEach(gamePlayer -> Assert.assertEquals(24, gamePlayer.getPawns().size()));
    }

    @Test
    public void testStartPlayingPhase() throws GameException, GameStartException, MissingHostException, GamePlayerException, GameJoinException, BoardException, PlayerAlreadyInGameException, HostException {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        Game game = gameService.startPreparationPhase(joske.getPlayerName());
        this.printInformation(game);
        game = gameService.startPlayingPhase(game);
        this.printInformation(game);

        game.getGamePlayers().forEach(gamePlayer -> gamePlayer.getPawns().forEach(pawn -> Assert.assertNotNull(pawn.getBoardTile())));

        Assert.assertFalse(this.checkIfNotValid(game));
        Assert.assertEquals(72, game.getInitialState().getInitialOccupations().size());

        List<InitialOccupation> checkedInitialOccupations = new ArrayList<>();
        boolean duplicatesFound = false;
        for (InitialOccupation initialOccupation : game.getInitialState().getInitialOccupations()) {
            boolean duplicate = false;
            for (InitialOccupation checkedInitialOccupation : checkedInitialOccupations) {
                if (checkedInitialOccupation.getId() == initialOccupation.getId()) {
                    duplicatesFound = true;
                    duplicate = true;
                }
            }
            if (!duplicate) checkedInitialOccupations.add(initialOccupation);
        }

        Assert.assertFalse(duplicatesFound);
    }

    @Test
    public void testOrganisePawnWith2Games() throws BoardException, PlayerAlreadyInGameException, MissingHostException, GameException, GamePlayerException, BoardTileException, GameStartException, InvalidGameStateException, PawnException, InvalidBoardTileException, InvalidPawnException, HostException {
        Game joskeLobby = lobbyService.createLobby(joske.getPlayerName());
        Game joske3Lobby = lobbyService.createLobby(joske3.getPlayerName());

        lobbyService.acceptInvite(joske2.getPlayerName(), joskeLobby.getInviteUUID());
        lobbyService.acceptInvite(joske4.getPlayerName(), joske3Lobby.getInviteUUID());

        joskeLobby = lobbyService.load(joskeLobby.getId());
        joske3Lobby = lobbyService.load(joske3Lobby.getId());

        GamePlayer gpJoske1 = joskeLobby.getGamePlayers().get(0);
        GamePlayer gpJoske2 = joskeLobby.getGamePlayers().get(1);
        GamePlayer gpJoske3 = joske3Lobby.getGamePlayers().get(0);
        GamePlayer gpJoske4 = joske3Lobby.getGamePlayers().get(1);

        gpJoske1.setTeam(Team.TEAM1);
        gpJoske2.setTeam(Team.TEAM2);
        gpJoske3.setTeam(Team.TEAM1);
        gpJoske4.setTeam(Team.TEAM2);

        gamePlayerService.save(gpJoske1);
        gamePlayerService.save(gpJoske2);
        gamePlayerService.save(gpJoske3);
        gamePlayerService.save(gpJoske4);

        lobbyService.save(joskeLobby);
        lobbyService.save(joske3Lobby);

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske4.getPlayerName(), true);

        gameService.startPreparationPhase(joske.getPlayerName());
        gameService.startPreparationPhase(joske3.getPlayerName());

        Game game1 = gameService.loadCurrentGameByPlayerName(joske.getPlayerName());
        GamePlayer updatedJoske = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        BoardTile game1BoardTile = game1.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() == updatedJoske.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn game1Pawn = updatedJoske.getPawns().get(0);

        Game game2 = gameService.loadCurrentGameByPlayerName(joske3.getPlayerName());
        GamePlayer updatedJoske3 = gamePlayerService.loadGamePlayerByName(joske3.getPlayerName());
        BoardTile game2BoardTile = game2.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() == updatedJoske3.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn game2Pawn = updatedJoske3.getPawns().get(0);

        gameService.organisePawn(game1Pawn.getGamePlayer().getPlayer().getPlayerName(), game1Pawn.getId(), game1BoardTile.getTileId());
        gameService.organisePawn(game2Pawn.getGamePlayer().getPlayer().getPlayerName(), game2Pawn.getId(), game2BoardTile.getTileId());
    }

    @Test(expected = InvalidBoardTileException.class)
    public void testOrganisePawnWith2GamesWithInvalidMoves() throws BoardException, PlayerAlreadyInGameException, MissingHostException, GameException, GamePlayerException, BoardTileException, GameStartException, InvalidGameStateException, PawnException, InvalidBoardTileException, InvalidPawnException, HostException {
        Game joskeLobby = lobbyService.createLobby(joske.getPlayerName());
        Game joske3Lobby = lobbyService.createLobby(joske3.getPlayerName());

        lobbyService.acceptInvite(joske2.getPlayerName(), joskeLobby.getInviteUUID());
        lobbyService.acceptInvite(joske4.getPlayerName(), joske3Lobby.getInviteUUID());

        joskeLobby = lobbyService.load(joskeLobby.getId());
        joske3Lobby = lobbyService.load(joske3Lobby.getId());

        GamePlayer gpJoske1 = joskeLobby.getGamePlayers().get(0);
        GamePlayer gpJoske2 = joskeLobby.getGamePlayers().get(1);
        GamePlayer gpJoske3 = joske3Lobby.getGamePlayers().get(0);
        GamePlayer gpJoske4 = joske3Lobby.getGamePlayers().get(1);

        gpJoske1.setTeam(Team.TEAM1);
        gpJoske2.setTeam(Team.TEAM2);
        gpJoske3.setTeam(Team.TEAM1);
        gpJoske4.setTeam(Team.TEAM2);

        gamePlayerService.save(gpJoske1);
        gamePlayerService.save(gpJoske2);
        gamePlayerService.save(gpJoske3);
        gamePlayerService.save(gpJoske4);

        lobbyService.save(joskeLobby);
        lobbyService.save(joske3Lobby);

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske4.getPlayerName(), true);

        gameService.startPreparationPhase(joske.getPlayerName());
        gameService.startPreparationPhase(joske3.getPlayerName());

        Game game1 = gameService.loadCurrentGameByPlayerName(joske.getPlayerName());
        GamePlayer updatedJoske = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        BoardTile game1BoardTile = game1.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() == updatedJoske.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn game1Pawn = updatedJoske.getPawns().get(0);

        Game game2 = gameService.loadCurrentGameByPlayerName(joske3.getPlayerName());
        GamePlayer updatedJoske3 = gamePlayerService.loadGamePlayerByName(joske3.getPlayerName());
        BoardTile game2BoardTile = game2.getBoard().getBoardTiles().stream().filter(bt -> bt.getTeam() == updatedJoske3.getTeam()).findFirst().orElseThrow(() -> new BoardTileException("The board tile could not be found."));
        Pawn game2Pawn = updatedJoske3.getPawns().get(0);

        gameService.organisePawn(game1Pawn.getGamePlayer().getPlayer().getPlayerName(), game1Pawn.getId(), game1BoardTile.getTileId());
        game1Pawn = updatedJoske.getPawns().get(1);
        gameService.organisePawn(game1Pawn.getGamePlayer().getPlayer().getPlayerName(), game1Pawn.getId(), game1BoardTile.getTileId());
        gameService.organisePawn(game2Pawn.getGamePlayer().getPlayer().getPlayerName(), game2Pawn.getId(), game2BoardTile.getTileId());
        game2Pawn = updatedJoske3.getPawns().get(1);
        gameService.organisePawn(game2Pawn.getGamePlayer().getPlayer().getPlayerName(), game2Pawn.getId(), game2BoardTile.getTileId());
    }

    @Test
    public void testMovePawnToTileOccupiedWithLowerRank() throws Exception {
        this.setUpMovePawnTest();

        Pawn pawn = gamePlayerJoske.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MINER).findFirst().get();
        pawn.setBoardTile(game.getBoard().getBoardTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId = pawnService.save(pawn).getBoardTile().getTileId();

        Pawn pawn2 = gamePlayerJoske2.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.BOMB).findFirst().get();
        pawn2.setBoardTile(pawn.getBoardTile().getConnectedTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId2 = pawnService.save(pawn2).getBoardTile().getTileId();

        game = gameService.load(game.getId());
        this.printInformation(game);

        gameService.movePawn(joske.getPlayerName(), pawn.getId(), tileId2);

        game = gameService.load(game.getId());
        this.printInformation(game);

        pawn = pawnService.load(pawn.getId());
        pawn2 = pawnService.load(pawn2.getId());

        Assert.assertFalse(pawn.isDead());
        Assert.assertTrue(pawn2.isDead());

        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId, game.getId()));
        Assert.assertNotNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));

        Assert.assertEquals(pawn, pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));
    }

    @Test
    public void testMovePawnToEmptyTile() throws Exception {
        this.setUpMovePawnTest();

        Pawn pawn = gamePlayerJoske.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MINER).findFirst().get();
        pawn.setBoardTile(game.getBoard().getBoardTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId = pawnService.save(pawn).getBoardTile().getTileId();

        int tileId2 = pawn.getBoardTile().getConnectedTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get().getTileId();

        gamePlayerService.save(gamePlayerJoske);
        gameService.movePawn(joske.getPlayerName(), pawn.getId(), tileId2);

        game = gameService.load(game.getId());
        this.printInformation(game);

        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId, game.getId()));
        Assert.assertNotNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));
    }

    @Test
    public void testMovePawnToTileOccupiedWithHigherRank() throws Exception {
        this.setUpMovePawnTest();

        Pawn pawn = gamePlayerJoske.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MINER).findFirst().get();
        pawn.setBoardTile(game.getBoard().getBoardTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId = pawnService.save(pawn).getBoardTile().getTileId();

        Pawn pawn2 = gamePlayerJoske2.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MAJOR).findFirst().get();
        pawn2.setBoardTile(pawn.getBoardTile().getConnectedTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId2 = pawnService.save(pawn2).getBoardTile().getTileId();

        long boardTileId2 = pawn2.getBoardTile().getId();

        gameService.movePawn(joske.getPlayerName(), pawn.getId(), tileId2);

        pawn = pawnService.load(pawn.getId());
        pawn2 = pawnService.load(pawn2.getId());

        game = gameService.load(game.getId());
        this.printInformation(game);

        pawn = pawnService.load(pawn.getId());
        pawn2 = pawnService.load(pawn2.getId());

        Assert.assertTrue(pawn.isDead());
        Assert.assertFalse(pawn2.isDead());

        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId, game.getId()));
        Assert.assertNotNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));

        Assert.assertEquals(pawn2, pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));
    }

    @Test
    public void testMoveRegularPawnToBomb() throws Exception {
        this.setUpMovePawnTest();

        Pawn pawn = gamePlayerJoske.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MAJOR).findFirst().get();
        pawn.setBoardTile(game.getBoard().getBoardTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId = pawnService.save(pawn).getBoardTile().getTileId();

        Pawn pawn2 = gamePlayerJoske2.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.BOMB).findFirst().get();
        pawn2.setBoardTile(pawn.getBoardTile().getConnectedTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId2 = pawnService.save(pawn2).getBoardTile().getTileId();

        long boardTileId2 = pawn2.getBoardTile().getId();

        gameService.movePawn(joske.getPlayerName(), pawn.getId(), tileId2);

        game = gameService.load(game.getId());
        this.printInformation(game);

        pawn = pawnService.load(pawn.getId());
        pawn2 = pawnService.load(pawn2.getId());

        Assert.assertTrue(pawn.isDead());
        Assert.assertTrue(pawn2.isDead());

        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId, game.getId()));
        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));
    }

    @Test
    public void testMoveMinerToBomb() throws Exception {
        this.setUpMovePawnTest();

        Pawn pawn = gamePlayerJoske.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MINER).findFirst().get();
        pawn.setBoardTile(game.getBoard().getBoardTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId = pawnService.save(pawn).getBoardTile().getTileId();

        Pawn pawn2 = gamePlayerJoske2.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.BOMB).findFirst().get();
        pawn2.setBoardTile(pawn.getBoardTile().getConnectedTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId2 = pawnService.save(pawn2).getBoardTile().getTileId();

        long boardTileId2 = pawn2.getBoardTile().getId();

        gameService.movePawn(joske.getPlayerName(), pawn.getId(), tileId2);

        game = gameService.load(game.getId());
        this.printInformation(game);

        pawn = pawnService.load(pawn.getId());
        pawn2 = pawnService.load(pawn2.getId());

        Assert.assertFalse(pawn.isDead());
        Assert.assertTrue(pawn2.isDead());

        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId, game.getId()));
        Assert.assertNotNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));
    }

    @Test
    public void testAttackPawnOfEqualRank() throws Exception {
        this.setUpMovePawnTest();

        Pawn pawn = gamePlayerJoske.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MINER).findFirst().get();
        pawn.setBoardTile(game.getBoard().getBoardTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId = pawnService.save(pawn).getBoardTile().getTileId();

        Pawn pawn2 = gamePlayerJoske2.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MINER).findFirst().get();
        pawn2.setBoardTile(pawn.getBoardTile().getConnectedTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId2 = pawnService.save(pawn2).getBoardTile().getTileId();

        gameService.movePawn(joske.getPlayerName(), pawn.getId(), tileId2);

        game = gameService.load(game.getId());
        this.printInformation(game);

        pawn = pawnService.load(pawn.getId());
        pawn2 = pawnService.load(pawn2.getId());

        Assert.assertTrue(pawn.isDead());
        Assert.assertTrue(pawn2.isDead());

        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId, game.getId()));
        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));
    }

    @Test
    public void testMoveSpyToMarshall() throws Exception {
        this.setUpMovePawnTest();

        Pawn pawn = gamePlayerJoske.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.SPY).findFirst().get();
        pawn.setBoardTile(game.getBoard().getBoardTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId = pawnService.save(pawn).getBoardTile().getTileId();

        Pawn pawn2 = gamePlayerJoske2.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MARSHAL).findFirst().get();
        pawn2.setBoardTile(pawn.getBoardTile().getConnectedTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId2 = pawnService.save(pawn2).getBoardTile().getTileId();

        gameService.movePawn(joske.getPlayerName(), pawn.getId(), tileId2);

        game = gameService.load(game.getId());
        this.printInformation(game);

        pawn = pawnService.load(pawn.getId());
        pawn2 = pawnService.load(pawn2.getId());

        Assert.assertFalse(pawn.isDead());
        Assert.assertTrue(pawn2.isDead());

        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId, game.getId()));
        Assert.assertNotNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));
    }

    @Test
    public void testMoveMarshallToSpy() throws Exception {
        this.setUpMovePawnTest();

        Pawn pawn = gamePlayerJoske.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.MARSHAL).findFirst().get();
        pawn.setBoardTile(game.getBoard().getBoardTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId = pawnService.save(pawn).getBoardTile().getTileId();

        Pawn pawn2 = gamePlayerJoske2.getPawns().stream().filter(pawn1 -> pawn1.getRank() == Rank.SPY).findFirst().get();
        pawn2.setBoardTile(pawn.getBoardTile().getConnectedTiles().stream().filter(boardTile -> boardTile.getTeam() == null).findFirst().get());

        int tileId2 = pawnService.save(pawn2).getBoardTile().getTileId();

        gameService.movePawn(joske.getPlayerName(), pawn.getId(), tileId2);

        game = gameService.load(game.getId());
        this.printInformation(game);

        Assert.assertFalse(pawn.isDead());
        Assert.assertTrue(pawn2.isDead());

        Assert.assertNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId, game.getId()));
        Assert.assertNotNull(pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), tileId2, game.getId()));
    }

    @Test
    public void testEndTurn() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        Game game = gameService.startPreparationPhase(joske.getPlayerName());

        GamePlayer gamePlayerJoske = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        GamePlayer gamePlayerJoske2 = gamePlayerService.loadGamePlayerByName(joske2.getPlayerName());
        GamePlayer gamePlayerJoske3 = gamePlayerService.loadGamePlayerByName(joske3.getPlayerName());

        GamePlayer firstGamePlayer = game.getGamePlayers().stream().filter(gamePlayer -> gamePlayer.getTeam() == Team.TEAM1).findFirst().get();
        firstGamePlayer.setCurrentTurn(true);
        gamePlayerService.save(firstGamePlayer);
        game = gameService.save(game);

        gameTurnService.endTurn(game, new GameTurn());

        game = gameService.load(game.getId());
        firstGamePlayer = gamePlayerService.load(firstGamePlayer.getId());
        GamePlayer secondGamePlayer = game.getGamePlayers().stream().filter(gamePlayer -> gamePlayer.getTeam() == Team.TEAM2).findFirst().get();

        Assert.assertFalse(firstGamePlayer.isCurrentTurn());
        Assert.assertTrue(secondGamePlayer.isCurrentTurn());

        gameTurnService.endTurn(game, new GameTurn());

        game = gameService.load(game.getId());
        secondGamePlayer = gamePlayerService.load(secondGamePlayer.getId());
        GamePlayer thirdGamePlayer = game.getGamePlayers().stream().filter(gamePlayer -> gamePlayer.getTeam() == Team.TEAM3).findFirst().get();

        Assert.assertFalse(secondGamePlayer.isCurrentTurn());
        Assert.assertTrue(thirdGamePlayer.isCurrentTurn());

        gameTurnService.endTurn(game, new GameTurn());

        thirdGamePlayer = gamePlayerService.load(thirdGamePlayer.getId());
        firstGamePlayer = gamePlayerService.load(firstGamePlayer.getId());

        Assert.assertFalse(thirdGamePlayer.isCurrentTurn());
        Assert.assertTrue(firstGamePlayer.isCurrentTurn());
    }

    private void setUpMovePawnTest() throws Exception {
        Game lobby = lobbyService.createLobby(joske.getPlayerName());

        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske3.getPlayerName());

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);
        lobbyService.updatePlayerStatus(joske3.getPlayerName(), true);

        game = gameService.startPreparationPhase(joske.getPlayerName());
        this.printInformation(game);

        gamePlayerJoske = gamePlayerService.loadGamePlayerByName(joske.getPlayerName());
        gamePlayerJoske2 = gamePlayerService.loadGamePlayerByName(joske2.getPlayerName());
        GamePlayer gamePlayerJoske3 = gamePlayerService.loadGamePlayerByName(joske2.getPlayerName());

        gamePlayerJoske.setCurrentTurn(true);
        gamePlayerJoske2.setCurrentTurn(false);
        gamePlayerJoske3.setCurrentTurn(false);

        gamePlayerService.save(gamePlayerJoske);
        gamePlayerService.save(gamePlayerJoske2);
        gamePlayerService.save(gamePlayerJoske3);

        game = gameService.startPlayingPhase(game);

        this.printInformation(game);

        Assert.assertFalse(this.checkIfNotValid(game));

    }

    private boolean checkIfNotValid(Game game) {
        boolean wrongTeam = false;
        List<Pawn> allPawns = new ArrayList<>();
        List<BoardTile> allTiles = game.getBoard().getBoardTiles();
        game.getGamePlayers().forEach(gamePlayer -> allPawns.addAll(gamePlayer.getPawns()));
        for (Pawn pawn : allPawns) {
            if (pawn.getBoardTile().getTeam() != pawn.getGamePlayer().getTeam()) wrongTeam = true;
        }

        if (wrongTeam) return true;

        boolean duplicated = false;
        List<BoardTile> duplicatedTiles = new ArrayList<>();
        for (Pawn pawn1 : allPawns) {
            for (BoardTile boardTile : allTiles) {
                for (Pawn pawn2 : allPawns) {
                    if (pawn1.getId() != pawn2.getId() && pawn1.getBoardTile().getId() == pawn2.getBoardTile().getId())
                        duplicatedTiles.add(boardTile);
                }
            }
        }

        if (duplicatedTiles.size() > 0) duplicated = true;

        return duplicated;
    }

    private void printInformation(Game game) {
        game.getGamePlayers().forEach(gamePlayer -> System.out.printf("Name: %s - Team - %s - Pawns: %d\n", gamePlayer.getPlayer().getPlayerName(), gamePlayer.getTeam(), gamePlayer.getPawns().size()));
    }
}

package be.kdg.strategobackend.service.manager.gamelogic.ai;

import be.kdg.strategobackend.RegisterHelper;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.Difficulty;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.service.*;
import be.kdg.strategobackend.service.exception.*;
import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class AIManagerTest {
    @Autowired
    private LobbyService lobbyService;
    @Autowired
    private GameService gameService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private RegisterHelper registerHelper;

    private Player joske;
    private Player joske2;

    @Before
    public void setUp() throws Exception {
        joske = registerHelper.register("Joske");
        joske2 = registerHelper.register("Joske2");
    }

    @Test
    public void testOrganisePawnsWithEasyAI() throws BoardException, PlayerAlreadyInGameException, MissingHostException, GameException, GameStartException, GamePlayerException, GameJoinException, HostException {
        this.createGameWithAI(Difficulty.EASY);
    }

    @Test
    public void testOrganisePawnsWithMediumAI() throws BoardException, PlayerAlreadyInGameException, MissingHostException, GameException, GameStartException, GamePlayerException, GameJoinException, HostException {
        this.createGameWithAI(Difficulty.MEDIUM);
    }

    @Test
    public void testOrganisePawnsWithHardAI() throws BoardException, PlayerAlreadyInGameException, MissingHostException, GameException, GameStartException, GamePlayerException, GameJoinException, HostException {
        this.createGameWithAI(Difficulty.HARD);
    }

    @Test
    public void testOrganisePawnsWithMasterAI() throws BoardException, PlayerAlreadyInGameException, MissingHostException, GameException, GameStartException, GamePlayerException, GameJoinException, HostException {
        this.createGameWithAI(Difficulty.MASTER);
    }

    private void createGameWithAI(Difficulty difficulty) throws BoardException, PlayerAlreadyInGameException, GameJoinException, GameException, MissingHostException, HostException, GamePlayerException, GameStartException {
        lobbyService.createLobby(joske.getPlayerName());
        Game lobby = lobbyService.loadPublicLobby(joske2.getPlayerName());
        lobbyService.addPlayerToGame(lobby, joske2.getPlayerName());
        GamePlayer gamePlayer = lobby.getGamePlayers().stream().filter(GamePlayer::isComputer).findFirst().orElseThrow(() -> new GamePlayerException("The game player could not be found."));
        lobbyService.updateComputer(joske.getPlayerName(), gamePlayer.getTeam(), difficulty);

        lobbyService.updatePlayerStatus(joske2.getPlayerName(), true);

        gameService.startPreparationPhase(joske.getPlayerName());

        Game game = gameService.load(lobby.getId());

        Assert.assertEquals(game.getBoard().getBoardTiles().size(), boardService.load(game.getBoard().getId()).getBoardTiles().size());
        game.getGamePlayers().forEach(gp -> Assert.assertEquals(24, gp.getPawns().size()));

        gamePlayer = game.getGamePlayers().stream().filter(GamePlayer::isComputer).findFirst().orElseThrow(() -> new GamePlayerException("The game player could not be found."));
        gamePlayer.getPawns().sort(Comparator.comparing(Pawn::getRank));

        boolean duplicated = false;
        List<Integer> tileIds = new ArrayList<>();
        List<Integer> duplicatedIds = new ArrayList<>();

        for (Pawn pawn : gamePlayer.getPawns()) {
            boolean tileTaken = false;
            for (Integer tileId : tileIds) {
                if (tileId == pawn.getBoardTile().getTileId()) {
                    tileTaken = true;
                    duplicated = true;
                }
            }
            if (!tileTaken) tileIds.add(pawn.getBoardTile().getTileId());
            else duplicatedIds.add(pawn.getBoardTile().getTileId());
        }

        System.out.print("Taken tiles: ");
        for (Integer tileId : tileIds) {
            System.out.print(tileId + " ");
        }
        System.out.println();

        System.out.print("Duplicated tiles: ");
        for (Integer tileId : duplicatedIds) {
            System.out.print(tileId + " ");
        }
        System.out.println();

        Assert.assertFalse(duplicated);
    }
}

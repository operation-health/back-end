package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.notification.EmailSender;
import be.kdg.strategobackend.rest.dto.friend.FriendListDTO;
import be.kdg.strategobackend.rest.dto.other.AccountManagementDTO;
import be.kdg.strategobackend.rest.dto.other.ChangePasswordDTO;
import be.kdg.strategobackend.rest.dto.other.PlayerEmailDTO;
import be.kdg.strategobackend.rest.dto.other.PlayerNameDTO;
import be.kdg.strategobackend.rest.dto.shared.PlayerDTO;
import be.kdg.strategobackend.rest.jwt.JwtTokenProvider;
import be.kdg.strategobackend.service.PlayerService;
import be.kdg.strategobackend.service.exception.FriendException;
import be.kdg.strategobackend.streaming.SseProcessor;
import be.kdg.strategobackend.streaming.event.actions.FriendEventBuilder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/player")
public class PlayerController {
    private final ModelMapper modelMapper;
    private final JwtTokenProvider jwtTokenProvider;
    private final PlayerService playerService;
    private final EmailSender emailSender;
    private final FriendEventBuilder friendEventBuilder;
    private final SseProcessor sseProcessor;

    @Value("${jwt.passwordResetInMillis}")
    private int passwordResetExpiration;

    @Autowired
    public PlayerController(ModelMapper modelMapper, JwtTokenProvider jwtTokenProvider, PlayerService playerService, EmailSender emailSender, FriendEventBuilder friendEventBuilder, SseProcessor sseProcessor) {
        this.modelMapper = modelMapper;
        this.jwtTokenProvider = jwtTokenProvider;
        this.playerService = playerService;
        this.emailSender = emailSender;
        this.friendEventBuilder = friendEventBuilder;
        this.sseProcessor = sseProcessor;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Post methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PostMapping("{playerName}")
    public ResponseEntity<PlayerDTO> sendPasswordResetMail(@PathVariable String playerName) {
        Player player = playerService.loadPlayerByPlayerName(playerName);
        String jwt = jwtTokenProvider.generateToken(player.getPlayerName(), passwordResetExpiration);
        emailSender.sendPasswordReset(player.getEmail(), jwt);
        return new ResponseEntity<>(modelMapper.map(player, PlayerDTO.class), HttpStatus.OK);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Get methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @GetMapping
    public ResponseEntity<PlayerDTO> showPlayer(@AuthenticationPrincipal User user) {
        Player player = playerService.loadPlayerByPlayerName(user.getUsername());
        return new ResponseEntity<>(modelMapper.map(player, PlayerDTO.class), HttpStatus.OK);
    }

    @GetMapping("/playerName")
    public ResponseEntity<PlayerNameDTO> getPlayerName(@AuthenticationPrincipal User user) {
        return new ResponseEntity<>(new PlayerNameDTO(user.getUsername()), HttpStatus.OK);
    }

    @GetMapping("/friends/friends")
    public ResponseEntity<FriendListDTO> getFriendList(@AuthenticationPrincipal User user) {
        Player player = playerService.loadPlayerByPlayerName(user.getUsername());
        return new ResponseEntity<>(modelMapper.map(player, FriendListDTO.class), HttpStatus.OK);
    }

    @GetMapping("/friends/requests")
    public ResponseEntity<List<PlayerNameDTO>> getFriendRequests(@AuthenticationPrincipal User user) {
        List<PlayerNameDTO> playerNameDTOS = new ArrayList<>();
        playerService.getFriendRequests(user.getUsername()).forEach(playerName -> playerNameDTOS.add(new PlayerNameDTO(playerName)));
        return new ResponseEntity<>(playerNameDTOS, HttpStatus.OK);
    }

    @GetMapping("/friends/search")
    public ResponseEntity<List<PlayerNameDTO>> searchFriends(@AuthenticationPrincipal User user, @RequestParam("searchValue") String searchValue) {
        Set<Player> players = playerService.findPlayersLikePlayerName(searchValue);

        List<PlayerNameDTO> playerNameDTOS = new ArrayList<>();

        players.stream()
                .limit(10)
                .filter(player -> !player.getPlayerName().equals(user.getUsername()))
                .forEach(player -> playerNameDTOS.add(new PlayerNameDTO(player.getPlayerName())));

        return new ResponseEntity<>(playerNameDTOS, HttpStatus.OK);
    }

    @GetMapping("/email")
    public ResponseEntity<PlayerEmailDTO> getEmail(@AuthenticationPrincipal User user) {
        Player player = playerService.loadEmail(user.getUsername());
        return new ResponseEntity<>(new PlayerEmailDTO(player.getEmail()), HttpStatus.OK);
    }

    @GetMapping("/recent")
    public ResponseEntity<List<PlayerNameDTO>> getRecentPlayers(@AuthenticationPrincipal User user) {
        List<Player> players = playerService.loadRecentPlayers(user.getUsername());
        List<PlayerNameDTO> playerNameDTOS = new ArrayList<>();
        players.forEach(player -> playerNameDTOS.add(new PlayerNameDTO(player.getPlayerName())));
        return new ResponseEntity<>(playerNameDTOS, HttpStatus.OK);
    }

    @GetMapping("/picture")
    @ResponseBody
    public ResponseEntity<Resource> getProfilePicture(@AuthenticationPrincipal User user) throws MalformedURLException, FileNotFoundException {
        Resource file = playerService.loadProfilePicture(user.getUsername());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    @GetMapping("/picture/{playerName}")
    public ResponseEntity<Resource> getProfilePictureOfOtherUser(@PathVariable String playerName) throws MalformedURLException, FileNotFoundException {
        Resource file = playerService.loadProfilePicture(playerName);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Patch methods ----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PatchMapping("/friends/{friendName}")
    public ResponseEntity addFriend(@AuthenticationPrincipal User user, @PathVariable String friendName) throws UsernameNotFoundException, FriendException {
        String playerName = user.getUsername();
        if (playerName.equals(friendName)) throw new FriendException("You can't add yourself as a friend.");

        boolean added = playerService.addFriend(friendName, playerName);
        Player friendPlayer = playerService.loadPlayerByPlayerName(friendName);

        if (added) {
            Player player = playerService.loadPlayerByPlayerName(playerName);
            sseProcessor.push(player, friendEventBuilder.createFriendRequestAccepted(friendName));
            sseProcessor.push(friendPlayer, friendEventBuilder.createFriendRequestAccepted(playerName));
        } else sseProcessor.push(friendPlayer, friendEventBuilder.createFriendRequest(playerName));

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping("/friends/delete/{friendName}")
    public ResponseEntity deleteFriend(@AuthenticationPrincipal User user, @PathVariable String friendName) throws UsernameNotFoundException, FriendException {
        String playerName = user.getUsername();
        if (playerName.equals(friendName)) throw new FriendException("You can't delete yourself as a friend.");

        playerService.removeFriend(playerName, friendName);
        Player player = playerService.loadPlayerByPlayerName(playerName);
        Player friendPlayer = playerService.loadPlayerByPlayerName(friendName);

        sseProcessor.push(player, friendEventBuilder.createFriendRemoved(friendName));
        sseProcessor.push(friendPlayer, friendEventBuilder.createFriendRemoved(playerName));

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity<PlayerDTO> changePassword(@AuthenticationPrincipal User user, @RequestBody ChangePasswordDTO changePasswordDTO) {
        Player player = playerService.changePassword(user.getUsername(), changePasswordDTO.getPassword());
        return new ResponseEntity<>(modelMapper.map(player, PlayerDTO.class), HttpStatus.OK);
    }

    @PatchMapping("/picture")
    public ResponseEntity<PlayerDTO> changeProfilePicture(@AuthenticationPrincipal User user, @RequestParam("picture") MultipartFile picture) throws IOException {
        Player player = playerService.changeProfilePicture(user.getUsername(), picture);
        return new ResponseEntity<>(modelMapper.map(player, PlayerDTO.class), HttpStatus.OK);
    }

    @PatchMapping("/fcm/{fcmId}")
    public ResponseEntity addFcm(@AuthenticationPrincipal User user, @PathVariable String fcmId) throws UsernameNotFoundException {
        Player player = playerService.loadPlayerByPlayerName(user.getUsername());
        player.setFcmId(fcmId);
        playerService.save(player);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Put methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PutMapping
    public ResponseEntity<PlayerDTO> editAccount(@AuthenticationPrincipal User user, @RequestBody AccountManagementDTO accountManagementDTO) {
        Player player = playerService.editAccount(user.getUsername(), accountManagementDTO.getEmail(), accountManagementDTO.getPassword());
        return new ResponseEntity<>(modelMapper.map(player, PlayerDTO.class), HttpStatus.OK);
    }
}

package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.notification.NotificationSender;
import be.kdg.strategobackend.rest.dto.game.GameDTO;
import be.kdg.strategobackend.rest.dto.game.MovePawnDTO;
import be.kdg.strategobackend.rest.dto.game.TimerDTO;
import be.kdg.strategobackend.rest.dto.shared.UpdatePlayerReadyDTO;
import be.kdg.strategobackend.service.GameService;
import be.kdg.strategobackend.service.GameTurnService;
import be.kdg.strategobackend.service.exception.*;
import be.kdg.strategobackend.service.manager.gamelogic.GameTurnTransition;
import be.kdg.strategobackend.streaming.GameTimer;
import be.kdg.strategobackend.streaming.SseProcessor;
import be.kdg.strategobackend.streaming.event.Action;
import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.GameEventBuilder;
import be.kdg.strategobackend.streaming.event.actions.LobbyEventBuilder;
import be.kdg.strategobackend.utility.SseSupport;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/game")
public class GameController {
    private final SseProcessor sseProcessor;
    private final SseSupport sseSupport;
    private final GameService gameService;
    private final GameTimer gameTimer;
    private final LobbyEventBuilder lobbyEventBuilder;
    private final GameEventBuilder gameEventBuilder;
    private final ModelMapper modelMapper;
    private final GameTurnService gameTurnService;
    private final List<NotificationSender> notificationSenders;

    @Autowired
    public GameController(SseProcessor sseProcessor, SseSupport sseSupport, GameService gameService, GameTimer gameTimer, LobbyEventBuilder lobbyEventBuilder, GameEventBuilder gameEventBuilder, ModelMapper modelMapper, GameTurnService gameTurnService, List<NotificationSender> notificationSenders) {
        this.sseProcessor = sseProcessor;
        this.sseSupport = sseSupport;
        this.gameService = gameService;
        this.gameTimer = gameTimer;
        this.lobbyEventBuilder = lobbyEventBuilder;
        this.gameEventBuilder = gameEventBuilder;
        this.modelMapper = modelMapper;
        this.gameTurnService = gameTurnService;
        this.notificationSenders = notificationSenders;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Get methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @GetMapping("/timer")
    public ResponseEntity<TimerDTO> getTimer(@AuthenticationPrincipal User user) throws GameException, GamePlayerException {
        long time = gameService.getRemainingTime(user.getUsername());
        return new ResponseEntity<>(new TimerDTO(time), HttpStatus.OK);
    }

    @GetMapping("/state")
    public ResponseEntity<GameDTO> getFullGame(@AuthenticationPrincipal User user) throws GameException {
        Game game = gameService.loadCurrentGameByPlayerName(user.getUsername());

        GameDTO gameDTO = modelMapper.map(game, GameDTO.class);
        gameDTO.getGamePlayers()
                .stream()
                .filter(gamePlayer -> gamePlayer.isComputer() || !gamePlayer.getPlayer().getPlayerName().equals(user.getUsername()))
                .forEach(gamePlayer -> gamePlayer.getPawns()
                        .forEach(pawn -> pawn.setRank(null)));

        return new ResponseEntity<>(gameDTO, HttpStatus.OK);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Patch methods ----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PatchMapping
    public ResponseEntity updatePlayerStatus(@AuthenticationPrincipal User user, UpdatePlayerReadyDTO updatePlayerReadyDTO) throws GameException, GamePlayerException, InvalidGameStateException {
        String playerName = user.getUsername();
        GamePlayer gamePlayer = gameService.updatePlayerStatus(playerName, updatePlayerReadyDTO.isReady());
        Game game = gameService.loadCurrentGameByPlayerName(playerName);

        if (gameService.isEveryoneReady(game.getGamePlayers())) {
            game = gameService.startPlayingPhase(game);
            sseSupport.sendPlayingPhasePackets(game);
        } else {
            Event event = gameEventBuilder.createGamePlayerReadyChanged(gamePlayer);
            sseProcessor.push(game, event);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Put methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PutMapping
    public ResponseEntity startGame(@AuthenticationPrincipal User user) throws MissingHostException, GameException, GameStartException {
        String playerName = user.getUsername();
        Game game = gameService.startPreparationPhase(playerName);
        Event event = lobbyEventBuilder.createLobbyGameStarted();
        sseProcessor.push(game, event);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/organise")
    public ResponseEntity organisePawn(@AuthenticationPrincipal User user, @RequestBody MovePawnDTO movePawnDTO) throws PawnException, GamePlayerException, InvalidBoardTileException, BoardTileException, GameException, InvalidPawnException, InvalidGameStateException {
        String playerName = user.getUsername();
        long pawnId = movePawnDTO.getId();
        int tileId = movePawnDTO.getTileId();
        Game game = gameService.organisePawn(playerName, movePawnDTO.getId(), movePawnDTO.getTileId());
        Event event = gameEventBuilder.createGamePawnOrganised(pawnId, tileId);
        sseProcessor.push(game, event);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/move")
    public ResponseEntity movePawn(@AuthenticationPrincipal User user, @RequestBody MovePawnDTO movePawnDTO) throws GameException, PawnException, GamePlayerException, InvalidPawnException, InvalidGameStateException, BoardTileException, InvalidBoardTileException, GameTurnException {
        String playerName = user.getUsername();
        GameTurn gameTurn = gameService.movePawn(playerName, movePawnDTO.getId(), movePawnDTO.getTileId());
        Game game = gameService.loadCurrentGameByPlayerName(playerName);
        GameTurnTransition gameTurnTransition = gameTurnService.endTurn(game, gameTurn);
        Event event = sseSupport.getGameEvent(game, gameTurnTransition);
        sseProcessor.push(game, event);

        if (event.getAction() == Action.GAME_FINAL_BLOW
                || event.getAction() == Action.GAME_PASSIVE_WIN
                || event.getAction() == Action.GAME_HUMANS_ELIMINATED
                || event.getAction() == Action.GAME_ATTRITION_VICTORY) {
            gameTimer.endTurnSystem(game);
            gameService.endGame(game);
        } else {
            gameTimer.startTurnWithNewTransaction(game.getId());
            notificationSenders.forEach(notificationSender -> {
                notificationSender.sendTurnAlert(gameTurnTransition.getNextPlayer(), game);
                notificationSender.sendTurnPlayedAlert(gameTurnTransition.getGameTurn().getGamePlayer(), game);
            });
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}

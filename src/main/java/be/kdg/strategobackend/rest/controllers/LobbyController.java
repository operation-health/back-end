package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.notification.GameInviteSender;
import be.kdg.strategobackend.rest.dto.lobby.*;
import be.kdg.strategobackend.rest.dto.other.PlayerNameDTO;
import be.kdg.strategobackend.rest.dto.shared.*;
import be.kdg.strategobackend.service.LobbyService;
import be.kdg.strategobackend.service.PlayerService;
import be.kdg.strategobackend.service.exception.*;
import be.kdg.strategobackend.streaming.SseProcessor;
import be.kdg.strategobackend.streaming.event.actions.LobbyEventBuilder;
import be.kdg.strategobackend.utility.SseSupport;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/lobby")
public class LobbyController {
    private final SseProcessor sseProcessor;
    private final SseSupport sseSupport;
    private final LobbyEventBuilder lobbyEventBuilder;
    private final LobbyService lobbyService;
    private final PlayerService playerService;
    private final ModelMapper modelMapper;
    private final List<GameInviteSender> gameInviteSenders;

    @Autowired
    public LobbyController(LobbyService lobbyService, ModelMapper modelMapper, SseProcessor sseProcessor, SseSupport sseSupport, LobbyEventBuilder lobbyEventBuilder, PlayerService playerService, List<GameInviteSender> gameInviteSenders) {
        this.sseProcessor = sseProcessor;
        this.lobbyService = lobbyService;
        this.sseSupport = sseSupport;
        this.lobbyEventBuilder = lobbyEventBuilder;
        this.modelMapper = modelMapper;
        this.playerService = playerService;
        this.gameInviteSenders = gameInviteSenders;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Post methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PostMapping
    public ResponseEntity<LobbyDTO> createLobby(@AuthenticationPrincipal User user) throws BoardException, PlayerAlreadyInGameException, GameException {
        String hostname = user.getUsername();
        Game game = lobbyService.createLobby(hostname);
        return new ResponseEntity<>(modelMapper.map(game, LobbyDTO.class), HttpStatus.CREATED);
    }

    @PostMapping("/invite")
    public ResponseEntity invitePlayer(@AuthenticationPrincipal User user, @RequestBody PlayerNameDTO playerNameDTO) throws GameException, HostException, MissingHostException, PlayerAlreadyInGameException {
        String playerName = user.getUsername();
        Game game = lobbyService.loadCurrentGameByPlayerName(playerName);
        Player playerToInvite = playerService.loadPlayerByPlayerName(playerNameDTO.getPlayerName());

        if (lobbyService.canSendInvite(playerName, playerToInvite, game)) {
            gameInviteSenders.forEach(gameInviteSender -> gameInviteSender.sendGameInvite(playerToInvite, playerName, game.getInviteUUID()));
            return new ResponseEntity(HttpStatus.OK);
        }

        return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Get methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @GetMapping("/invite/{inviteUUID}")
    public ResponseEntity<LobbyDTO> acceptInvite(@AuthenticationPrincipal User user, @PathVariable UUID inviteUUID) throws GameException, MissingHostException, PlayerAlreadyInGameException, GamePlayerException {
        String playerName = user.getUsername();
        Game game = lobbyService.acceptInvite(playerName, inviteUUID);

        Team team = game.getGamePlayers().stream()
                .filter(gamePlayer -> !gamePlayer.isComputer())
                .filter(gamePlayer -> gamePlayer.getPlayer().getPlayerName().equals(playerName))
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."))
                .getTeam();

        if (game.getGamePlayers().stream().anyMatch(GamePlayer::isComputer))
            sseProcessor.push(game, lobbyEventBuilder.createLobbyAiLeft(team));

        return sseSupport.sendLobbyPlayerJoined(game, playerName);
    }

    @GetMapping("/invite/uuid")
    public ResponseEntity<InviteDTO> getInviteUUID(@AuthenticationPrincipal User user) throws GameException {
        Game lobby = lobbyService.loadCurrentGameByPlayerName(user.getUsername());
        return new ResponseEntity<>(new InviteDTO(lobby.getInviteUUID()), HttpStatus.OK);
    }

    @GetMapping("/join")
    public ResponseEntity<LobbyDTO> joinPublic(@AuthenticationPrincipal User user) throws GameJoinException, GameException, GamePlayerException {
        String playerName = user.getUsername();
        Game game = lobbyService.loadPublicLobby(playerName);

        if (lobbyService.isAddable(game, playerName)) {
            game = lobbyService.addPlayerToGame(game, playerName);

            Team team = game.getGamePlayers().stream()
                    .filter(gamePlayer -> !gamePlayer.isComputer())
                    .filter(gamePlayer -> gamePlayer.getPlayer().getPlayerName().equals(playerName))
                    .findFirst()
                    .orElseThrow(() -> new GamePlayerException("The game player could not be found."))
                    .getTeam();

            sseProcessor.push(game, lobbyEventBuilder.createLobbyAiLeft(team));
        }

        return sseSupport.sendLobbyPlayerJoined(game, playerName);
    }

    @GetMapping
    public ResponseEntity<LobbyDTO> getLobby(@AuthenticationPrincipal User user) throws GameException {
        Game game = lobbyService.loadCurrentGameByPlayerName(user.getUsername());
        return new ResponseEntity<>(modelMapper.map(game, LobbyDTO.class), HttpStatus.OK);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Patch methods ----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PatchMapping("/privacy")
    public ResponseEntity updatePrivacy(@AuthenticationPrincipal User user, @RequestBody UpdatePrivacyDTO updatePrivacyDTO) throws MissingHostException, GameException, HostException {
        String playerName = user.getUsername();
        Game game = lobbyService.updatePrivacy(playerName, updatePrivacyDTO.isPublicAccess());
        sseProcessor.push(game, lobbyEventBuilder.createLobbyAccessChanged(game.isPublicAccess()));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/board")
    public ResponseEntity updateBoard(@AuthenticationPrincipal User user, @RequestBody UpdateBoardDTO updateBoardDTO) throws BoardException, MissingHostException, GameException, HostException, GamePlayerException {
        String playerName = user.getUsername();

        Game game = lobbyService.loadCurrentGameByPlayerName(playerName);
        List<GamePlayer> oldComputerGamePlayers = game.getGamePlayers()
                .stream()
                .filter(GamePlayer::isComputer)
                .collect(Collectors.toList());

        game = lobbyService.updateBoard(playerName, updateBoardDTO.getBoardType());
        List<GamePlayer> newComputerGamePlayers = game.getGamePlayers()
                .stream()
                .filter(GamePlayer::isComputer)
                .collect(Collectors.toList());

        sseProcessor.push(game, lobbyEventBuilder.createLobbyBoardChanged(game.getBoard()));

        if (newComputerGamePlayers.containsAll(oldComputerGamePlayers)) {
            newComputerGamePlayers.removeAll(oldComputerGamePlayers);
            for (GamePlayer computerGamePlayer : newComputerGamePlayers)
                sseProcessor.push(game, lobbyEventBuilder.createLobbyAiJoined(computerGamePlayer));
        } else {
            oldComputerGamePlayers.removeAll(newComputerGamePlayers);
            for (GamePlayer computerGamePlayer : oldComputerGamePlayers)
                sseProcessor.push(game, lobbyEventBuilder.createLobbyAiLeft(computerGamePlayer.getTeam()));
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/ready")
    public ResponseEntity updatePlayerReady(@AuthenticationPrincipal User user, @RequestBody UpdatePlayerReadyDTO updatePlayerReadyDTO) throws GameException, GamePlayerException {
        String playerName = user.getUsername();
        Game game = lobbyService.updatePlayerStatus(playerName, updatePlayerReadyDTO.isReady());
        sseProcessor.push(game, lobbyEventBuilder.createLobbyPlayerReadyChanged(playerName, updatePlayerReadyDTO.isReady()));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/increasedifficulty")
    public ResponseEntity increaseDifficulty(@AuthenticationPrincipal User user, @RequestBody UpdateComputerDTO updateComputerDTO) throws GameException, GamePlayerException, MissingHostException, HostException {
        String playerName = user.getUsername();
        GamePlayer gamePlayer = this.lobbyService.increaseDifficulty(playerName, updateComputerDTO.getTeam());
        sseProcessor.push(gamePlayer.getGame(), lobbyEventBuilder.createLobbyAiUpdated(gamePlayer));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PatchMapping("/decreasedifficulty")
    public ResponseEntity decreaseDifficulty(@AuthenticationPrincipal User user, @RequestBody UpdateComputerDTO updateComputerDTO) throws GameException, GamePlayerException, MissingHostException, HostException {
        String playerName = user.getUsername();
        GamePlayer gamePlayer = this.lobbyService.decreaseDifficulty(playerName, updateComputerDTO.getTeam());
        sseProcessor.push(gamePlayer.getGame(), lobbyEventBuilder.createLobbyAiUpdated(gamePlayer));
        return new ResponseEntity(HttpStatus.OK);
    }


    //------------------------------------------------------------------------------------------------------------------
    //Put methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PutMapping("/turntimer")
    public ResponseEntity updateTurnTimer(@AuthenticationPrincipal User user, @RequestBody TurnTimerDTO turnTimerDTO) throws MissingHostException, GameException, HostException {
        String playerName = user.getUsername();
        Game game = lobbyService.updateTurnTimer(playerName, turnTimerDTO.getTurnTimer());
        sseProcessor.push(game, lobbyEventBuilder.createLobbyTurnTimerChanged(turnTimerDTO.getTurnTimer()));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/afktimer")
    public ResponseEntity updateAfkTimer(@AuthenticationPrincipal User user, @RequestBody AfkTimerDTO afkTimerDTO) throws MissingHostException, GameException, HostException {
        String playerName = user.getUsername();
        Game game = lobbyService.updateAfkTimer(playerName, afkTimerDTO.getAfkTimer());
        sseProcessor.push(game, lobbyEventBuilder.createLobbyAfkTimerChanged(afkTimerDTO.getAfkTimer()));
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/preparationtimer")
    public ResponseEntity updatePreparationTimer(@AuthenticationPrincipal User user, @RequestBody PreparationTimerDTO preparationTimerDTO) throws MissingHostException, GameException, HostException {
        String playerName = user.getUsername();
        Game game = lobbyService.updatePreparationTimer(playerName, preparationTimerDTO.getPreparationTimer());
        sseProcessor.push(game, lobbyEventBuilder.createLobbyPrepTimerChanged(preparationTimerDTO.getPreparationTimer()));
        return new ResponseEntity(HttpStatus.OK);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Delete methods ---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @DeleteMapping
    public ResponseEntity kickPlayer(@AuthenticationPrincipal User user, @RequestBody DeletePlayerDTO deletePlayerDTO) throws GameException, HostException, MissingHostException, GamePlayerException {
        Game game = lobbyService.loadCurrentGameByPlayerName(user.getUsername());
        GamePlayer originalGamePlayer = game.getGamePlayers()
                .stream()
                .filter(gp -> !gp.isComputer())
                .filter(gp -> gp.getPlayer().getPlayerName().equals(deletePlayerDTO.getPlayerName()))
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."));

        lobbyService.kickPlayer(user.getUsername(), deletePlayerDTO.getPlayerName());
        sseProcessor.push(game, lobbyEventBuilder.createLobbyPlayerKicked(deletePlayerDTO.getPlayerName()));
        sseSupport.sendNewComputerPackets(game, originalGamePlayer);

        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/leave")
    public ResponseEntity leaveGame(@AuthenticationPrincipal User user) throws GameException, GamePlayerException, HostException {
        String playerName = user.getUsername();
        Game game = lobbyService.loadCurrentGameByPlayerName(playerName);
        GamePlayer originalHostGamePlayer = game.getGamePlayers()
                .stream()
                .filter(gamePlayer -> !gamePlayer.isComputer())
                .filter(GamePlayer::isHost)
                .findFirst()
                .orElseThrow(() -> new GameException("The player is not in a game."));

        GamePlayer originalGamePlayer = game.getGamePlayers()
                .stream()
                .filter(gamePlayer -> !gamePlayer.isComputer())
                .filter(gamePlayer -> gamePlayer.getPlayer().getPlayerName().equals(playerName))
                .findFirst()
                .orElseThrow(() -> new GameException("The player is not in a game."));

        game = lobbyService.leaveGame(playerName);

        if (game == null) return new ResponseEntity(HttpStatus.OK);

        Optional<GamePlayer> newHostGamePlayer = game.getGamePlayers()
                .stream()
                .filter(gp -> !gp.isComputer() && gp.isHost())
                .findFirst();

        if (newHostGamePlayer.isPresent()) {
            String newHostPlayerName = newHostGamePlayer.get().getPlayer().getPlayerName();
            if (originalHostGamePlayer.getPlayer().getPlayerName().equals(newHostPlayerName))
                sseProcessor.push(game, lobbyEventBuilder.createLobbyPlayerLeft(playerName));
            else sseProcessor.push(game, lobbyEventBuilder.createLobbyHostLeft(playerName, newHostPlayerName));
            sseSupport.sendNewComputerPackets(game, originalGamePlayer);
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}

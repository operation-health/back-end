package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.player.PlayerStats;
import be.kdg.strategobackend.rest.dto.shared.PlayerStatsDTO;
import be.kdg.strategobackend.rest.dto.shared.UpdatePrivacyDTO;
import be.kdg.strategobackend.service.PlayerStatsService;
import be.kdg.strategobackend.service.exception.PlayerStatsException;
import be.kdg.strategobackend.service.exception.PlayerStatsInaccessibleException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/stats")
public class PlayerStatsController {
    private final PlayerStatsService playerStatsService;
    private final ModelMapper modelMapper;

    @Autowired
    public PlayerStatsController(PlayerStatsService playerStatsService, ModelMapper modelMapper) {
        this.playerStatsService = playerStatsService;
        this.modelMapper = modelMapper;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Get methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @GetMapping("/{playerName}")
    public ResponseEntity<PlayerStatsDTO> getPlayerStatsByPlayerName(@AuthenticationPrincipal User user, @PathVariable String playerName) throws PlayerStatsException, PlayerStatsInaccessibleException {
        PlayerStats playerStats = playerStatsService.loadPlayerStatsByPlayerName(user.getUsername(), playerName);
        return new ResponseEntity<>(modelMapper.map(playerStats, PlayerStatsDTO.class), HttpStatus.OK);
    }

    @PatchMapping
    public ResponseEntity<PlayerStatsDTO> updateStatsPrivacy(@AuthenticationPrincipal User user, UpdatePrivacyDTO updatePrivacyDTO) throws PlayerStatsException {
        playerStatsService.updatePlayerStats(user.getUsername(), updatePrivacyDTO.isPublicAccess());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.service.PlayerService;
import be.kdg.strategobackend.streaming.SseProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
@RequestMapping("api/stream")
public class StreamController {
    private final SseProcessor sseProcessor;
    private final PlayerService playerService;

    @Autowired
    public StreamController(SseProcessor sseProcessor, PlayerService playerService) {
        this.sseProcessor = sseProcessor;
        this.playerService = playerService;
    }

    @GetMapping
    public SseEmitter getStream(@AuthenticationPrincipal User user) {
        Player player = playerService.loadPlayerByPlayerName(user.getUsername());
        return sseProcessor.getEmitter(player);
    }
}

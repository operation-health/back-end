package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.player.NotificationSettings;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.model.player.PlayerStats;
import be.kdg.strategobackend.notification.EmailSender;
import be.kdg.strategobackend.rest.dto.other.*;
import be.kdg.strategobackend.rest.dto.shared.PlayerDTO;
import be.kdg.strategobackend.rest.jwt.JwtTokenProvider;
import be.kdg.strategobackend.service.NotificationSettingsService;
import be.kdg.strategobackend.service.PlayerService;
import be.kdg.strategobackend.service.PlayerStatsService;
import be.kdg.strategobackend.service.SocialAuthService;
import be.kdg.strategobackend.service.exception.EmailAlreadyExistsException;
import be.kdg.strategobackend.service.exception.EmailConfirmationException;
import be.kdg.strategobackend.service.exception.PlayerNameAlreadyExistsException;
import be.kdg.strategobackend.service.exception.SocialLoginException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;

@RestController
@RequestMapping("api/authentication")
public class AuthenticationController {
    private final JwtTokenProvider jwtTokenProvider;
    private final ModelMapper modelMapper;
    private final PlayerService playerService;
    private final EmailSender emailSender;
    private final PlayerStatsService playerStatsService;
    private final SocialAuthService socialAuthService;
    private final NotificationSettingsService notificationSettingsService;

    @Value("${jwt.emailConfirmationInMillis}")
    private int confirmationExpiration;

    @Autowired
    public AuthenticationController(ModelMapper modelMapper, JwtTokenProvider jwtTokenProvider, PlayerService playerService, EmailSender emailSender, PlayerStatsService playerStatsService, SocialAuthService socialAuthService, NotificationSettingsService notificationSettingsService) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.modelMapper = modelMapper;
        this.playerService = playerService;
        this.emailSender = emailSender;
        this.playerStatsService = playerStatsService;
        this.socialAuthService = socialAuthService;
        this.notificationSettingsService = notificationSettingsService;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Post methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PostMapping("/register")
    public ResponseEntity<PlayerDTO> register(@RequestBody RegisterDTO registerDTO) throws PlayerNameAlreadyExistsException, EmailAlreadyExistsException {
        Player playerToRegister = new Player(registerDTO.getEmail(), registerDTO.getPlayerName(), registerDTO.getPassword());
        playerToRegister.setFriends(new HashSet<>());
        playerToRegister.setHasPassword(true);

        PlayerStats playerStats = new PlayerStats();
        playerStatsService.save(playerStats);
        playerToRegister.setPlayerStats(playerStats);

        NotificationSettings notificationSettings = new NotificationSettings();
        notificationSettings.setYourTurn(false);
        notificationSettings.setTurnPlayed(false);
        notificationSettings.setTurnExpired(false);
        notificationSettings = notificationSettingsService.save(notificationSettings);

        playerToRegister.setNotificationSettings(notificationSettings);
        Player registeredPlayer = playerService.register(playerToRegister);
        emailSender.sendEmailConfirmation(registeredPlayer.getEmail(), jwtTokenProvider.generateToken(registeredPlayer.getPlayerName(), confirmationExpiration));

        return new ResponseEntity<>(modelMapper.map(registeredPlayer, PlayerDTO.class), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<TokenDTO> logIn(@RequestBody LoginDTO loginDTO) throws UsernameNotFoundException, EmailConfirmationException {
        Player player = playerService.loadPlayerThatHasPassword(loginDTO.getPlayerNameOrEmail());
        String jwt = jwtTokenProvider.authenticateAndGenerateToken(player.getPlayerName(), loginDTO.getPassword());
        return new ResponseEntity<>(new TokenDTO(jwt, player.getPlayerName()), HttpStatus.OK);
    }

    @PostMapping("/socialregister")
    public ResponseEntity<PlayerDTO> registerWithSocial(@RequestBody SocialRegisterDTO socialRegisterDTO) throws PlayerNameAlreadyExistsException, EmailAlreadyExistsException, SocialLoginException {
        Player player = socialAuthService.registerWithSocial(socialRegisterDTO.getPlayerName(), socialRegisterDTO.getToken(), SocialProvider.valueOf(socialRegisterDTO.getProvider().toUpperCase()));
        return new ResponseEntity<>(modelMapper.map(player, PlayerDTO.class), HttpStatus.CREATED);
    }

    @PostMapping("/sociallogin")
    public ResponseEntity<TokenDTO> loginWithSocial(@RequestBody SocialLoginDTO socialLoginDTO) throws EmailConfirmationException, SocialLoginException {
        Player player = socialAuthService.findPlayerBySocial(socialLoginDTO.getToken(), SocialProvider.valueOf(socialLoginDTO.getProvider().toUpperCase()));
        String jwt = jwtTokenProvider.generateLoginToken(player.getPlayerName());
        return new ResponseEntity<>(new TokenDTO(jwt, player.getPlayerName()), HttpStatus.CREATED);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Get methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @GetMapping()
    public ResponseEntity isLoggedIn(@AuthenticationPrincipal User user) {
        this.playerService.loadUserByUsername(user.getUsername());
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/{playerName}")
    public ResponseEntity checkIfPlayerNameExists(@PathVariable String playerName) {
        boolean playerNameExists = playerService.playerNameExists(playerName);
        return playerNameExists ? new ResponseEntity(HttpStatus.CONFLICT) : new ResponseEntity(HttpStatus.OK);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Patch methods ----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PatchMapping("/confirmEmail/{token}")
    public ResponseEntity<PlayerDTO> confirmEmail(@PathVariable String token) throws UsernameNotFoundException {
        String playerName = jwtTokenProvider.getPlayerNameFromToken(token);
        Player player = playerService.confirmEmail(playerName);
        return new ResponseEntity<>(modelMapper.map(player, PlayerDTO.class), HttpStatus.OK);
    }
}


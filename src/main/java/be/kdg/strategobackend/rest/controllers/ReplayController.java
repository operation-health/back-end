package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.game.GameState;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.rest.dto.game.GamePlayerDTO;
import be.kdg.strategobackend.rest.dto.replay.ReplayGameDTO;
import be.kdg.strategobackend.rest.dto.replay.ReplayListItemDTO;
import be.kdg.strategobackend.rest.dto.shared.GameRulesDTO;
import be.kdg.strategobackend.rest.dto.shared.PlayerDTO;
import be.kdg.strategobackend.service.GameService;
import be.kdg.strategobackend.service.exception.GameException;
import be.kdg.strategobackend.service.exception.GamePlayerException;
import be.kdg.strategobackend.service.exception.PawnException;
import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.utility.TurnsToEventsConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/replay")
public class ReplayController {
    private final GameService gameService;
    private final TurnsToEventsConverter turnsToEventsConverter;
    private final ModelMapper modelMapper;

    @Autowired
    public ReplayController(GameService gameService, TurnsToEventsConverter turnsToEventsConverter, ModelMapper modelMapper) {
        this.gameService = gameService;
        this.turnsToEventsConverter = turnsToEventsConverter;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public ResponseEntity<List<ReplayListItemDTO>> getReplays(@AuthenticationPrincipal User user) {
        String playerName = user.getUsername();
        List<Game> games = gameService.loadGamesByPlayerName(playerName);
        List<ReplayListItemDTO> replayListItemDTOS = new ArrayList<>();

        games.forEach(game -> {
            boolean victory = game.getGamePlayers()
                    .stream()
                    .filter(gamePlayer -> !gamePlayer.isComputer() && gamePlayer.getPlayer().getPlayerName().equals(playerName))
                    .noneMatch(GamePlayer::isDead);

            List<GameTurn> gameTurns = turnsToEventsConverter.collectGameTurns(game);
            LocalDateTime lastTurnTime = gameTurns.get(gameTurns.size() - 1).getTime();
            String formattedEndTime = lastTurnTime.format(DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy"));

            replayListItemDTOS.add(new ReplayListItemDTO(game.getId(), game.getBoard().getType(), formattedEndTime, victory));
        });

        return new ResponseEntity<>(replayListItemDTOS, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ReplayGameDTO> getReplay(@PathVariable long id) throws GameException, PawnException, GamePlayerException {
        Game game = gameService.load(id);
        turnsToEventsConverter.resetGamePlayersAndPawns(game);

        List<GamePlayerDTO> gamePlayerDTOS = new ArrayList<>();
        game.getGamePlayers().forEach(gamePlayer -> {
            GamePlayerDTO gamePlayerDTO = modelMapper.map(gamePlayer, GamePlayerDTO.class);
            if (gamePlayer.isComputer()) gamePlayerDTO.setPlayer(new PlayerDTO("AI - " + gamePlayer.getDifficulty()));
            gamePlayerDTOS.add(gamePlayerDTO);
        });

        List<Event> events = turnsToEventsConverter.convertToEvents(game);
        GameRulesDTO gameRulesDTO = modelMapper.map(game.getGameRules(), GameRulesDTO.class);
        ReplayGameDTO replayGameDTO = new ReplayGameDTO(game.getBoard().getType(), GameState.PLAYING, gameRulesDTO, gamePlayerDTOS, events);

        return new ResponseEntity<>(replayGameDTO, HttpStatus.OK);
    }
}

package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.rest.dto.lobby.BoardDTO;
import be.kdg.strategobackend.rest.dto.shared.GameBoardDTO;
import be.kdg.strategobackend.service.BoardService;
import be.kdg.strategobackend.service.LobbyService;
import be.kdg.strategobackend.service.exception.BoardException;
import be.kdg.strategobackend.service.exception.GameException;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/board")
public class BoardController {
    private final LobbyService lobbyService;
    private final BoardService boardService;
    private final ModelMapper modelMapper;

    public BoardController(LobbyService lobbyService, BoardService boardService, ModelMapper modelMapper) {
        this.lobbyService = lobbyService;
        this.boardService = boardService;
        this.modelMapper = modelMapper;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Get methods ------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @GetMapping
    public ResponseEntity<GameBoardDTO> getBoard(@AuthenticationPrincipal User user) throws GameException {
        String playerName = user.getUsername();
        Board board = lobbyService.loadCurrentGameByPlayerName(playerName).getBoard();
        return new ResponseEntity<>(modelMapper.map(board, GameBoardDTO.class), HttpStatus.OK);
    }

    @GetMapping("/{type}")
    public ResponseEntity<GameBoardDTO> getReplayBoard(@PathVariable String type) throws BoardException {
        Board board = boardService.loadByType(type);
        return new ResponseEntity<>(modelMapper.map(board, GameBoardDTO.class), HttpStatus.OK);
    }

    @GetMapping("/boards")
    public ResponseEntity<List<BoardDTO>> getBoards() {
        List<BoardDTO> boardDTOS = boardService.loadBoards()
                .stream()
                .map(board -> modelMapper.map(board, BoardDTO.class))
                .collect(Collectors.toList());

        return new ResponseEntity<>(boardDTOS, HttpStatus.OK);
    }
}

package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.rest.dto.shared.GameMessageDTO;
import be.kdg.strategobackend.rest.dto.shared.PrivateMessageDTO;
import be.kdg.strategobackend.service.LobbyService;
import be.kdg.strategobackend.service.PlayerService;
import be.kdg.strategobackend.service.exception.GameException;
import be.kdg.strategobackend.streaming.SseProcessor;
import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.ChatEventBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("api/message")
public class MessageController {
    private final SseProcessor sseProcessor;
    private final ChatEventBuilder chatEventBuilder;
    private final LobbyService lobbyService;
    private final PlayerService playerService;

    @Autowired
    public MessageController(SseProcessor sseProcessor, ChatEventBuilder chatEventBuilder, LobbyService lobbyService, PlayerService playerService) {
        this.sseProcessor = sseProcessor;
        this.chatEventBuilder = chatEventBuilder;
        this.lobbyService = lobbyService;
        this.playerService = playerService;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Post methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @PostMapping
    public ResponseEntity sendMessage(@AuthenticationPrincipal User user, @RequestBody GameMessageDTO gameMessageDTO) throws GameException {
        Game game = lobbyService.loadCurrentGameByPlayerName(user.getUsername());
        if (game == null) return new ResponseEntity(HttpStatus.NOT_FOUND);

        Event event = chatEventBuilder.createChatGameMessageReceived(user.getUsername(), LocalDateTime.now(), gameMessageDTO.getMessage());
        sseProcessor.push(game, event);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/private")
    public ResponseEntity sendPrivateMessage(@AuthenticationPrincipal User user, @RequestBody PrivateMessageDTO privateMessageDTO) {
        Player targetPlayer = playerService.loadPlayerByPlayerName(privateMessageDTO.getPlayerName());
        Event event = chatEventBuilder.createChatPrivateMessageReceived(user.getUsername(), LocalDateTime.now(), privateMessageDTO.getMessage());
        sseProcessor.push(targetPlayer, event);
        return new ResponseEntity(HttpStatus.OK);
    }
}

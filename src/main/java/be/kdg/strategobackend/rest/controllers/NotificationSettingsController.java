package be.kdg.strategobackend.rest.controllers;

import be.kdg.strategobackend.model.player.NotificationSettings;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.rest.dto.other.NotificationSettingsDTO;
import be.kdg.strategobackend.service.NotificationSettingsService;
import be.kdg.strategobackend.service.PlayerService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/notificationsettings")
public class NotificationSettingsController {
    private final NotificationSettingsService notificationSettingsService;
    private final PlayerService playerService;
    private final ModelMapper modelMapper;

    public NotificationSettingsController(NotificationSettingsService notificationSettingsService, PlayerService playerService, ModelMapper modelMapper) {
        this.notificationSettingsService = notificationSettingsService;
        this.playerService = playerService;
        this.modelMapper = modelMapper;
    }

    @PutMapping
    public ResponseEntity<NotificationSettingsDTO> changeNotificationSettings(@AuthenticationPrincipal User user, @RequestBody NotificationSettingsDTO notificationSettingsDTO) {
        Player player = playerService.loadPlayerByPlayerName(user.getUsername());
        NotificationSettings notificationSettings = modelMapper.map(notificationSettingsDTO, NotificationSettings.class);
        notificationSettings.setId(player.getNotificationSettings().getId());
        NotificationSettings updatedNotificationSettings = notificationSettingsService.save(notificationSettings);
        return new ResponseEntity<>(modelMapper.map(updatedNotificationSettings, NotificationSettingsDTO.class), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<NotificationSettingsDTO> getNotificationSettings(@AuthenticationPrincipal User user) {
        NotificationSettings notificationSettings = playerService.loadPlayerByPlayerName(user.getUsername()).getNotificationSettings();
        return new ResponseEntity<>(modelMapper.map(notificationSettings, NotificationSettingsDTO.class), HttpStatus.OK);
    }
}

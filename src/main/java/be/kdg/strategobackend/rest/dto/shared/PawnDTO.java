package be.kdg.strategobackend.rest.dto.shared;

import be.kdg.strategobackend.model.pawn.Rank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PawnDTO {
    private long id;
    private Rank rank;
    private boolean dead;
    private BoardTileDTO boardTile;
}

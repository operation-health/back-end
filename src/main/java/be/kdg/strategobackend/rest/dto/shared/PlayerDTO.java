package be.kdg.strategobackend.rest.dto.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerDTO {
    private String playerName;
    private String profilePicture;
    private PlayerStatsDTO playerStats;

    public PlayerDTO(String playerName) {
        this.playerName = playerName;
    }
}

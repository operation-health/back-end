package be.kdg.strategobackend.rest.dto.game;

import be.kdg.strategobackend.rest.dto.shared.BoardTileDTO;
import be.kdg.strategobackend.rest.dto.shared.PawnDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameTurnDTO {
    private LocalDateTime date;
    private boolean afkTurn;
    private PawnDTO movedPawn;
    private PawnDTO attackedPawn;
    private BoardTileDTO movedToTile;
    private BoardTileDTO movedFromTile;
}

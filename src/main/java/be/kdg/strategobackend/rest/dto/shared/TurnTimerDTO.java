package be.kdg.strategobackend.rest.dto.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TurnTimerDTO {
    private long turnTimer;
}

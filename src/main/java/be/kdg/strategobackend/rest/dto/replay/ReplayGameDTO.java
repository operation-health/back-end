package be.kdg.strategobackend.rest.dto.replay;

import be.kdg.strategobackend.model.game.GameState;
import be.kdg.strategobackend.rest.dto.game.GameDTO;
import be.kdg.strategobackend.rest.dto.game.GamePlayerDTO;
import be.kdg.strategobackend.rest.dto.shared.GameRulesDTO;
import be.kdg.strategobackend.streaming.event.Event;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ReplayGameDTO extends GameDTO {
    private List<Event> events;

    public ReplayGameDTO(String boardType, GameState gameState, GameRulesDTO gameRules, List<GamePlayerDTO> gamePlayers, List<Event> events) {
        super(boardType, gameState, gameRules, gamePlayers);
        this.events = events;
    }
}

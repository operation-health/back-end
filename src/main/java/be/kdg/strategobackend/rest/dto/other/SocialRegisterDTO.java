package be.kdg.strategobackend.rest.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Bart Wezenbeek
 * @version 1.0 12/02/2019 15:17 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SocialRegisterDTO {
    private String provider;
    private String token;
    private String playerName;
}

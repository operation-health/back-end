package be.kdg.strategobackend.rest.dto.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BoardTileDTO {
    private int tileId;
    private List<Integer> connectedTileIds;
}

package be.kdg.strategobackend.rest.dto.friend;

import be.kdg.strategobackend.rest.dto.other.PlayerNameDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchFriendDTO {
    private Set<PlayerNameDTO> players;
}

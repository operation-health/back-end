package be.kdg.strategobackend.rest.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Bart Wezenbeek
 * @version 1.0 14/02/2019 16:26 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SocialLoginDTO {
    private String token;
    private String provider;
}

package be.kdg.strategobackend.rest.dto.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovePawnDTO {
    private long id;
    private int tileId;
}

package be.kdg.strategobackend.rest.dto.lobby;

import be.kdg.strategobackend.model.gameplayer.Difficulty;
import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.rest.dto.shared.PlayerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LobbyPlayerDTO {
    private Team team;
    private boolean ready;
    private boolean afk;
    private boolean host;
    private boolean computer;
    private Difficulty difficulty;
    private PlayerDTO player;
}

package be.kdg.strategobackend.rest.dto.other;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationSettingsDTO {
    private boolean gameInvite;
    private boolean message;
    private boolean turnPlayed;
    private boolean yourTurn;
    private boolean turnExpired;
}

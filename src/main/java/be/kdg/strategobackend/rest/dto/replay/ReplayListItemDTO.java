package be.kdg.strategobackend.rest.dto.replay;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReplayListItemDTO {
    private long id;
    private String boardType;
    private String endTime;
    private boolean victory;
}

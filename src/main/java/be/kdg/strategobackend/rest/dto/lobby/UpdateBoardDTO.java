package be.kdg.strategobackend.rest.dto.lobby;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateBoardDTO {
    private String boardType; //BOARD-REFACTOR
}

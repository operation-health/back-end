package be.kdg.strategobackend.rest.dto.shared;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerStatsDTO {
    private int gamesPlayed;
    private int gamesWon;
    private boolean publicAccess;
}

package be.kdg.strategobackend.rest.dto.lobby;

import be.kdg.strategobackend.model.game.GameState;
import be.kdg.strategobackend.rest.dto.shared.GameRulesDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LobbyDTO {
    private boolean publicAccess;
    private GameState gameState;
    private LobbyBoardDTO board;
    private GameRulesDTO gameRules;
    private List<LobbyPlayerDTO> gamePlayers;
}

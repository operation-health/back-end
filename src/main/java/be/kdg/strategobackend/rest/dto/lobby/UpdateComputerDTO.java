package be.kdg.strategobackend.rest.dto.lobby;

import be.kdg.strategobackend.model.gameplayer.Team;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateComputerDTO {
    private Team team;
}

package be.kdg.strategobackend.rest.dto.game;

import be.kdg.strategobackend.model.game.GameState;
import be.kdg.strategobackend.rest.dto.shared.GameRulesDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameDTO {
    private String type;
    private GameState gameState;
    private GameRulesDTO gameRules;
    private List<GamePlayerDTO> gamePlayers;
}

package be.kdg.strategobackend.rest.dto.game;

import be.kdg.strategobackend.model.gameplayer.Difficulty;
import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.rest.dto.shared.PawnDTO;
import be.kdg.strategobackend.rest.dto.shared.PlayerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GamePlayerDTO {
    private Team team;
    private boolean ready;
    private boolean dead;
    private boolean afk;
    private boolean computer;
    private boolean currentTurn;
    private LocalDateTime turnStartTime;
    private Difficulty difficulty;
    private PlayerDTO player;
    private List<PawnDTO> pawns;
}

package be.kdg.strategobackend.rest.dto.other;

/**
 * @author Bart Wezenbeek
 * @version 1.0 12/02/2019 15:09 *
 */
public enum SocialProvider {
    FACEBOOK, GOOGLE
}

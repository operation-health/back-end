package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.persistence.BoardTileRepository;
import be.kdg.strategobackend.service.exception.BoardTileException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class BoardTileService {
    private BoardTileRepository repo;

    @Autowired
    public BoardTileService(BoardTileRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public BoardTile save(BoardTile board) {
        return repo.save(board);
    }

    public List<BoardTile> saveAll(List<BoardTile> boardTiles) {
        return repo.saveAll(boardTiles);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public BoardTile load(long id) throws BoardTileException {
        return repo.findById(id).orElseThrow(() -> new BoardTileException("The board tile could not be found."));
    }

    public BoardTile loadByBoardIdAndTileId(long boardId, int tileId) throws BoardTileException {
        return repo.findByBoardIdAndTileId(boardId, tileId).orElseThrow(() -> new BoardTileException("The board tile could not be found."));
    }
}

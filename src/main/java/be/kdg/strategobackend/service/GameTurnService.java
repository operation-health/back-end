package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.persistence.GameTurnRepository;
import be.kdg.strategobackend.service.exception.GameTurnException;
import be.kdg.strategobackend.service.manager.gamelogic.GameTurnTransition;
import be.kdg.strategobackend.service.manager.gamelogic.TurnManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(isolation = Isolation.READ_UNCOMMITTED)
@Service
public class GameTurnService {
    private final GameTurnRepository gameTurnRepository;
    private final TurnManager turnManager;

    @Autowired
    public GameTurnService(GameTurnRepository gameTurnRepository, TurnManager turnManager) {
        this.gameTurnRepository = gameTurnRepository;
        this.turnManager = turnManager;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public GameTurn save(GameTurn gameTurn) {
        return gameTurnRepository.save(gameTurn);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public GameTurn load(long id) throws GameTurnException {
        return gameTurnRepository.findById(id).orElseThrow(() -> new GameTurnException("The game turn could not be found."));
    }

    //------------------------------------------------------------------------------------------------------------------
    //Update methods ---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public GameTurnTransition endTurn(Game game, GameTurn movedGameTurn) throws GameTurnException {
        GameTurnTransition gameTurnTransition = turnManager.endTurn(game, false);
        GameTurn gameTurn = gameTurnTransition.getGameTurn();

        gameTurn.setMovedPawn(movedGameTurn.getMovedPawn());
        gameTurn.setAttackedPawn(movedGameTurn.getAttackedPawn());
        gameTurn.setMovedFromTile(movedGameTurn.getMovedFromTile());
        gameTurn.setMovedToTile(movedGameTurn.getMovedToTile());

        gameTurn = this.save(gameTurn);
        gameTurnTransition.setGameTurn(gameTurn);

        return gameTurnTransition;
    }
}

package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.persistence.PlayerRepository;
import be.kdg.strategobackend.service.exception.EmailAlreadyExistsException;
import be.kdg.strategobackend.service.exception.EmailConfirmationException;
import be.kdg.strategobackend.service.exception.FriendException;
import be.kdg.strategobackend.service.exception.PlayerNameAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class PlayerService implements UserDetailsService {
    private final PlayerRepository repo;
    private final FileService fileService;

    private final PasswordEncoder passwordEncoder;

    private Map<String, Set<String>> friendRequests = new HashMap<>();

    @Autowired
    public PlayerService(PlayerRepository repo, PasswordEncoder passwordEncoder, FileService fileService) {
        this.repo = repo;
        this.passwordEncoder = passwordEncoder;
        this.fileService = fileService;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Player save(Player player) {
        return repo.save(player);
    }

    public Player register(Player player) throws PlayerNameAlreadyExistsException, EmailAlreadyExistsException {
        if (repo.existsByPlayerName(player.getPlayerName()))
            throw new PlayerNameAlreadyExistsException("A player with this playerName already exists.");
        if (repo.existsByEmail(player.getEmail()))
            throw new EmailAlreadyExistsException("A player with this email already exists.");

        player.setPassword(passwordEncoder.encode(player.getPassword()));
        return repo.save(player);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        Player player = repo.findByPlayerNameOrEmail(usernameOrEmail).orElseThrow(() -> new UsernameNotFoundException("No players with that name or email were found."));
        return new User(player.getPlayerName(), player.getPassword(), new ArrayList<>());
    }

    public Player loadPlayerByPlayerName(String playerName) throws UsernameNotFoundException {
        return repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("No players with that name were found."));
    }

    public Set<Player> findPlayersLikePlayerName(String playerName) throws UsernameNotFoundException {
        return new HashSet<>(repo.findPlayersLikePlayerName(playerName));
    }

    public Set<String> getFriendRequests(String playerName) {
        return this.friendRequests.getOrDefault(playerName, new HashSet<>());
    }


    public Player loadPlayerByPlayerNameOrEmail(String playerNameOrEmail) throws UsernameNotFoundException, EmailConfirmationException {
        Player player = repo.findByPlayerNameOrEmail(playerNameOrEmail).orElseThrow(() -> new UsernameNotFoundException("No players with that name or email were found."));
        return player; // TODO: FOR EASE OF USE
//            if (player.isEmailConfirmed()) {
//                return player;
//            }
//            throw new EmailConfirmationException("Email not yet confirmed");
    }

    public Player loadEmail(String playerName) {
        return repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("No players with that name were found."));
    }

    public Player loadPlayerThatHasPassword(String playerNameOrEmail) throws EmailConfirmationException {
        Player player = loadPlayerByPlayerNameOrEmail(playerNameOrEmail);
        if (player.isHasPassword()) return player;
        throw new UsernameNotFoundException("No players with that name or email were found.");
    }

    public List<Player> loadRecentPlayers(String playerName) {
        Player player = repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("The player could not be found."));
        List<Player> recentPlayers = new ArrayList<>();
        Set<Player> friends = player.getFriends();
        player.getGamePlayers()
                .forEach(gamePlayer -> gamePlayer.getGame().getGamePlayers()
                        .stream()
                        .filter(gp -> !gp.isComputer())
                        .filter(gp -> !gp.getPlayer().getPlayerName().equals(playerName) && !friends.contains(gp.getPlayer()))
                        .forEach(gameGp -> {
                            if (recentPlayers.size() != 5) recentPlayers.add(gameGp.getPlayer());
                        }));
        return recentPlayers;
    }

    public boolean playerNameExists(String playerName) {
        return repo.existsByPlayerName(playerName);
    }

    public Resource loadProfilePicture(String playerName) throws MalformedURLException, FileNotFoundException {
        Player player = repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("No players with this name exist."));
        return fileService.load(player.getProfilePicture());
    }

    public boolean hasFriendRequest(String playerName, String invitedPlayerName) {
        Set<String> requests = this.friendRequests.getOrDefault(playerName, new HashSet<>());
        return requests.contains(invitedPlayerName);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Update methods ---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Player confirmEmail(String playerName) {
        Player playerToConfirm = repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("No players with that name exist."));
        playerToConfirm.setEmailConfirmed(true);
        return this.save(playerToConfirm);
    }

    public Player changePassword(String playerName, String newPassword) throws UsernameNotFoundException {
        Player player = repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("No players with that name exist."));
        player.setPassword(passwordEncoder.encode(newPassword));
        return this.save(player);
    }

    public boolean addFriend(String playerName, String friendName) throws UsernameNotFoundException, FriendException {
        Player player = repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("No players with that name exist."));
        Player friend = repo.findByPlayerName(friendName).orElseThrow(() -> new UsernameNotFoundException("No players with that name exist."));

        if (player.getFriends().stream().map(Player::getPlayerName).findFirst().isPresent())
            throw new FriendException("You're already friends with that player!");
        if (this.hasFriendRequest(playerName, friendName))
            throw new FriendException("You've already sent this player a friend request.");

        if (this.hasFriendRequest(friendName, playerName)) {
            player.getFriends().add(friend);
            friend.getFriends().add(player);
            this.save(player);
            this.save(friend);
            this.friendRequests.getOrDefault(playerName, new HashSet<>()).remove(friendName);
            this.friendRequests.getOrDefault(friendName, new HashSet<>()).remove(playerName);
            return true;
        } else {
            Set<String> requests = this.friendRequests.getOrDefault(playerName, new HashSet<>());
            requests.add(friendName);
            this.friendRequests.put(playerName, requests);
            return false;
        }
    }

    public Player changeProfilePicture(String playerName, MultipartFile profilePic) throws IOException {
        Player player = repo.findByPlayerName(playerName)
                .orElseThrow(() -> new UsernameNotFoundException("No players with this name exist."));

        String filename = player.getPlayerName() +
                profilePic.getOriginalFilename()
                        .substring(Objects.requireNonNull(profilePic.getOriginalFilename()).lastIndexOf('.'));
        fileService.save(profilePic, filename);
        player.setProfilePicture(filename);
        return this.save(player);
    }

    public void removeFriend(String playerName, String friendName) {
        Player player = repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("No players with that name exist."));
        Player friend = repo.findByPlayerName(friendName).orElseThrow(() -> new UsernameNotFoundException("No players with that name exist."));
        player.getFriends().remove(friend);
        friend.getFriends().remove(player);
        this.save(friend);
        this.save(player);
    }

    public Player editAccount(String playerName, String email, String password) {
        Player player = repo.findByPlayerName(playerName).orElseThrow(() -> new UsernameNotFoundException("No players with that name were found."));

        if (email != null) player.setEmail(email);
        if (password != null) player.setPassword(password);
        if (password != null) player.setPassword(passwordEncoder.encode(password));

        return this.save(player);
    }
}


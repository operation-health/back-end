package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.persistence.GamePlayerRepository;
import be.kdg.strategobackend.service.exception.GamePlayerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class GamePlayerService {
    private final GamePlayerRepository repo;

    @Autowired
    public GamePlayerService(GamePlayerRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public GamePlayer save(GamePlayer gamePlayer) {
        return repo.save(gamePlayer);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public GamePlayer load(Long id) throws GamePlayerException {
        return repo.findById(id).orElseThrow(() -> new GamePlayerException("The game player could not be found."));
    }

    public GamePlayer loadGamePlayerByName(String playerName) throws GamePlayerException {
        return repo.findGamePlayerByName(playerName).orElseThrow(() -> new GamePlayerException("No game players with that name could be found."));
    }

    public GamePlayer loadGamePlayerByGameIdAndTeam(long gameId, Team team) throws GamePlayerException {
        return repo.findGamePlayerByGameIdAndTeam(gameId, team).orElseThrow(() -> new GamePlayerException("No matching AI has been found"));
    }

    //------------------------------------------------------------------------------------------------------------------
    //Delete methods ---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public void delete(long id) {
        repo.deleteById(id);
    }
}

package be.kdg.strategobackend.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileService {
    private final Path rootLocation = Paths.get("profile-pics");

    @Value("${file.directory}")
    private String directory;

    @PostConstruct
    private void init() throws IOException {
        if (!Files.exists(rootLocation)) Files.createDirectory(rootLocation);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    void save(MultipartFile file, String filename) throws IOException {
        if (file.isEmpty()) throw new FileNotFoundException("File is empty.");

        Files.copy(file.getInputStream(),
                this.rootLocation.resolve(filename),
                StandardCopyOption.REPLACE_EXISTING);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Resource load(String filename) throws MalformedURLException, FileNotFoundException {
        if (filename != null) {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) return resource;
        }
        throw new FileNotFoundException("Could not load file.");
    }
}

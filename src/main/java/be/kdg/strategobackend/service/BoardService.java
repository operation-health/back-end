package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.persistence.BoardRepository;
import be.kdg.strategobackend.service.exception.BoardException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class BoardService {
    private BoardRepository repo;

    @Autowired
    public BoardService(BoardRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Board save(Board board) {
        return repo.save(board);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Board load(Long id) throws BoardException {
        return repo.findById(id).orElseThrow(() -> new BoardException("The board could not be found."));
    }

    public Board loadByType(String boardType) throws BoardException {
        return repo.findByType(boardType).orElseThrow(() -> new BoardException("The board could not be found."));
    }

    public List<Board> loadBoards() {
        return repo.findAll();
    }
}

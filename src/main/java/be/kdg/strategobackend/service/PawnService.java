package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.persistence.PawnRepository;
import be.kdg.strategobackend.service.exception.PawnException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class PawnService {
    private final PawnRepository repo;

    @Autowired
    public PawnService(PawnRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Pawn save(Pawn pawn) {
        return repo.save(pawn);
    }

    public List<Pawn> saveAll(Collection<Pawn> pawns) {
        return repo.saveAll(pawns);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Pawn load(long id) throws PawnException {
        return repo.findById(id).orElseThrow(() -> new PawnException("The pawn could not be found."));
    }

    public Pawn loadByBoardIdAndBoardTileIdAndGameId(long boardId, int tileId, long gameId) {
        return repo.findByBoardIdAndBoardTileIdAndGameId(boardId, tileId, gameId).orElse(null);
    }
}

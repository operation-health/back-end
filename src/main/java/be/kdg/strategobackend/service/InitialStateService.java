package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.game.InitialState;
import be.kdg.strategobackend.persistence.InitialStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class InitialStateService {
    private final InitialStateRepository repo;

    @Autowired
    public InitialStateService(InitialStateRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public InitialState save(InitialState initialState) {
        return repo.save(initialState);
    }
}

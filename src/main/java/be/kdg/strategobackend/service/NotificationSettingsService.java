package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.player.NotificationSettings;
import be.kdg.strategobackend.persistence.NotificationSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class NotificationSettingsService {
    private final NotificationSettingsRepository repo;

    @Autowired
    public NotificationSettingsService(NotificationSettingsRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public NotificationSettings save(NotificationSettings notificationSettings) {
        return repo.save(notificationSettings);
    }
}

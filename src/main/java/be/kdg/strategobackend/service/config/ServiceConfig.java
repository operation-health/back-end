package be.kdg.strategobackend.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Random;

@Configuration
public class ServiceConfig {
    @Bean
    public Random getRandom(){
        return new Random();
    }

}

package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.game.InitialOccupation;
import be.kdg.strategobackend.persistence.InitialOccupationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class InitialOccupationService {
    private final InitialOccupationRepository repo;

    @Autowired
    public InitialOccupationService(InitialOccupationRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public InitialOccupation save(InitialOccupation initialOccupation) {
        return repo.save(initialOccupation);
    }
}

package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.game.*;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.model.player.PlayerStats;
import be.kdg.strategobackend.persistence.GameRepository;
import be.kdg.strategobackend.service.exception.*;
import be.kdg.strategobackend.service.manager.GameManager;
import be.kdg.strategobackend.service.manager.gamelogic.ConstraintManager;
import be.kdg.strategobackend.service.manager.gamelogic.PawnMoveManager;
import be.kdg.strategobackend.service.manager.gamelogic.ai.AIManager;
import be.kdg.strategobackend.service.manager.gamelogic.ai.Node;
import be.kdg.strategobackend.utility.PawnFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class GameService {
    private final GameRepository repo;
    private final GamePlayerService gamePlayerService;
    private final BoardTileService boardTileService;
    private final PawnService pawnService;
    private final PlayerStatsService playerStatsService;
    private final InitialStateService initialStateService;
    private final InitialOccupationService initialOccupationService;

    private final ConstraintManager constraintManager;
    private final PawnMoveManager pawnMoveManager;
    private final PawnFactory pawnFactory;
    private final AIManager aiManager;
    private final GameManager gameManager;

    private final Random random;

    @Autowired
    public GameService(GameRepository repo, GamePlayerService gamePlayerService, BoardTileService boardTileService, PawnService pawnService, PlayerStatsService playerStatsService, InitialStateService initialStateService, InitialOccupationService initialOccupationService, ConstraintManager constraintManager, PawnMoveManager pawnMoveManager, PawnFactory pawnFactory, AIManager aiManager, GameManager gameManager, Random random) {
        this.repo = repo;
        this.gamePlayerService = gamePlayerService;
        this.boardTileService = boardTileService;
        this.pawnService = pawnService;
        this.playerStatsService = playerStatsService;
        this.initialStateService = initialStateService;
        this.initialOccupationService = initialOccupationService;
        this.constraintManager = constraintManager;
        this.pawnMoveManager = pawnMoveManager;
        this.pawnFactory = pawnFactory;
        this.aiManager = aiManager;
        this.gameManager = gameManager;
        this.random = random;

        this.deleteUnfinishedGames();
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Game save(Game game) {
        return repo.save(game);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Game load(long id) throws GameException {
        return repo.findById(id).orElseThrow(() -> new GameException("The game could not be found."));
    }

    public List<Game> loadPreparing() {
        return repo.findGamesByGameState(GameState.PREPARING);
    }

    public List<Game> loadGamesByPlayerName(String playerName) {
        return repo.findGamesByPlayerName(playerName);
    }

    public Game loadCurrentGameByPlayerName(String playerName) throws GameException {
        return repo.findCurrentGameByPlayerName(playerName).orElseThrow(() -> new GameException("The player is not in a game."));
    }

    public long getRemainingTime(String playerName) throws GameException, GamePlayerException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        GameRules gameRules = game.getGameRules();
        if (game.getGameState() == GameState.PREPARING)
            return gameRules.getPreparationTimer() - Duration.between(game.getStartTime(), LocalDateTime.now()).getSeconds();
        else {
            GamePlayer gamePlayer = gameManager.getGamePlayerWithTurn(game);
            if (gamePlayer.isAfk())
                return gameRules.getAfkTimer() - Duration.between(gamePlayer.getTurnStartTime(), LocalDateTime.now()).getSeconds();
            return gameRules.getTurnTimer() - Duration.between(gamePlayer.getTurnStartTime(), LocalDateTime.now()).getSeconds();
        }
    }

    public boolean isEveryoneReady(List<GamePlayer> gamePlayers) {
        return gamePlayers.stream().filter(gp -> !gp.isHost()).allMatch(GamePlayer::isReady);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Update methods ---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public GamePlayer updatePlayerStatus(String playerName, boolean ready) throws GamePlayerException, GameException, InvalidGameStateException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        if (game.getGameState() != GameState.PREPARING)
            throw new InvalidGameStateException("The game is not in the preparing phase.");
        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(playerName);
        gamePlayer.setReady(ready);
        gamePlayerService.save(gamePlayer);
        return gamePlayer;
    }

    public Game startPreparationPhase(String playerName) throws MissingHostException, GameException, GameStartException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        if (!this.isEveryoneReady(game.getGamePlayers()))
            throw new GameStartException("The game could not be started because not everyone is ready.");
        if (!gameManager.isHost(game, playerName))
            throw new GameStartException("The player is not the host.");
        if (game.getGameState() != GameState.LOBBY)
            throw new GameStartException("The game is not in lobby phase.");

        game.getGamePlayers().forEach(gamePlayer -> {
            List<Pawn> pawns = pawnFactory.generatePawns(gamePlayer);
            if (!gamePlayer.isComputer()) gamePlayer.setReady(false);
            gamePlayer.setPawns(pawns);
            pawnService.saveAll(gamePlayer.getPawns());
            gamePlayerService.save(gamePlayer);
        });

        game.getGamePlayers()
                .stream()
                .filter(GamePlayer::isComputer)
                .forEach(gamePlayer -> {
                    aiManager.organisePawns(gamePlayer);
                    pawnService.saveAll(gamePlayer.getPawns());
                    gamePlayerService.save(gamePlayer);
                });

        game.setGameState(GameState.PREPARING);
        game.setStartTime(LocalDateTime.now());

        return this.save(game);
    }

    public Game startPlayingPhase(Game game) throws GameException {
        constraintManager.placeUnorganisedPawns(game);

        GamePlayer gamePlayer = game.getGamePlayers().get(random.nextInt(game.getGamePlayers().size()));
        gamePlayer.setCurrentTurn(true);
        game = this.load(game.getId());

        List<InitialOccupation> initialOccupations = new ArrayList<>();
        game.getGamePlayers().forEach(gp -> gp.getPawns().forEach(pawn -> {
            InitialOccupation initialOccupation = initialOccupationService.save(new InitialOccupation(pawn, pawn.getBoardTile()));
            initialOccupations.add(initialOccupation);
        }));
        InitialState initialState = initialStateService.save(new InitialState(game, initialOccupations));

        game.setInitialState(initialState);
        game.setGameState(GameState.PLAYING);

        gamePlayer.setTurnStartTime(LocalDateTime.now());
        gamePlayerService.save(gamePlayer);

        return this.save(game);
    }

    public Game organisePawn(String playerName, long pawnId, int tileId) throws GamePlayerException, PawnException, InvalidBoardTileException, BoardTileException, GameException, InvalidPawnException, InvalidGameStateException {
        Game game = loadCurrentGameByPlayerName(playerName);

        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(playerName);
        Pawn pawn = pawnService.load(pawnId);

        boolean canOrganise = pawnMoveManager.canOrganise(game, gamePlayer, pawn);

        BoardTile boardTileToMoveTo;
        if (canOrganise) {
            if (tileId != -1) {
                boardTileToMoveTo = boardTileService.loadByBoardIdAndTileId(game.getBoard().getId(), tileId);
                boolean isValidTile = pawnMoveManager.isValidTile(gamePlayer, boardTileToMoveTo, gameManager.isTileOccupied(game.getBoard().getId(), tileId, game.getId()));
                if (isValidTile) pawn.setBoardTile(boardTileToMoveTo);
            } else pawn.setBoardTile(null);
            pawnService.save(pawn);
            gamePlayerService.save(gamePlayer);

            return this.save(game);
        }
        return game;
    }

    public GameTurn movePawnWithAI(GamePlayer gamePlayer) throws PawnException, GameTurnException, InvalidBoardTileException, BoardTileException, InvalidGameStateException, InvalidPawnException {
        Node node = aiManager.movePawn(gamePlayer);
        return this.movePawn(gamePlayer, node.getPawn().getId(), node.getBoardTile().getTileId());
    }

    public GameTurn movePawn(String playerName, long pawnId, int tileId) throws GamePlayerException, PawnException, InvalidGameStateException, InvalidPawnException, BoardTileException, InvalidBoardTileException, GameTurnException {
        GamePlayer gamePlayer = gamePlayerService.loadGamePlayerByName(playerName);
        return this.movePawn(gamePlayer, pawnId, tileId);
    }

    public GameTurn movePawn(GamePlayer gamePlayer, long pawnId, int tileId) throws PawnException, InvalidGameStateException, InvalidPawnException, BoardTileException, InvalidBoardTileException, GameTurnException {
        Game game = gamePlayer.getGame();
        Pawn pawn = pawnService.load(pawnId);

        BoardTile currentBoardTile = pawn.getBoardTile();
        BoardTile boardTileToMoveTo = boardTileService.loadByBoardIdAndTileId(game.getBoard().getId(), tileId);

        boolean canMove = pawnMoveManager.canMove(game, gamePlayer, currentBoardTile, boardTileToMoveTo, pawn);

        Pawn pawnOnTileToMoveTo = pawnService.loadByBoardIdAndBoardTileIdAndGameId(game.getBoard().getId(), boardTileToMoveTo.getTileId(), game.getId());

        if (pawnOnTileToMoveTo != null && pawnMoveManager.canAttack(pawnOnTileToMoveTo, gamePlayer) && canMove) {
            pawnMoveManager.attack(pawn, pawnOnTileToMoveTo, boardTileToMoveTo);
            pawnOnTileToMoveTo = pawnService.save(pawnOnTileToMoveTo);
            if (pawnOnTileToMoveTo.getRank() == Rank.FLAG) {
                GamePlayer deadPlayer = pawnOnTileToMoveTo.getGamePlayer();
                deadPlayer = gamePlayerService.save(deadPlayer);
                pawnService.saveAll(deadPlayer.getPawns());

                for (GamePlayer gp : game.getGamePlayers()) {
                    if (gp.getTeam() == deadPlayer.getTeam()) {
                        gp.setDead(true);
                    }
                }
            }
        } else if (canMove) pawn.setBoardTile(boardTileToMoveTo);

        pawn = pawnService.save(pawn);
        game = this.save(game);

        game.getGamePlayers().forEach(gp -> {
            if (constraintManager.hasOnlyBombsAndFlagLeft(gp)) {
                gp = gamePlayerService.save(gp);
                pawnService.saveAll(gp.getPawns());
            }
        });

        this.save(game);

        pawn = pawnService.load(pawn.getId());
        if (pawnOnTileToMoveTo != null) pawnOnTileToMoveTo = pawnService.load(pawnOnTileToMoveTo.getId());

        return GameTurn.builder()
                .movedPawn(pawn)
                .attackedPawn(pawnOnTileToMoveTo)
                .movedFromTile(currentBoardTile)
                .movedToTile(boardTileToMoveTo)
                .build();
    }

    public Game endGame(Game game) {
        long humanPlayersCount = game.getGamePlayers()
                .stream()
                .filter(gamePlayer -> !gamePlayer.isComputer())
                .count();

        if (humanPlayersCount > 1) {
            game.getGamePlayers().stream()
                    .filter(gamePlayer -> !gamePlayer.isComputer())
                    .forEach(gamePlayer -> {
                        PlayerStats playerStats = gamePlayer.getPlayer().getPlayerStats();

                        int gamesPlayed = playerStats.getGamesPlayed() + 1;
                        int gamesWon = playerStats.getGamesWon();

                        if (!gamePlayer.isDead()) gamesWon++;

                        playerStats.setGamesPlayed(gamesPlayed);
                        playerStats.setGamesWon(gamesWon);

                        playerStatsService.save(playerStats);
                    });
        }

        game.setGameState(GameState.FINISHED);
        return this.save(game);
    }


    private void deleteUnfinishedGames() {
        repo.findGamesByNotGameState(GameState.FINISHED).forEach(repo::delete);
    }
}

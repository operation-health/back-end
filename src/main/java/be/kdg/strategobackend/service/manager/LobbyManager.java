package be.kdg.strategobackend.service.manager;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.service.GamePlayerService;
import be.kdg.strategobackend.service.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LobbyManager {
    private final GamePlayerService gamePlayerService;

    @Autowired
    public LobbyManager(GamePlayerService gamePlayerService) {
        this.gamePlayerService = gamePlayerService;
    }

    public boolean canSendInvite(String playerName, Player playerToInvite, Game game) throws GameException, MailException, HostException, PlayerAlreadyInGameException, MissingHostException {
        int capacity = game.getBoard().getPlayerCount();

        List<GamePlayer> humanGamePlayers = game.getGamePlayers()
                .stream()
                .filter(gp -> !gp.isComputer())
                .collect(Collectors.toList());

        if (playerToInvite.getFriends().stream().noneMatch(player -> player.getPlayerName().equals(playerName)))
            throw new HostException("The player is not a friend of the inviting player.");
        if (humanGamePlayers.stream().anyMatch(gp -> gp.getPlayer().equals(playerToInvite)))
            throw new PlayerAlreadyInGameException("The invited player is already in a game.");
        if (this.isNotHost(game, playerName))
            throw new HostException("The player is not the host.");
        if (humanGamePlayers.size() >= capacity)
            throw new GameException("The game is already full.");

        return true;
    }

    public boolean isNotHost(Game game, String playerName) throws MissingHostException {
        GamePlayer host = this.loadHost(game);
        return !host.getPlayer().getPlayerName().equals(playerName);
    }

    public boolean isEmptyGame(List<GamePlayer> gamePlayers) {
        return gamePlayers.stream().allMatch(gamePlayer -> gamePlayer.isComputer() || gamePlayer.isHost());
    }

    public GamePlayer getComputer(String playerName, Team team, Game game) throws GamePlayerException, MissingHostException, HostException {
        if (this.isNotHost(game, playerName)) throw new HostException("The player is not the host.");
        return game.getGamePlayers().stream().filter(gp -> gp.getTeam() == team && gp.isComputer()).findFirst().orElseThrow(() -> new GamePlayerException("No computer player was found with this team."));
    }

    public GamePlayer getComputer(Game game, String playerName, Team team) throws GamePlayerException, HostException, MissingHostException {
        if (this.isNotHost(game, playerName)) throw new HostException("The player is not the host");
        return gamePlayerService.loadGamePlayerByGameIdAndTeam(game.getId(), team);
    }

    public boolean isAddable(Game game, String playerName) {
        return game.getGamePlayers().stream().allMatch(gamePlayer -> gamePlayer.isComputer() || !gamePlayer.getPlayer().getPlayerName().equals(playerName));
    }

    public boolean isValidValue(long value) {
        return 0 < value && value <= 600;
    }

    private GamePlayer loadHost(Game game) throws MissingHostException {
        return game.getGamePlayers().stream().filter(GamePlayer::isHost).findFirst().orElseThrow(() -> new MissingHostException("The game does not contain a host."));
    }
}

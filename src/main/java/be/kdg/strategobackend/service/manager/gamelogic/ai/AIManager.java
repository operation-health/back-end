package be.kdg.strategobackend.service.manager.gamelogic.ai;

import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.service.exception.PawnException;
import org.springframework.stereotype.Component;

@Component
public class AIManager {
    private final AIOrganiseManager aiOrganiseManager;
    private final AIMoveManager aiMoveManager;

    public AIManager(AIOrganiseManager aiOrganiseManager, AIMoveManager aiMoveManager) {
        this.aiOrganiseManager = aiOrganiseManager;
        this.aiMoveManager = aiMoveManager;
    }

    public void organisePawns(GamePlayer gamePlayer) {
        aiOrganiseManager.organisePawns(gamePlayer);
    }

    public Node movePawn(GamePlayer gamePlayer) throws PawnException {
        return aiMoveManager.movePawn(gamePlayer);
    }


}

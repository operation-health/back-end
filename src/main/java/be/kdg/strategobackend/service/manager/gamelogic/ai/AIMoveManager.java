package be.kdg.strategobackend.service.manager.gamelogic.ai;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.service.exception.PawnException;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class AIMoveManager {
    Node movePawn(GamePlayer gamePlayer) throws PawnException {
        Map<Node, Integer> nodeValues = new HashMap<>();
        List<Pawn> allPawns = new ArrayList<>();

        gamePlayer.getGame().getGamePlayers().forEach(gp -> allPawns.addAll(gp.getPawns()));

        List<Pawn> gamePlayerPawns = this.getOwnPawns(gamePlayer, allPawns);
        Collections.shuffle(gamePlayerPawns);

        for (Pawn pawn : gamePlayerPawns) {
            for (BoardTile boardTile : pawn.getBoardTile().getConnectedTiles()) {
                Optional<Pawn> pawnOnBoardTile = this.getPawnOnBoardTile(boardTile, allPawns);
                if (!pawnOnBoardTile.isPresent() || pawnOnBoardTile.get().getGamePlayer().getTeam() != gamePlayer.getTeam()) {
                    Node node = new Node(boardTile, pawn);
                    int value = this.alphaBetaPruning(node, gamePlayer.getDifficulty().getDifficulty(), Integer.MIN_VALUE, Integer.MAX_VALUE, true, allPawns);
                    nodeValues.put(node, value);
                }
            }
        }

        return nodeValues.entrySet()
                .stream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .orElseThrow(() -> new PawnException("The AI has no pawns that can be moved."))
                .getKey();
    }

    private int alphaBetaPruning(Node node, int depth, double alpha, double beta, boolean maximizingPlayer, List<Pawn> allPawns) {
        int value;
        Optional<Pawn> pawnToAttack = this.getPawnOnBoardTile(node.getBoardTile(), allPawns);
        if (pawnToAttack.isPresent() && pawnToAttack.get().getGamePlayer().getTeam() != node.getPawn().getGamePlayer().getTeam())
            return this.valueFunction(pawnToAttack.get(), node.getPawn(), depth);
        else if (depth == 0) return 0;
        else if (maximizingPlayer) {
            value = Integer.MIN_VALUE;
            for (BoardTile connectedTile : this.getConnectedTilesOfNode(node, allPawns)) {
                value = Math.max(value, this.alphaBetaPruning(new Node(connectedTile, node.getPawn()), depth - 1, alpha, beta, false, allPawns));
                alpha = Math.max(alpha, value);
                if (alpha >= beta) break;
            }
            value *= 1.2;
            return value;
        } else {
            value = Integer.MAX_VALUE;
            for (BoardTile connectedTile : this.getConnectedTilesOfNode(node, allPawns)) {
                value = Math.min(value, this.alphaBetaPruning(new Node(connectedTile, node.getPawn()), depth - 1, alpha, beta, true, allPawns));
                beta = Math.min(beta, value);
                if (beta >= alpha) {
                    break;
                }
            }
            return value;
        }
    }

    private int valueFunction(Pawn pawnToAttack, Pawn attackingPawn, int depth) {
        if (pawnToAttack.getRank() == Rank.FLAG) return 2000 * depth;
        else if (attackingPawn.getRank() == Rank.SPY && pawnToAttack.getRank() == Rank.MARSHAL ||
                attackingPawn.getRank() == Rank.MARSHAL && pawnToAttack.getRank() == Rank.SPY)
            return Rank.MARSHAL.getRank() * 1000 * depth;
        else return (attackingPawn.getRank().getRank() * 100 - pawnToAttack.getRank().getRank() * 100) * depth;
    }

    private Optional<Pawn> getPawnOnBoardTile(BoardTile boardTile, List<Pawn> pawns) {
        return pawns.stream()
                .filter(pawn -> pawn.getBoardTile() != null && pawn.getBoardTile().equals(boardTile))
                .findFirst();
    }

    private List<Pawn> getOwnPawns(GamePlayer gamePlayer, List<Pawn> allPawns) {
        return gamePlayer.getPawns()
                .stream()
                .filter(pawn -> pawn.getBoardTile() != null)
                .filter(pawn -> pawn.getBoardTile().getConnectedTiles()
                        .stream()
                        .anyMatch(boardTile -> !getPawnOnBoardTile(boardTile, allPawns).isPresent() ||
                                getPawnOnBoardTile(boardTile, allPawns).get().getGamePlayer().getTeam() != gamePlayer.getTeam()))
                .filter(pawn -> pawn.getRank() != Rank.BOMB && pawn.getRank() != Rank.FLAG)
                .collect(Collectors.toList());
    }

    private List<BoardTile> getConnectedTilesOfNode(Node node, List<Pawn> allPawns) {
        return node.getBoardTile().getConnectedTiles()
                .stream()
                .filter(boardTile -> !this.getPawnOnBoardTile(boardTile, allPawns).isPresent() || this.getPawnOnBoardTile(boardTile, allPawns).get().getGamePlayer().getTeam() != node.getPawn().getGamePlayer().getTeam())
                .collect(Collectors.toList());
    }
}

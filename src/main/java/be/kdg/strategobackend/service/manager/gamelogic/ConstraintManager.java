package be.kdg.strategobackend.service.manager.gamelogic;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.service.GamePlayerService;
import be.kdg.strategobackend.service.PawnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class ConstraintManager {
    private final PawnService pawnService;
    private final GamePlayerService gamePlayerService;

    private final Random random;

    @Autowired
    public ConstraintManager(PawnService pawnService, GamePlayerService gamePlayerService, Random random) {
        this.pawnService = pawnService;
        this.gamePlayerService = gamePlayerService;
        this.random = random;
    }

    public void placeUnorganisedPawns(Game game) {
        game.getGamePlayers().forEach(gamePlayer -> {
            List<Pawn> unorganisedPawns = this.getUnorganisedPawns(gamePlayer);
            List<BoardTile> unoccupiedTeamTiles = this.getUnorganisedTiles(game, gamePlayer);
            this.placePawns(unorganisedPawns, unoccupiedTeamTiles);
            gamePlayerService.save(gamePlayer);
        });
    }

    public boolean hasOnlyBombsAndFlagLeft(GamePlayer gamePlayer) {
        boolean onlyBombsAndFlag = gamePlayer.getPawns()
                .stream()
                .filter(pawn -> !pawn.isDead())
                .allMatch(pawn -> pawn.getRank() == Rank.BOMB || pawn.getRank() == Rank.FLAG);

        if (onlyBombsAndFlag) {
            gamePlayer.setDead(true);
            gamePlayer.getPawns().forEach(pawn -> pawn.setBoardTile(null));
        }

        return onlyBombsAndFlag;
    }

    private List<Pawn> getUnorganisedPawns(GamePlayer gamePlayer) {
        return gamePlayer.getPawns()
                .stream()
                .filter(pawn -> pawn.getBoardTile() == null)
                .collect(Collectors.toList());
    }

    private List<BoardTile> getUnorganisedTiles(Game game, GamePlayer gamePlayer) {
        return game.getBoard().getBoardTiles()
                .stream()
                .filter(boardTile -> !this.isTileOccupied(game.getBoard().getId(), boardTile.getTileId(), game.getId()))
                .filter(boardTile -> boardTile.getTeam() == gamePlayer.getTeam())
                .collect(Collectors.toList());
    }

    private void placePawns(List<Pawn> unorganisedPawns, List<BoardTile> unoccupiedTeamTiles) {
        unorganisedPawns.forEach(pawn -> {
            int index = random.nextInt(unoccupiedTeamTiles.size());
            pawn.setBoardTile(unoccupiedTeamTiles.get(index));
            pawnService.save(pawn);
            unoccupiedTeamTiles.remove(index);
        });
    }

    private boolean isTileOccupied(long boardId, int tileId, long gameId) {
        return pawnService.loadByBoardIdAndBoardTileIdAndGameId(boardId, tileId, gameId) != null;
    }
}

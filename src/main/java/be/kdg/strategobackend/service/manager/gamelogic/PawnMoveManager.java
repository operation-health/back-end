package be.kdg.strategobackend.service.manager.gamelogic;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.game.GameState;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.service.exception.GameTurnException;
import be.kdg.strategobackend.service.exception.InvalidBoardTileException;
import be.kdg.strategobackend.service.exception.InvalidGameStateException;
import be.kdg.strategobackend.service.exception.InvalidPawnException;
import org.springframework.stereotype.Component;

@Component
public class PawnMoveManager {
    public boolean canMove(Game game, GamePlayer gamePlayer, BoardTile currentBoardTile, BoardTile boardTileToMoveTo, Pawn pawn) throws InvalidGameStateException, InvalidPawnException, InvalidBoardTileException, GameTurnException {
        if (game.getGameState() != GameState.PLAYING)
            throw new InvalidGameStateException("The game is not in the playing phase.");
        if (gamePlayer.getPawns().stream().noneMatch(p -> p.getId() == pawn.getId()))
            throw new InvalidPawnException("The pawn does not belong to the playing player.");
        if (pawn.getRank() == Rank.BOMB || pawn.getRank() == Rank.FLAG)
            throw new InvalidPawnException("The pawn cannot be moved because it is a " + (pawn.getRank() == Rank.BOMB ? "bomb" : "flag") + ".");
        if (!currentBoardTile.getConnectedTiles().contains(boardTileToMoveTo))
            throw new InvalidBoardTileException("The tile is not connected to the original tile.");
        if (!gamePlayer.isCurrentTurn() || gamePlayer.isDead())
            throw new GameTurnException("The game player does not have a valid turn.");

        return true;
    }

    public void attack(Pawn attackingPawn, Pawn defendingPawn, BoardTile boardTileToMoveTo) {
        if (attackingPawn.getRank() == Rank.MINER && defendingPawn.getRank() == Rank.BOMB) {
            attackingPawn.setBoardTile(boardTileToMoveTo);
            defendingPawn.setDead(true);
            defendingPawn.setBoardTile(null);
        } else if (attackingPawn.getRank() == Rank.SPY && defendingPawn.getRank() == Rank.MARSHAL) {
            attackingPawn.setBoardTile(boardTileToMoveTo);
            defendingPawn.setDead(true);
            defendingPawn.setBoardTile(null);
        } else if (defendingPawn.getRank() == Rank.BOMB || attackingPawn.getRank().getRank() == defendingPawn.getRank().getRank()) {
            attackingPawn.setDead(true);
            attackingPawn.setBoardTile(null);
            defendingPawn.setDead(true);
            defendingPawn.setBoardTile(null);
        } else if (defendingPawn.getRank() == Rank.FLAG) {
            attackingPawn.setBoardTile(boardTileToMoveTo);
            defendingPawn.setDead(true);
            defendingPawn.setBoardTile(null);
            defendingPawn.getGamePlayer().setDead(true);
            defendingPawn.getGamePlayer().getPawns().forEach(pawn -> pawn.setBoardTile(null));
        } else if (attackingPawn.getRank().getRank() > defendingPawn.getRank().getRank()) {
            attackingPawn.setBoardTile(boardTileToMoveTo);
            defendingPawn.setDead(true);
            defendingPawn.setBoardTile(null);
        } else if (attackingPawn.getRank().getRank() < defendingPawn.getRank().getRank()) {
            attackingPawn.setDead(true);
            attackingPawn.setBoardTile(null);
        }
    }

    public boolean canAttack(Pawn pawnOnTileToMoveTo, GamePlayer gamePlayer) throws InvalidBoardTileException {
        if (pawnOnTileToMoveTo.getGamePlayer().equals(gamePlayer))
            throw new InvalidBoardTileException("The pawn is a pawn of the player's own team.");
        return true;
    }

    public boolean canOrganise(Game game, GamePlayer gamePlayer, Pawn pawn) throws InvalidGameStateException, InvalidPawnException {
        if (game.getGameState() != GameState.PREPARING)
            throw new InvalidGameStateException("The game is not in the preparing phase.");
        if (gamePlayer.getPawns().stream().noneMatch(p -> p.equals(pawn)))
            throw new InvalidPawnException("The pawn does not belong to the playing player.");
        return true;
    }

    public boolean isValidTile(GamePlayer gamePlayer, BoardTile boardTileToMoveTo, boolean isOccupied) throws InvalidBoardTileException {
        if (gamePlayer.getTeam() != boardTileToMoveTo.getTeam())
            throw new InvalidBoardTileException("The tile does not belong to the player's team.");
        if (isOccupied)
            throw new InvalidBoardTileException("A pawn already occupies the tile.");
        return true;
    }
}

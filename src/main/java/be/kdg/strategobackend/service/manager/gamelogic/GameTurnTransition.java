package be.kdg.strategobackend.service.manager.gamelogic;

import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameTurnTransition {
    private GameTurn gameTurn;
    private GamePlayer nextPlayer;
}

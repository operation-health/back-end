package be.kdg.strategobackend.service.manager.gamelogic.ai;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.pawn.Pawn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Node {
    private BoardTile boardTile;
    private Pawn pawn;
}

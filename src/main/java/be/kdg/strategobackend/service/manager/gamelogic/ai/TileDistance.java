package be.kdg.strategobackend.service.manager.gamelogic.ai;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TileDistance implements Comparable<TileDistance> {
    private int tileId;
    private int distance;

    @Override
    public int compareTo(TileDistance o) {
        return Integer.compare(this.distance, o.distance);
    }

    @Override
    public String toString() {
        return "TileDistance{" + "tileId=" + tileId + ", distance=" + distance + '}';
    }
}

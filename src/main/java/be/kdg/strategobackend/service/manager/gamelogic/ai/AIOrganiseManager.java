package be.kdg.strategobackend.service.manager.gamelogic.ai;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.Difficulty;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.service.exception.PawnException;
import be.kdg.strategobackend.utility.PawnFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class AIOrganiseManager {
    private final PawnFactory pawnFactory;
    private final Random random;

    private List<BoardTile> occupiedTiles;
    private List<BoardTile> remainingTeamTiles;

    public AIOrganiseManager(PawnFactory pawnFactory, Random random) {
        this.pawnFactory = pawnFactory;
        this.random = random;
        occupiedTiles = new ArrayList<>();
        remainingTeamTiles = new ArrayList<>();
    }

    void organisePawns(GamePlayer gamePlayer) {
        Game game = gamePlayer.getGame();
        remainingTeamTiles = this.getTeamTiles(game, gamePlayer);
        try {
            this.placePawns(gamePlayer.getPawns(), gamePlayer.getDifficulty());
        } catch (PawnException e) {
            gamePlayer.setPawns(pawnFactory.generatePawns(gamePlayer));
            this.organisePawns(gamePlayer);
        }
    }

    private List<BoardTile> getTeamTiles(Game game, GamePlayer gamePlayer) {
        return game.getBoard().getBoardTiles()
                .stream()
                .filter(boardTile -> boardTile.getTeam() == gamePlayer.getTeam())
                .collect(Collectors.toList());
    }

    private void placePawns(List<Pawn> pawns, Difficulty difficulty) throws PawnException {
        this.setFlagTile(pawns, difficulty);
        this.setBombTiles(pawns, difficulty);
        this.setMarshallTile(pawns, difficulty);
        this.setSpyTile(pawns, difficulty);
        this.setGeneralTile(pawns, difficulty);
        this.setScoutTiles(pawns, difficulty);
        this.setColonelTiles(pawns, difficulty);
        this.setMajorTiles(pawns, difficulty);

        this.setLowerRankTiles(pawns, Rank.CAPTAIN);
        this.setLowerRankTiles(pawns, Rank.LIEUTENANT);
        this.setLowerRankTiles(pawns, Rank.SERGEANT);
        this.setLowerRankTiles(pawns, Rank.MINER);

        occupiedTiles = new ArrayList<>();
    }

    private void setFlagTile(List<Pawn> pawns, Difficulty difficulty) throws PawnException {
        Pawn flag = this.findPawn(pawns, Rank.FLAG);
        List<BoardTile> validTiles = this.getValidFlagTiles(difficulty);

        this.setTileForPawn(flag, validTiles);
        this.removeTeamTile(flag);
    }

    private List<BoardTile> getValidFlagTiles(Difficulty difficulty) {
        List<BoardTile> tiles = this.getFrontLineTiles();
        List<TileDistance> tileDistances = this.getInitialDistances(tiles);
        List<Integer> distances = this.calculateDistances(tiles, tileDistances);

        return this.filterValidFlagTiles(difficulty, tileDistances, distances);
    }

    private List<BoardTile> filterValidFlagTiles(Difficulty difficulty, List<TileDistance> tileDistances, List<Integer> distances) {
        int minimumDistance = Collections.min(distances);
        int maximumDistance = Collections.max(distances);

        if (difficulty == Difficulty.EASY) return this.filterEasyFlagTiles(minimumDistance, tileDistances);
        else if (difficulty == Difficulty.MEDIUM) return this.filterMediumFlagTiles(maximumDistance, tileDistances);
        else if (difficulty == Difficulty.HARD) return this.filterHardFlagTiles(maximumDistance, tileDistances);
        else return this.filterMasterFlagTiles(maximumDistance, tileDistances);
    }

    private List<BoardTile> filterEasyFlagTiles(int minimumDistance, List<TileDistance> tileDistances) {
        List<BoardTile> validTiles = new ArrayList<>();

        tileDistances.stream()
                .filter(tileDistance -> tileDistance.getDistance() > minimumDistance)
                .forEach(tileDistance -> remainingTeamTiles
                        .forEach(teamTile -> {
                            if (teamTile.getTileId() == tileDistance.getTileId())
                                this.addToValidTiles(teamTile, validTiles);
                        }));

        return validTiles;
    }

    private List<BoardTile> filterMediumFlagTiles(int maximumDistance, List<TileDistance> tileDistances) {
        List<BoardTile> validTiles = new ArrayList<>();

        tileDistances.stream()
                .filter(tileDistance -> tileDistance.getDistance() > maximumDistance / 2)
                .forEach(tileDistance -> remainingTeamTiles
                        .forEach(teamTile -> {
                            if (teamTile.getTileId() == tileDistance.getTileId())
                                this.addToValidTiles(teamTile, validTiles);
                        }));

        return validTiles;
    }

    private List<BoardTile> filterHardFlagTiles(int maximumDistance, List<TileDistance> tileDistances) {
        List<BoardTile> validTiles = new ArrayList<>();

        tileDistances.stream()
                .filter(tileDistance -> tileDistance.getDistance() >= maximumDistance - 1)
                .forEach(tileDistance -> remainingTeamTiles
                        .forEach(teamTile -> {
                            if (teamTile.getTileId() == tileDistance.getTileId())
                                this.addToValidTiles(teamTile, validTiles);
                        }));

        return validTiles;
    }

    private List<BoardTile> filterMasterFlagTiles(int maximumDistance, List<TileDistance> tileDistances) {
        List<BoardTile> validTiles = new ArrayList<>();

        tileDistances.stream()
                .filter(tileDistance -> tileDistance.getDistance() == maximumDistance)
                .forEach(tileDistance -> remainingTeamTiles
                        .forEach(teamTile -> {
                            if (teamTile.getTileId() == tileDistance.getTileId())
                                this.addToValidTiles(teamTile, validTiles);
                        }));

        return validTiles;
    }

    private void setBombTiles(List<Pawn> pawns, Difficulty difficulty) throws PawnException {
        Pawn flag = this.findPawn(pawns, Rank.FLAG);
        List<Pawn> bombs = this.findPawns(pawns, Rank.BOMB);
        BoardTile flagTile = flag.getBoardTile();
        List<BoardTile> validTiles;

        if (difficulty == Difficulty.EASY) validTiles = this.filterValidTilesBasedOnOtherPosition(4, flagTile);
        else if (difficulty == Difficulty.MEDIUM) validTiles = this.filterValidTilesBasedOnOtherPosition(3, flagTile);
        else if (difficulty == Difficulty.HARD) validTiles = this.filterValidTilesBasedOnOtherPosition(1, flagTile);
        else validTiles = this.filterValidTilesBasedOnOtherPosition(2, flagTile);

        bombs.forEach(bomb -> this.setTileForPawn(bomb, validTiles));
        bombs.forEach(this::removeTeamTile);
    }

    private void setMarshallTile(List<Pawn> pawns, Difficulty difficulty) throws PawnException {
        Pawn marshall = this.findPawn(pawns, Rank.MARSHAL);
        List<BoardTile> validTiles = this.getValidMarshallTiles(difficulty);

        this.setTileForPawn(marshall, validTiles);
        this.removeTeamTile(marshall);
    }

    private List<BoardTile> getValidMarshallTiles(Difficulty difficulty) {
        List<BoardTile> tiles = this.getFrontLineTiles();
        List<TileDistance> tileDistances = this.getInitialDistances(tiles);
        List<Integer> distances = this.calculateDistances(tiles, tileDistances);

        return this.filterValidMarshallTiles(difficulty, tileDistances, distances);
    }

    private List<BoardTile> filterValidMarshallTiles(Difficulty difficulty, List<TileDistance> tileDistances, List<Integer> distances) {
        int minimumDistance = Collections.min(distances);

        if (difficulty == Difficulty.EASY) return this.filterMarshallTiles(minimumDistance, 3, tileDistances);
        else if (difficulty == Difficulty.MEDIUM) return this.filterMarshallTiles(minimumDistance, 2, tileDistances);
        else if (difficulty == Difficulty.HARD) return this.filterMarshallTiles(minimumDistance, 1, tileDistances);
        else return this.filterMarshallTiles(minimumDistance, 0, tileDistances);
    }

    private List<BoardTile> filterMarshallTiles(int minimumDistance, int factor, List<TileDistance> tileDistances) {
        List<BoardTile> validTiles = new ArrayList<>();

        tileDistances.stream()
                .filter(tileDistance -> tileDistance.getDistance() <= minimumDistance + factor)
                .forEach(tileDistance -> remainingTeamTiles
                        .forEach(teamTile -> {
                            if (teamTile.getTileId() == tileDistance.getTileId())
                                this.addToValidTiles(teamTile, validTiles);
                        }));

        return validTiles;
    }

    private void setSpyTile(List<Pawn> pawns, Difficulty difficulty) throws PawnException {
        Pawn spy = this.findPawn(pawns, Rank.SPY);
        List<BoardTile> validTiles;

        if (difficulty == Difficulty.EASY) validTiles = new ArrayList<>(remainingTeamTiles);
        else if (difficulty == Difficulty.MEDIUM) {
            Pawn flag = this.findPawn(pawns, Rank.MARSHAL);
            BoardTile flagTile = flag.getBoardTile();
            validTiles = this.filterValidTilesBasedOnOtherPosition(1, flagTile);
        } else if (difficulty == Difficulty.HARD) {
            Pawn marshall = this.findPawn(pawns, Rank.MARSHAL);
            BoardTile marshallTile = marshall.getBoardTile();
            validTiles = this.filterValidTilesBasedOnOtherPosition(1, marshallTile);
        } else validTiles = this.getFirstTwoRows();

        this.setTileForPawn(spy, validTiles);
        this.removeTeamTile(spy);
    }

    private void setGeneralTile(List<Pawn> pawns, Difficulty difficulty) throws PawnException {
        Pawn general = this.findPawn(pawns, Rank.GENERAL);
        List<BoardTile> validTiles;

        if (difficulty == Difficulty.EASY) validTiles = this.getFrontLineTiles();
        else if (difficulty == Difficulty.MEDIUM) {
            Pawn flag = this.findPawn(pawns, Rank.FLAG);
            BoardTile flagTile = flag.getBoardTile();
            validTiles = this.filterValidTilesBasedOnOtherPosition(1, flagTile);
        } else if (difficulty == Difficulty.HARD) {
            Pawn flag = this.findPawn(pawns, Rank.FLAG);
            BoardTile flagTile = flag.getBoardTile();
            validTiles = this.filterValidTilesBasedOnOtherPosition(2, flagTile);
        } else {
            Pawn spy = this.findPawn(pawns, Rank.SPY);
            BoardTile spyTile = spy.getBoardTile();
            validTiles = this.filterValidTilesBasedOnOtherPosition(1, spyTile);
        }

        this.setTileForPawn(general, validTiles);
        this.removeTeamTile(general);
    }

    private void setScoutTiles(List<Pawn> pawns, Difficulty difficulty) {
        List<Pawn> scouts = this.findPawns(pawns, Rank.SCOUT);
        List<BoardTile> validTiles;

        if (difficulty == Difficulty.EASY) {
            validTiles = this.getFrontLineTiles();
            scouts.forEach(scout -> this.setTileForPawn(scout, validTiles));
        } else if (difficulty == Difficulty.MEDIUM) {
            validTiles = this.getFirstTwoRows();
            scouts.forEach(scout -> this.setTileForPawn(scout, validTiles));
        } else if (difficulty == Difficulty.HARD) this.setScoutsInFrontLine(scouts, 3);
        else this.setScoutsInFrontLine(scouts, 2);

        scouts.forEach(this::removeTeamTile);
    }

    private void setScoutsInFrontLine(List<Pawn> scouts, int amountInFrontLine) {
        List<BoardTile> validTiles = this.getFrontLineTiles();
        for (int i = 0; i < amountInFrontLine; i++) this.setTileForPawn(scouts.get(i), validTiles);

        validTiles = this.getSecondRow();
        for (int i = amountInFrontLine; i < scouts.size(); i++) this.setTileForPawn(scouts.get(i), validTiles);
    }

    private void setColonelTiles(List<Pawn> pawns, Difficulty difficulty) {
        List<Pawn> colonels = this.findPawns(pawns, Rank.COLONEL);
        List<BoardTile> validTiles;

        if (difficulty == Difficulty.HARD || difficulty == Difficulty.MASTER) validTiles = this.getSecondRow();
        else validTiles = new ArrayList<>(remainingTeamTiles);

        colonels.forEach(colonel -> this.setTileForPawn(colonel, validTiles));
        colonels.forEach(this::removeTeamTile);
    }

    private void setMajorTiles(List<Pawn> pawns, Difficulty difficulty) {
        List<Pawn> colonels = this.findPawns(pawns, Rank.COLONEL);
        List<Pawn> majors = this.findPawns(pawns, Rank.MAJOR);
        List<BoardTile> validTiles;

        if (difficulty != Difficulty.MASTER) validTiles = new ArrayList<>(remainingTeamTiles);
        else {
            validTiles = new ArrayList<>();
            List<BoardTile> secondRow = this.getSecondRow();
            List<BoardTile> colonelTiles = colonels.stream()
                    .map(Pawn::getBoardTile)
                    .collect(Collectors.toList());

            colonelTiles.forEach(colonelTile ->
                    colonelTile.getConnectedTiles().forEach(connectedTile -> {
                        secondRow.forEach(secondRowTile -> {
                            if (secondRowTile.getTileId() == connectedTile.getTileId())
                                this.addToValidTiles(connectedTile, validTiles);
                        });
                    }));
        }

        majors.forEach(major -> this.setTileForPawn(major, validTiles));
        majors.forEach(this::removeTeamTile);
    }

    private void setLowerRankTiles(List<Pawn> pawns, Rank rank) {
        List<Pawn> rankPawns = this.findPawns(pawns, rank);
        List<BoardTile> validTiles = new ArrayList<>(remainingTeamTiles);

        rankPawns.forEach(rankPawn -> this.setTileForPawn(rankPawn, validTiles));
        rankPawns.forEach(this::removeTeamTile);
    }

    private Pawn findPawn(List<Pawn> pawns, Rank rank) throws PawnException {
        return pawns.stream()
                .filter(pawn -> pawn.getRank() == rank)
                .findFirst()
                .orElseThrow(() -> new PawnException("The computer does not own a " + rank + "."));
    }

    private List<Pawn> findPawns(List<Pawn> pawns, Rank rank) {
        return pawns.stream()
                .filter(pawn -> pawn.getRank() == rank)
                .collect(Collectors.toList());
    }

    private List<BoardTile> getFrontLineTiles() {
        return remainingTeamTiles.stream()
                .filter(tile -> tile.getConnectedTiles()
                        .stream()
                        .anyMatch(connectedTile -> connectedTile.getTeam() == null))
                .collect(Collectors.toList());
    }

    private List<TileDistance> getInitialDistances(List<BoardTile> tiles) {
        return tiles.stream()
                .map(frontLineTile -> new TileDistance(frontLineTile.getTileId(), 1))
                .collect(Collectors.toList());
    }

    private List<Integer> calculateDistances(List<BoardTile> tiles, List<TileDistance> tileDistances) {
        while (tiles.size() != 0) tiles = this.getNextTiles(tiles, tileDistances);

        List<Integer> distances = new ArrayList<>();
        tileDistances.forEach(tileDistance -> {
            if (distances.stream().noneMatch(d -> d == tileDistance.getDistance()))
                distances.add(tileDistance.getDistance());
        });

        return distances;
    }

    private List<BoardTile> getNextTiles(List<BoardTile> tiles, List<TileDistance> tileDistances) {
        List<BoardTile> nextTiles = new ArrayList<>();

        tiles.forEach(tile -> tile.getConnectedTiles().forEach(connectedTile -> {
            if (connectedTile.getTeam() != null && tileDistances.stream().noneMatch(tileDistance -> tileDistance.getTileId() == connectedTile.getTileId())) {
                nextTiles.add(connectedTile);
                int distance = tileDistances.stream()
                        .filter(tileDistance -> tile.getTileId() == tileDistance.getTileId())
                        .findFirst()
                        .orElse(new TileDistance(-1, -1))
                        .getDistance();
                if (distance != -1)
                    tileDistances.add(new TileDistance(connectedTile.getTileId(), ++distance));
            }
        }));

        return nextTiles;
    }

    private List<BoardTile> filterValidTilesBasedOnOtherPosition(int distance, BoardTile tile) {
        List<BoardTile> validTiles = new ArrayList<>();
        List<BoardTile> tiles = new ArrayList<>();
        tiles.add(tile);

        for (int i = 0; tiles.size() != 0 && i < distance; i++) {
            tiles = this.getNextTilesWithoutDistanceCalculation(tiles);
            tiles.forEach(nextTile -> this.addToValidTiles(nextTile, validTiles));
        }

        return validTiles;
    }

    private List<BoardTile> getNextTilesWithoutDistanceCalculation(List<BoardTile> tiles) {
        List<BoardTile> nextTiles = new ArrayList<>();
        tiles.forEach(tile -> nextTiles.addAll(tile.getConnectedTiles()));

        return nextTiles;
    }

    private void setTileForPawn(Pawn pawn, List<BoardTile> tiles) {
        if (tiles.size() == 0) this.setTileLastResort(pawn);
        else if (!this.setTileOnAlgorithmTile(pawn, tiles)) this.setTileLastResort(pawn);
    }

    private boolean setTileOnAlgorithmTile(Pawn pawn, List<BoardTile> tiles) {
        int i = 0;

        while (pawn.getBoardTile() == null && i < 100) {
            int index = random.nextInt(tiles.size());
            if (this.isNotOccupied(tiles.get(index), occupiedTiles)) {
                pawn.setBoardTile(tiles.get(index));
                occupiedTiles.add(tiles.get(index));
                return true;
            }
            i++;
        }

        return false;
    }

    private void setTileLastResort(Pawn pawn) {
        int index;

        while (pawn.getBoardTile() == null) {
            index = random.nextInt(remainingTeamTiles.size());
            if (this.isNotOccupied(remainingTeamTiles.get(index), occupiedTiles)) {
                pawn.setBoardTile(remainingTeamTiles.get(index));
                occupiedTiles.add(remainingTeamTiles.get(index));
                return;
            }
        }
    }

    private void addToValidTiles(BoardTile tileToAdd, List<BoardTile> validTiles) {
        if (tileToAdd.getTeam() == null) return;
        boolean alreadyAdded = validTiles.stream().anyMatch(validTile -> validTile.getTileId() == tileToAdd.getTileId());
        if (!alreadyAdded) validTiles.add(tileToAdd);
    }

    private void removeTeamTile(Pawn pawn) {
        remainingTeamTiles.removeIf(teamTile -> pawn.getBoardTile().getId() == teamTile.getId());
    }

    private boolean isNotOccupied(BoardTile boardTileToPlaceOn, List<BoardTile> occupiedTiles) {
        for (BoardTile occupiedTile : occupiedTiles)
            if (occupiedTile.getId() == boardTileToPlaceOn.getId()) return false;
        return true;
    }

    private List<BoardTile> getFirstTwoRows() {
        List<BoardTile> firstTwoRows = this.getFrontLineTiles();
        List<TileDistance> tileDistances = this.getInitialDistances(firstTwoRows);
        List<BoardTile> nextTiles = this.getNextTiles(firstTwoRows, tileDistances);

        nextTiles.forEach(tile -> this.addToValidTiles(tile, firstTwoRows));

        return firstTwoRows;
    }

    private List<BoardTile> getSecondRow() {
        return this.getFirstTwoRows()
                .stream()
                .filter(tile -> tile.getConnectedTiles()
                        .stream()
                        .noneMatch(connectedTile -> connectedTile.getTeam() == null))
                .collect(Collectors.toList());
    }
}

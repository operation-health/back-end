package be.kdg.strategobackend.service.manager;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.service.PawnService;
import be.kdg.strategobackend.service.exception.GamePlayerException;
import be.kdg.strategobackend.service.exception.MissingHostException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GameManager {
    private final PawnService pawnService;

    @Autowired
    public GameManager(PawnService pawnService) {
        this.pawnService = pawnService;
    }

    public boolean isHost(Game game, String playerName) throws MissingHostException {
        GamePlayer host = this.getHost(game);
        return host.getPlayer().getPlayerName().equals(playerName);
    }

    public boolean isTileOccupied(long boardId, int tileId, long gameId) {
        return pawnService.loadByBoardIdAndBoardTileIdAndGameId(boardId, tileId, gameId) != null;
    }

    public GamePlayer getGamePlayerWithTurn(Game game) throws GamePlayerException {
        return game.getGamePlayers()
                .stream()
                .filter(GamePlayer::isCurrentTurn)
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("No game players were found with a turn."));
    }

    private GamePlayer getHost(Game game) throws MissingHostException {
        return game.getGamePlayers()
                .stream()
                .filter(GamePlayer::isHost)
                .findFirst()
                .orElseThrow(() -> new MissingHostException("The game does not contain a host."));
    }
}

package be.kdg.strategobackend.service.manager.gamelogic;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.service.GamePlayerService;
import be.kdg.strategobackend.service.exception.GameTurnException;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class TurnManager {
    private final GamePlayerService gamePlayerService;


    public TurnManager(GamePlayerService gamePlayerService) {
        this.gamePlayerService = gamePlayerService;
    }

    public GameTurnTransition endTurn(Game game, boolean isAfk) throws GameTurnException {
        GamePlayer currentGamePlayer = game.getGamePlayers()
                .stream()
                .filter(GamePlayer::isCurrentTurn)
                .findFirst()
                .orElseThrow(() -> new GameTurnException("No game players were found with a turn while ending a turn."));
        GamePlayer nextGamePlayer = null;

        game.getGamePlayers().sort(Comparator.comparing(GamePlayer::getTeam));

        List<GamePlayer> livingGamePlayers = game.getGamePlayers()
                .stream()
                .filter(gamePlayer -> !gamePlayer.isDead() && gamePlayer.getTeam().getTeam() > currentGamePlayer.getTeam().getTeam())
                .collect(Collectors.toList());

        Optional<GamePlayer> nextGamePlayerOptional = livingGamePlayers.stream().findFirst();

        if (nextGamePlayerOptional.isPresent()) {
            nextGamePlayer = nextGamePlayerOptional.get();
            nextGamePlayer.setCurrentTurn(true);
            nextGamePlayer.setTurnStartTime(LocalDateTime.now());
            nextGamePlayer = gamePlayerService.save(nextGamePlayer);
        } else {
            nextGamePlayerOptional = game.getGamePlayers()
                    .stream()
                    .filter(gamePlayer -> !gamePlayer.isDead())
                    .findFirst();
            if (nextGamePlayerOptional.isPresent()) {
                nextGamePlayer = nextGamePlayerOptional.get();
                nextGamePlayer.setCurrentTurn(true);
                nextGamePlayer.setTurnStartTime(LocalDateTime.now());
                nextGamePlayer = gamePlayerService.save(nextGamePlayer);
            }
        }

        currentGamePlayer.setAfk(isAfk);
        currentGamePlayer.setCurrentTurn(false);
        gamePlayerService.save(currentGamePlayer);

        GameTurn gameTurn = GameTurn.builder()
                .afkTurn(isAfk)
                .time(LocalDateTime.now())
                .gamePlayer(currentGamePlayer)
                .build();

        gamePlayerService.save(currentGamePlayer);

        return new GameTurnTransition(gameTurn, nextGamePlayer);
    }


}

package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.game.GameRules;
import be.kdg.strategobackend.persistence.GameRulesRepository;
import be.kdg.strategobackend.service.exception.GameRulesException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class GameRulesService {
    private final GameRulesRepository repo;

    @Autowired
    public GameRulesService(GameRulesRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public GameRules save(GameRules gameRules) {
        return repo.save(gameRules);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public GameRules load(Long id) throws GameRulesException {
        return repo.findById(id).orElseThrow(() -> new GameRulesException("The game rules could not be found."));
    }
}

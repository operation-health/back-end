package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.game.GameRules;
import be.kdg.strategobackend.model.game.GameState;
import be.kdg.strategobackend.model.gameplayer.Difficulty;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.persistence.GameRepository;
import be.kdg.strategobackend.properties.GameRulesProperties;
import be.kdg.strategobackend.service.exception.*;
import be.kdg.strategobackend.service.manager.LobbyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class LobbyService {
    private final GameRepository repo;
    private final BoardService boardService;
    private final GameRulesService gameRulesService;
    private final GamePlayerService gamePlayerService;
    private final PlayerService playerService;

    private final LobbyManager lobbyManager;

    private final GameRulesProperties gameRulesProperties;

    @Autowired
    public LobbyService(GameRepository repo, BoardService boardService, GameRulesService gameRulesService, GamePlayerService gamePlayerService, PlayerService playerService, LobbyManager lobbyManager, GameRulesProperties gameRulesProperties) {
        this.repo = repo;
        this.boardService = boardService;
        this.gameRulesService = gameRulesService;
        this.gamePlayerService = gamePlayerService;
        this.playerService = playerService;
        this.lobbyManager = lobbyManager;
        this.gameRulesProperties = gameRulesProperties;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Game save(Game game) {
        return repo.save(game);
    }

    public Game createLobby(String hostname) throws BoardException, PlayerAlreadyInGameException, GameException {
        Player player = playerService.loadPlayerByPlayerName(hostname);

        if (this.isAlreadyInGame(hostname))
            throw new PlayerAlreadyInGameException("The player is already in a game.");

        Board board = boardService.loadByType("WATERLOO");

        GameRules gameRules = new GameRules(gameRulesProperties.getTurnTimer(), gameRulesProperties.getAfkTimer(), gameRulesProperties.getPreparationTimer());
        gameRules = gameRulesService.save(gameRules);

        GamePlayer gamePlayer = new GamePlayer(Team.randomTeam(board), player, true);
        gamePlayer = gamePlayerService.save(gamePlayer);
        List<GamePlayer> gamePlayers = new ArrayList<>();
        gamePlayers.add(gamePlayer);

        Game game = new Game(UUID.randomUUID(), true, GameState.LOBBY, board, gameRules, gamePlayers);
        game = this.save(game);

        gamePlayer.setGame(game);
        gamePlayerService.save(gamePlayer);

        game = this.load(game.getId());
        return this.fillWithComputers(game);
    }

    public Game fillWithComputers(Game game) throws GameException {
        if (game.getGamePlayers().size() >= game.getBoard().getPlayerCount())
            throw new GameException("The game is already full.");

        for (int i = 0; i <= game.getBoard().getPlayerCount() - game.getGamePlayers().size(); i++) {
            GamePlayer gamePlayer = new GamePlayer(Team.randomTeam(game), game);
            gamePlayer.setGame(game);
            gamePlayer.setReady(true);
            gamePlayer.setDifficulty(Difficulty.EASY);
            game.getGamePlayers().add(gamePlayer);
            gamePlayerService.save(gamePlayer);
        }

        return this.save(game);
    }

    public boolean canSendInvite(String playerName, Player playerToInvite, Game game) throws GameException, MailException, HostException, PlayerAlreadyInGameException, MissingHostException {
        return lobbyManager.canSendInvite(playerName, playerToInvite, game);
    }

    public Game addComputer(Game game) throws GameException {
        if (game.getGamePlayers().size() >= game.getBoard().getPlayerCount())
            throw new GameException("The game is already full.");

        GamePlayer gamePlayer = new GamePlayer(Team.randomTeam(game), game);
        gamePlayer = gamePlayerService.save(gamePlayer);

        game.getGamePlayers().add(gamePlayer);
        return this.save(game);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Game load(long id) throws GameException {
        return repo.findById(id).orElseThrow(() -> new GameException("The game could not be found."));
    }

    public Game loadCurrentGameByPlayerName(String playerName) throws GameException {
        return repo.findCurrentGameByPlayerName(playerName).orElseThrow(() -> new GameException("The player is not in a game."));
    }

    public Game acceptInvite(String playerName, UUID uuid) throws GameException, PlayerAlreadyInGameException, MissingHostException, GamePlayerException {
        Player player = playerService.loadPlayerByPlayerName(playerName);

        if (this.isAlreadyInGame(playerName))
            throw new PlayerAlreadyInGameException("The player is already in a game.");

        Game game = repo.findLobbyByInviteUUID(uuid).orElseThrow(() -> new GameException("The game could not be found."));
        game.getGamePlayers()
                .stream()
                .filter(GamePlayer::isHost)
                .findFirst()
                .orElseThrow(() -> new MissingHostException("The game does not contain a host."));

        return this.addPlayerToGame(game, player);
    }

    public Game loadPublicLobby(String playerName) throws GameJoinException, GameException {
        if (this.isAlreadyInGame(playerName)) return this.loadCurrentGameByPlayerName(playerName);
        return repo.findGamesByGameStateAndPublicAccessTrue(GameState.LOBBY)
                .stream()
                .filter(g -> g.getGamePlayers()
                        .stream()
                        .filter(gamePlayer -> !gamePlayer.isComputer()).count() < g.getBoard().getPlayerCount())
                .findAny()
                .orElseThrow(() -> new GameJoinException("There are no public games available."));
    }

    public boolean isAddable(Game game, String playerName) {
        return lobbyManager.isAddable(game, playerName);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Update methods ---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public Game updatePrivacy(String playerName, boolean publicAccess) throws MissingHostException, GameException, HostException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        if (lobbyManager.isNotHost(game, playerName)) throw new HostException("Player is not host.");
        if (game.getGameState() == GameState.LOBBY) game.setPublicAccess(publicAccess);
        return this.save(game);
    }

    public Game updateBoard(String playerName, String boardType) throws BoardException, MissingHostException, GameException, HostException, GamePlayerException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        Board oldBoard = game.getBoard();
        if (lobbyManager.isNotHost(game, playerName)) throw new HostException("Player is not host.");

        if (game.getGameState() == GameState.LOBBY) {
            Board board = boardService.loadByType(boardType);
            game.setBoard(board);

            if (oldBoard.getPlayerCount() < board.getPlayerCount()) {
                int extraComputers = board.getPlayerCount() - oldBoard.getPlayerCount();
                for (int i = 0; i < extraComputers; i++) game = this.addComputer(game);
            } else {
                int fewerComputers = oldBoard.getPlayerCount() - board.getPlayerCount();
                for (int i = 0; i < fewerComputers; i++) game = this.removeAI(game);
            }
        }
        return this.save(game);
    }

    public Game updatePlayerStatus(String playerName, boolean ready) throws GamePlayerException, GameException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        GamePlayer gamePlayer = game.getGamePlayers()
                .stream()
                .filter(gp -> !gp.isComputer())
                .filter(gp -> gp.getPlayer().getPlayerName().equals(playerName))
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."));

        if (game.getGameState() == GameState.LOBBY) {
            gamePlayer.setReady(ready);
            gamePlayerService.save(gamePlayer);
        }

        return this.save(game);
    }

    public Game updateComputer(String playerName, Team team, Difficulty difficulty) throws HostException, MissingHostException, GameException, GamePlayerException {
        Game game = loadCurrentGameByPlayerName(playerName);
        GamePlayer gamePlayer = lobbyManager.getComputer(playerName, team, game);
        gamePlayer.setDifficulty(difficulty);

        gamePlayerService.save(gamePlayer);
        return this.save(game);
    }


    public GamePlayer increaseDifficulty(String playerName, Team team) throws GameException, GamePlayerException, MissingHostException, HostException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        GamePlayer computerGamePlayer = lobbyManager.getComputer(game, playerName, team);
        computerGamePlayer.setDifficulty(computerGamePlayer.getDifficulty().getNext());
        return gamePlayerService.save(computerGamePlayer);
    }

    public GamePlayer decreaseDifficulty(String playerName, Team team) throws GameException, GamePlayerException, MissingHostException, HostException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        GamePlayer computerGamePlayer = lobbyManager.getComputer(game, playerName, team);
        computerGamePlayer.setDifficulty(computerGamePlayer.getDifficulty().getPrevious());
        return gamePlayerService.save(computerGamePlayer);
    }

    public GameRules updateTimer(String playerName, long timer) throws GameException, MissingHostException, HostException {
        if (!lobbyManager.isValidValue(timer)) throw new GameException("The timer is not valid.");

        Game game = this.loadCurrentGameByPlayerName(playerName);
        if (lobbyManager.isNotHost(game, playerName)) throw new HostException("The player is not the host.");
        if (game.getGameState() != GameState.LOBBY) throw new GameException("The game is not in the lobby state.");

        return game.getGameRules();
    }

    public Game updateTurnTimer(String playerName, long turnTimer) throws MissingHostException, GameException, HostException {
        GameRules gameRules = this.updateTimer(playerName, turnTimer);
        gameRules.setTurnTimer(turnTimer);
        gameRulesService.save(gameRules);
        Game game = this.loadCurrentGameByPlayerName(playerName);
        return this.save(game);
    }

    public Game updateAfkTimer(String playerName, long afkTimer) throws MissingHostException, GameException, HostException {
        GameRules gameRules = this.updateTimer(playerName, afkTimer);
        gameRules.setAfkTimer(afkTimer);
        gameRulesService.save(gameRules);
        Game game = this.loadCurrentGameByPlayerName(playerName);
        return this.save(game);
    }

    public Game updatePreparationTimer(String playerName, long preparationTimer) throws MissingHostException, GameException, HostException {
        GameRules gameRules = this.updateTimer(playerName, preparationTimer);
        gameRules.setPreparationTimer(preparationTimer);
        gameRulesService.save(gameRules);
        Game game = this.loadCurrentGameByPlayerName(playerName);
        return this.save(game);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Delete methods ---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public void delete(Game game) {
        repo.delete(game);
    }

    public Game kickPlayer(String hostName, String playerName) throws GameException, HostException, MissingHostException, GamePlayerException {
        Game game = this.loadCurrentGameByPlayerName(hostName);
        if (lobbyManager.isNotHost(game, hostName)) throw new HostException("The player is not the host.");
        this.removePlayerFromGame(playerName, game);
        return this.load(game.getId());
    }

    public Game leaveGame(String playerName) throws GameException, GamePlayerException, HostException {
        Game game = this.loadCurrentGameByPlayerName(playerName);
        return this.removePlayerFromGame(playerName, game);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Private support methods ------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    private Game addPlayerToGame(Game game, Player player) throws GamePlayerException {
        game = this.removeAI(game);
        GamePlayer gamePlayer = new GamePlayer(Team.randomTeam(game), player, game, false);
        gamePlayer = gamePlayerService.save(gamePlayer);

        game.getGamePlayers().add(gamePlayer);
        game = this.save(game);

        return game;
    }

    public Game addPlayerToGame(Game game, String playerName) throws GamePlayerException {
        return this.addPlayerToGame(game, playerService.loadPlayerByPlayerName(playerName));
    }

    public Game removeAI(Game game) throws GamePlayerException {
        GamePlayer computerGamePlayer = game.getGamePlayers()
                .stream()
                .filter(GamePlayer::isComputer)
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("No AI found in the game."));

        gamePlayerService.delete(computerGamePlayer.getId());
        game.getGamePlayers().remove(computerGamePlayer);
        return game;
    }

    private Game removePlayerFromGame(String playerName, Game game) throws GamePlayerException, HostException, GameException {
        GamePlayer gamePlayer = game.getGamePlayers().stream()
                .filter(gp -> !gp.isComputer())
                .filter(gp -> gp.getPlayer().getPlayerName().equals(playerName))
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."));

        if (lobbyManager.isEmptyGame(game.getGamePlayers())) {
            game.getGamePlayers().forEach(gp -> gamePlayerService.delete(gp.getId()));
            this.delete(game);
            return null;
        } else {
            game.getGamePlayers().remove(gamePlayer);
            if (gamePlayer.isHost()) {
                GamePlayer newHost = game.getGamePlayers()
                        .stream()
                        .filter(gp -> !gp.isComputer())
                        .findFirst()
                        .orElseThrow(() -> new HostException("The new host could not be found."));
                newHost.setHost(true);
                gamePlayerService.save(newHost);
            }

            gamePlayerService.delete(gamePlayer.getId());
            game = this.addComputer(game);

            return game;
        }
    }

    private boolean isAlreadyInGame(String playerName) {
        return repo.findCurrentGameByPlayerName(playerName).isPresent();
    }
}

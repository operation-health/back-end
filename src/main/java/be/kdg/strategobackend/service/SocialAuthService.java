package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.player.NotificationSettings;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.model.player.PlayerStats;
import be.kdg.strategobackend.rest.dto.other.SocialProvider;
import be.kdg.strategobackend.service.exception.EmailAlreadyExistsException;
import be.kdg.strategobackend.service.exception.EmailConfirmationException;
import be.kdg.strategobackend.service.exception.PlayerNameAlreadyExistsException;
import be.kdg.strategobackend.service.exception.SocialLoginException;
import org.springframework.social.InvalidAuthorizationException;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class SocialAuthService {
    private final PlayerService playerService;
    private final PlayerStatsService playerStatsService;
    private final NotificationSettingsService notificationSettingsService;

    public SocialAuthService(PlayerService playerService, PlayerStatsService playerStatsService, NotificationSettingsService notificationSettingsService) {
        this.playerService = playerService;
        this.playerStatsService = playerStatsService;
        this.notificationSettingsService = notificationSettingsService;
    }

    public Player registerWithSocial(String playerName, String token, SocialProvider socialProvider) throws PlayerNameAlreadyExistsException, EmailAlreadyExistsException, SocialLoginException {
        Player player = new Player();
        player.setHasPassword(false);
        player.setEmail(getEmailFromSocial(socialProvider, token));
        player.setEmailConfirmed(true);
        player.setPlayerName(playerName);
        player.setPlayerStats(playerStatsService.save(new PlayerStats()));
        player.setNotificationSettings(notificationSettingsService.save(new NotificationSettings()));
        player.setFriends(new HashSet<>());
        player.setGamePlayers(new HashSet<>());
        player.setPassword("");
        return playerService.register(player);
    }

    private String getEmailFromSocial(SocialProvider socialProvider, String token) throws SocialLoginException {
        if (socialProvider == SocialProvider.GOOGLE) {
            Google google = new GoogleTemplate(token);
            try {
                return google.oauth2Operations().getUserinfo().getEmail();
            } catch (NullPointerException ex) {
                throw new SocialLoginException("The token is not valid.");
            }
        } else {
            Facebook facebook = new FacebookTemplate(token);
            try {
                return facebook.userOperations().getUserProfile().getEmail();
            } catch (InvalidAuthorizationException e) {
                throw new SocialLoginException("The token is not valid.");
            }
        }
    }

    public Player findPlayerBySocial(String token, SocialProvider socialProvider) throws SocialLoginException, EmailConfirmationException {
        String email = getEmailFromSocial(socialProvider, token);
        return playerService.loadPlayerByPlayerNameOrEmail(email);
    }
}

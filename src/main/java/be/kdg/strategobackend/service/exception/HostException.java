package be.kdg.strategobackend.service.exception;

public class HostException extends Exception {
    public HostException(String message) {
        super(message);
    }
}

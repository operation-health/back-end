package be.kdg.strategobackend.service.exception;

public class PlayerNameAlreadyExistsException extends Exception {
    public PlayerNameAlreadyExistsException(String message) {
        super(message);
    }
}

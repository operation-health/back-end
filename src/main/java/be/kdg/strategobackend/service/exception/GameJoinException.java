package be.kdg.strategobackend.service.exception;

public class GameJoinException extends Exception {
    public GameJoinException(String message) {
        super(message);
    }
}

package be.kdg.strategobackend.service.exception;

public class GamePlayerException extends Exception {
    public GamePlayerException(String message) {
        super(message);
    }
}

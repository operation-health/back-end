package be.kdg.strategobackend.service.exception;

public class GameException extends Exception {
    public GameException(String message) {
        super(message);
    }
}

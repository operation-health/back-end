package be.kdg.strategobackend.service.exception;

public class GameStartException extends Exception {
    public GameStartException(String message) {
        super(message);
    }
}

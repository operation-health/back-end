package be.kdg.strategobackend.service.exception;

public class MissingHostException extends Exception {
    public MissingHostException(String message) {
        super(message);
    }
}

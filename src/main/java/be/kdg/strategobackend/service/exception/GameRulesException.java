package be.kdg.strategobackend.service.exception;

public class GameRulesException extends Exception {
    public GameRulesException(String message) {
        super(message);
    }
}

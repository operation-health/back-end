package be.kdg.strategobackend.service.exception;

public class BoardException extends Exception {
    public BoardException(String message) {
        super(message);
    }
}

package be.kdg.strategobackend.service.exception;

public class EmailConfirmationException extends Exception {
    public EmailConfirmationException(String message) {
        super(message);
    }
}

package be.kdg.strategobackend.service.exception;

public class InvalidPawnException extends Exception {
    public InvalidPawnException(String message) {
        super(message);
    }
}

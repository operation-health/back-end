package be.kdg.strategobackend.service.exception;

public class PawnException extends Exception {
    public PawnException(String message) {
        super(message);
    }
}

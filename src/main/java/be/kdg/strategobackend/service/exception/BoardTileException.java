package be.kdg.strategobackend.service.exception;

public class BoardTileException extends Exception {
    public BoardTileException(String message) {
        super(message);
    }
}

package be.kdg.strategobackend.service.exception;

public class PlayerStatsInaccessibleException extends Exception {
    public PlayerStatsInaccessibleException(String message) {
        super(message);
    }
}

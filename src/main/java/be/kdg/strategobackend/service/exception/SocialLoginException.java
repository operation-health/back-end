package be.kdg.strategobackend.service.exception;

public class SocialLoginException extends Exception {
    public SocialLoginException(String message) {
        super(message);
    }
}

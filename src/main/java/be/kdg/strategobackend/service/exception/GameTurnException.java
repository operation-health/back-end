package be.kdg.strategobackend.service.exception;

/**
 * @author Bart Wezenbeek
 * @version 1.0 20/02/2019 8:47 *
 */
public class GameTurnException extends Exception {
    public GameTurnException(String message) {
        super(message);
    }
}

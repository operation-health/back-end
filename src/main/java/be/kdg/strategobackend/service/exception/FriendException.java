package be.kdg.strategobackend.service.exception;

public class FriendException extends Exception {
    public FriendException(String message) {
        super(message);
    }
}

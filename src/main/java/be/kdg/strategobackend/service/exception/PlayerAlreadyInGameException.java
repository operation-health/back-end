package be.kdg.strategobackend.service.exception;

public class PlayerAlreadyInGameException extends Exception {
    public PlayerAlreadyInGameException(String message) {
        super(message);
    }
}

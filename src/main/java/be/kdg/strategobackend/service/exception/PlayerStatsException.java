package be.kdg.strategobackend.service.exception;

public class PlayerStatsException extends Exception {
    public PlayerStatsException(String message) {
        super(message);
    }
}

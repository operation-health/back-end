package be.kdg.strategobackend.service.exception;

public class InvalidBoardTileException extends Exception {
    public InvalidBoardTileException(String message) {
        super(message);
    }
}

package be.kdg.strategobackend.service.exception;

public class GameMessageException extends Exception {
    public GameMessageException(String message) {
        super(message);
    }
}

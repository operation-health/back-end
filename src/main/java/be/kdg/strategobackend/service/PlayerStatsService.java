package be.kdg.strategobackend.service;

import be.kdg.strategobackend.model.player.PlayerStats;
import be.kdg.strategobackend.persistence.PlayerStatsRepository;
import be.kdg.strategobackend.service.exception.PlayerStatsException;
import be.kdg.strategobackend.service.exception.PlayerStatsInaccessibleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class PlayerStatsService {
    private PlayerStatsRepository repo;

    @Autowired
    public PlayerStatsService(PlayerStatsRepository repo) {
        this.repo = repo;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Save methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public PlayerStats save(PlayerStats playerStats) {
        return repo.save(playerStats);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Load methods -----------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public PlayerStats load(long id) throws PlayerStatsException {
        return repo.findById(id).orElseThrow(() -> new PlayerStatsException("The player stats could not be found."));
    }

    public PlayerStats loadPlayerStatsByPlayerName(String callingPlayerName, String playerName) throws PlayerStatsException, PlayerStatsInaccessibleException {
        PlayerStats playerStats = repo.findPlayerStatsByPlayerName(playerName).orElseThrow(() -> new PlayerStatsException("The player stats could not be found."));
        if (!callingPlayerName.equals(playerName) && !playerStats.isPublicAccess())
            throw new PlayerStatsInaccessibleException("The stats of the player are not accessible.");
        return playerStats;
    }

    //------------------------------------------------------------------------------------------------------------------
    //Update methods ---------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    public PlayerStats updatePlayerStats(String playerName, boolean publicAccess) throws PlayerStatsException {
        PlayerStats playerStats = this.loadPlayerStatsByPlayerName(playerName);
        playerStats.setPublicAccess(publicAccess);
        return this.save(playerStats);
    }

    public PlayerStats updatePlayerStats(String playerName, int gamesWon, int gamesPlayed) throws PlayerStatsException {
        if (gamesPlayed < gamesWon)
            throw new PlayerStatsException("The amount of won games is higher than the amount of games played.");
        PlayerStats playerStats = this.loadPlayerStatsByPlayerName(playerName);
        playerStats.setGamesWon(gamesWon);
        playerStats.setGamesPlayed(gamesPlayed);
        return this.save(playerStats);
    }

    //------------------------------------------------------------------------------------------------------------------
    //Private support methods ------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    private PlayerStats loadPlayerStatsByPlayerName(String playerName) throws PlayerStatsException {
        return repo.findPlayerStatsByPlayerName(playerName).orElseThrow(() -> new PlayerStatsException("The player stats could not be found."));
    }
}

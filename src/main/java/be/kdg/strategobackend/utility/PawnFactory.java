package be.kdg.strategobackend.utility;

import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.pawn.Rank;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class PawnFactory {
    public List<Pawn> generatePawns(GamePlayer gamePlayer) {
        List<Pawn> pawns = new ArrayList<>();
        pawns.add(this.generateRank(gamePlayer, Rank.SPY));
        pawns.add(this.generateRank(gamePlayer, Rank.FLAG));
        pawns.add(this.generateRank(gamePlayer, Rank.MARSHAL));
        pawns.add(this.generateRank(gamePlayer, Rank.GENERAL));
        pawns.addAll(this.generateRank(gamePlayer, Rank.COLONEL, 2));
        pawns.addAll(this.generateRank(gamePlayer, Rank.MAJOR, 2));
        pawns.addAll(this.generateRank(gamePlayer, Rank.CAPTAIN, 2));
        pawns.addAll(this.generateRank(gamePlayer, Rank.LIEUTENANT, 2));
        pawns.addAll(this.generateRank(gamePlayer, Rank.SERGEANT, 2));
        pawns.addAll(this.generateRank(gamePlayer, Rank.MINER, 3));
        pawns.addAll(this.generateRank(gamePlayer, Rank.BOMB, 3));
        pawns.addAll(this.generateRank(gamePlayer, Rank.SCOUT, 4));
        Collections.shuffle(pawns);
        return pawns;
    }

    private List<Pawn> generateRank(GamePlayer gamePlayer, Rank rank, int amount) {
        List<Pawn> pawns = new ArrayList<>();
        for (int i = 0; i < amount; i++) pawns.add(this.generateRank(gamePlayer, rank));
        return pawns;
    }

    private Pawn generateRank(GamePlayer gamePlayer, Rank rank) {
        return Pawn.builder().dead(false).gamePlayer(gamePlayer).rank(rank).build();
    }
}

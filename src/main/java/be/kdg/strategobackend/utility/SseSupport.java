package be.kdg.strategobackend.utility;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.rest.dto.lobby.LobbyDTO;
import be.kdg.strategobackend.service.exception.GamePlayerException;
import be.kdg.strategobackend.service.manager.gamelogic.GameTurnTransition;
import be.kdg.strategobackend.streaming.SseProcessor;
import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.GameEventBuilder;
import be.kdg.strategobackend.streaming.event.actions.LobbyEventBuilder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

@Component
public class SseSupport {
    private final SseProcessor sseProcessor;
    private final ModelMapper modelMapper;
    private final GameEventBuilder gameEventBuilder;
    private final LobbyEventBuilder lobbyEventBuilder;

    @Autowired
    public SseSupport(SseProcessor sseProcessor, ModelMapper modelMapper, GameEventBuilder gameEventBuilder, LobbyEventBuilder lobbyEventBuilder) {
        this.sseProcessor = sseProcessor;
        this.modelMapper = modelMapper;
        this.gameEventBuilder = gameEventBuilder;
        this.lobbyEventBuilder = lobbyEventBuilder;
    }

    public void sendNewComputerPackets(Game game, GamePlayer originalGamePlayer) throws GamePlayerException {
        GamePlayer newComputerGamePlayer = game.getGamePlayers().stream()
                .filter(gp -> gp.getTeam() == originalGamePlayer.getTeam())
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."));
        sseProcessor.push(game, lobbyEventBuilder.createLobbyAiJoined(newComputerGamePlayer));
    }

    public void sendPlayingPhasePackets(Game game) {
        for (GamePlayer gp : game.getGamePlayers()) {
            if (gp.isComputer()) continue;
            Event event = gameEventBuilder.createGameTransitionToPlaying(game, gp.getPlayer().getPlayerName());
            sseProcessor.push(gp.getPlayer(), event);
        }
    }

    public ResponseEntity<LobbyDTO> sendLobbyPlayerJoined(Game game, String playerName) throws GamePlayerException {
        GamePlayer gamePlayer = game.getGamePlayers().stream()
                .filter(gp -> !gp.isComputer())
                .filter(gp -> gp.getPlayer().getPlayerName().equals(playerName))
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."));
        sseProcessor.push(game, lobbyEventBuilder.createLobbyPlayerJoined(gamePlayer));
        return new ResponseEntity<>(modelMapper.map(game, LobbyDTO.class), HttpStatus.OK);
    }

    public Event getGameEvent(Game game, GameTurnTransition gameTurnTransition) {
        GameTurn gameTurn = gameTurnTransition.getGameTurn();

        if (gameTurn.getAttackedPawn() == null) {
            return gameEventBuilder.createGamePawnMoved(
                    gameTurn.getMovedPawn(),
                    gameTurn.getMovedToTile(),
                    gameTurn.getGamePlayer(),
                    gameTurnTransition.getNextPlayer()
            );
        } else {
            GamePlayer currentGamePlayer = gameTurn.getGamePlayer();
            GamePlayer attackedGamePlayer = gameTurn.getAttackedPawn().getGamePlayer();

            if (currentGamePlayer.isDead() && attackedGamePlayer.isDead())
                return this.getSpecialAttackEvent(game, gameTurn, gameTurnTransition, currentGamePlayer, attackedGamePlayer);
            else if (!attackedGamePlayer.isDead() && !currentGamePlayer.isDead()) {
                return gameEventBuilder.createGamePawnAttacked(
                        gameTurn.getMovedPawn(),
                        gameTurn.getAttackedPawn(),
                        gameTurn.getMovedToTile(),
                        currentGamePlayer,
                        gameTurnTransition.getNextPlayer()
                );
            } else {
                Predicate<GamePlayer> allAlivePredicate = gp -> !gp.isDead();
                Predicate<GamePlayer> humanAlivePredicate = gp -> !gp.isDead() && !gp.isComputer();

                long aliveCount = this.getRemainingGamePlayerCountWithPredicate(game, allAlivePredicate);
                long humanAliveCount = this.getRemainingGamePlayerCountWithPredicate(game, humanAlivePredicate);

                if (humanAliveCount == 0) {
                    return gameEventBuilder.createGameHumansEliminated(
                            gameTurn.getMovedPawn(),
                            gameTurn.getAttackedPawn(),
                            gameTurn.getMovedToTile(),
                            currentGamePlayer,
                            attackedGamePlayer
                    );
                } else if (gameTurn.getAttackedPawn().getRank() == Rank.FLAG)
                    return this.getFlagInvolvedEvent(aliveCount, gameTurn, gameTurnTransition, currentGamePlayer, attackedGamePlayer);
                else
                    return this.getAttritionEvent(aliveCount, gameTurn, gameTurnTransition, currentGamePlayer, attackedGamePlayer);
            }
        }
    }

    long getRemainingGamePlayerCountWithPredicate(Game game, Predicate<GamePlayer> predicate) {
        return game.getGamePlayers()
                .stream()
                .filter(predicate)
                .count();
    }

    private Event getSpecialAttackEvent(Game game, GameTurn gameTurn, GameTurnTransition gameTurnTransition, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer) {
        Predicate<GamePlayer> humanPredicate = gamePlayer -> !gamePlayer.isComputer() && !gamePlayer.isDead();
        long remainingHumanGamePlayersCount = this.getRemainingGamePlayerCountWithPredicate(game, humanPredicate);

        if (remainingHumanGamePlayersCount >= 2) {
            return gameEventBuilder.createGameDoubleElimination(
                    gameTurn.getMovedPawn(),
                    gameTurn.getAttackedPawn(),
                    currentGamePlayer,
                    attackedGamePlayer,
                    gameTurnTransition.getNextPlayer()
            );
        } else if (remainingHumanGamePlayersCount == 1) {
            return gameEventBuilder.createGamePassiveWin(
                    gameTurn.getMovedPawn(),
                    gameTurn.getAttackedPawn(),
                    currentGamePlayer,
                    attackedGamePlayer
            );
        } else {
            return gameEventBuilder.createGameHumansEliminated(
                    gameTurn.getMovedPawn(),
                    gameTurn.getAttackedPawn(),
                    gameTurn.getMovedToTile(),
                    currentGamePlayer,
                    attackedGamePlayer
            );
        }
    }

    private Event getFlagInvolvedEvent(long aliveCount, GameTurn gameTurn, GameTurnTransition gameTurnTransition, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer) {
        if (aliveCount > 1) {
            return gameEventBuilder.createGameFlagKilled(
                    gameTurn.getMovedPawn(),
                    gameTurn.getAttackedPawn(),
                    gameTurn.getMovedToTile(),
                    currentGamePlayer,
                    attackedGamePlayer,
                    gameTurnTransition.getNextPlayer()
            );
        } else {
            return gameEventBuilder.createGameFinalBlow(
                    gameTurn.getMovedPawn(),
                    gameTurn.getAttackedPawn(),
                    gameTurn.getMovedToTile(),
                    currentGamePlayer,
                    attackedGamePlayer
            );
        }
    }

    private Event getAttritionEvent(long aliveCount, GameTurn gameTurn, GameTurnTransition gameTurnTransition, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer) {
        if (aliveCount > 1) {
            return gameEventBuilder.createGameAttritionKill(
                    gameTurn.getMovedPawn(),
                    gameTurn.getAttackedPawn(),
                    gameTurn.getMovedToTile(),
                    currentGamePlayer,
                    attackedGamePlayer,
                    gameTurnTransition.getNextPlayer()
            );
        } else {
            return gameEventBuilder.createGameAttritionVictory(
                    gameTurn.getMovedPawn(),
                    gameTurn.getAttackedPawn(),
                    gameTurn.getMovedToTile(),
                    currentGamePlayer,
                    attackedGamePlayer
            );
        }
    }
}

package be.kdg.strategobackend.utility;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.service.exception.GamePlayerException;
import be.kdg.strategobackend.service.exception.PawnException;
import be.kdg.strategobackend.service.manager.gamelogic.ConstraintManager;
import be.kdg.strategobackend.service.manager.gamelogic.PawnMoveManager;
import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.GameEventBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class TurnsToEventsConverter {
    private final GameEventBuilder gameEventBuilder;
    private final SseSupport sseSupport;
    private final PawnMoveManager pawnMoveManager;
    private final ConstraintManager constraintManager;

    @Autowired
    public TurnsToEventsConverter(GameEventBuilder gameEventBuilder, SseSupport sseSupport, PawnMoveManager pawnMoveManager, ConstraintManager constraintManager) {
        this.gameEventBuilder = gameEventBuilder;
        this.sseSupport = sseSupport;
        this.pawnMoveManager = pawnMoveManager;
        this.constraintManager = constraintManager;
    }

    public void resetGamePlayersAndPawns(Game game) {
        game.getGamePlayers().forEach(gamePlayer -> {
            gamePlayer.setDead(false);
            gamePlayer.getPawns()
                    .forEach(pawn -> game.getInitialState().getInitialOccupations()
                            .forEach(initialOccupation -> {
                                if (pawn.getId() == initialOccupation.getPawn().getId()) {
                                    pawn.setDead(false);
                                    pawn.setBoardTile(initialOccupation.getBoardTile());
                                }
                            }));
        });
    }

    public List<Event> convertToEvents(Game game) throws GamePlayerException, PawnException {
        List<Event> events = new ArrayList<>();
        List<GameTurn> gameTurns = this.collectGameTurns(game);

        for (int i = 0; i < gameTurns.size(); i++) {
            GameTurn gameTurn = gameTurns.get(i);
            if (gameTurn.getAttackedPawn() == null) {
                this.simulatePawnMove(game, gameTurn);
                events.add(gameEventBuilder.createGamePawnMoved(
                        gameTurn.getMovedPawn(),
                        gameTurn.getMovedToTile(),
                        gameTurn.getGamePlayer(),
                        gameTurns.get(i + 1).getGamePlayer()
                ));
            } else {
                Pawn attackingPawn = this.getAttackingPawn(game, gameTurn);
                Pawn defendingPawn = this.getDefendingPawn(game, gameTurn);

                BoardTile boardTileToMoveTo = gameTurn.getMovedToTile();

                pawnMoveManager.attack(attackingPawn, defendingPawn, boardTileToMoveTo);
                game.getGamePlayers().forEach(constraintManager::hasOnlyBombsAndFlagLeft);

                GamePlayer currentGamePlayer = gameTurn.getGamePlayer();
                GamePlayer attackedGamePlayer = gameTurn.getAttackedPawn().getGamePlayer();

                if (currentGamePlayer.isDead() && attackedGamePlayer.isDead())
                    this.getSpecialAttackEvent(i, game, attackingPawn, defendingPawn, currentGamePlayer, attackedGamePlayer, boardTileToMoveTo, gameTurns, events);
                else if (!currentGamePlayer.isDead() && !attackedGamePlayer.isDead()) {
                    events.add(gameEventBuilder.createGamePawnAttacked(
                            attackingPawn,
                            defendingPawn,
                            boardTileToMoveTo,
                            currentGamePlayer,
                            gameTurns.get(i + 1).getGamePlayer()
                    ));
                } else {
                    Predicate<GamePlayer> allAlivePredicate = gp -> !gp.isDead();
                    Predicate<GamePlayer> humanAlivePredicate = gp -> !gp.isDead() && !gp.isComputer();

                    long aliveCount = sseSupport.getRemainingGamePlayerCountWithPredicate(game, allAlivePredicate);
                    long humanAliveCount = sseSupport.getRemainingGamePlayerCountWithPredicate(game, humanAlivePredicate);

                    if (humanAliveCount == 0) {
                        events.add(gameEventBuilder.createGameHumansEliminated(
                                gameTurn.getMovedPawn(),
                                gameTurn.getAttackedPawn(),
                                gameTurn.getMovedToTile(),
                                currentGamePlayer,
                                attackedGamePlayer
                        ));
                    } else if (gameTurn.getAttackedPawn().getRank() == Rank.FLAG)
                        this.getFlagInvolvedEvent(i, aliveCount, attackingPawn, defendingPawn, currentGamePlayer, attackedGamePlayer, boardTileToMoveTo, gameTurns, events);
                    else
                        this.getAttritionEvent(i, aliveCount, attackingPawn, defendingPawn, currentGamePlayer, attackedGamePlayer, boardTileToMoveTo, gameTurns, events);
                }
            }
        }
        return events;
    }

    public List<GameTurn> collectGameTurns(Game game) {
        List<GameTurn> gameTurns = new ArrayList<>();
        game.getGamePlayers()
                .forEach(gamePlayer -> gameTurns.addAll(gamePlayer.getGameTurns()
                        .stream()
                        .filter(gameTurn -> !gameTurn.isAfkTurn())
                        .collect(Collectors.toList())));

        Collections.sort(gameTurns);

        return gameTurns;
    }

    private void simulatePawnMove(Game game, GameTurn gameTurn) throws GamePlayerException, PawnException {
        game.getGamePlayers()
                .stream()
                .filter(gamePlayer -> gamePlayer.getId() == gameTurn.getGamePlayer().getId())
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."))
                .getPawns()
                .stream()
                .filter(pawn -> pawn.getId() == gameTurn.getMovedPawn().getId())
                .findFirst()
                .orElseThrow(() -> new PawnException("The pawn could not be found."))
                .setBoardTile(gameTurn.getMovedToTile());
    }

    private Pawn getAttackingPawn(Game game, GameTurn gameTurn) throws GamePlayerException, PawnException {
        return game.getGamePlayers()
                .stream()
                .filter(gamePlayer -> gamePlayer.getPawns()
                        .stream()
                        .anyMatch(pawn -> pawn.getId() == gameTurn.getMovedPawn().getId()))
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."))
                .getPawns()
                .stream()
                .filter(pawn -> pawn.getId() == gameTurn.getMovedPawn().getId())
                .findFirst()
                .orElseThrow(() -> new PawnException("The pawn could not be found."));
    }

    private Pawn getDefendingPawn(Game game, GameTurn gameTurn) throws GamePlayerException, PawnException {
        return game.getGamePlayers()
                .stream()
                .filter(gamePlayer -> gamePlayer.getPawns()
                        .stream()
                        .anyMatch(pawn -> pawn.getId() == gameTurn.getAttackedPawn().getId()))
                .findFirst()
                .orElseThrow(() -> new GamePlayerException("The game player could not be found."))
                .getPawns()
                .stream()
                .filter(pawn -> pawn.getId() == gameTurn.getAttackedPawn().getId())
                .findFirst()
                .orElseThrow(() -> new PawnException("The pawn could not be found."));
    }

    private void getSpecialAttackEvent(int index, Game game, Pawn attackingPawn, Pawn defendingPawn, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer, BoardTile boardTileToMoveTo, List<GameTurn> gameTurns, List<Event> events) {
        Predicate<GamePlayer> humanPredicate = gamePlayer -> !gamePlayer.isComputer() && !gamePlayer.isDead();
        long remainingHumanGamePlayersCount = sseSupport.getRemainingGamePlayerCountWithPredicate(game, humanPredicate);

        if (remainingHumanGamePlayersCount >= 2) {
            events.add(gameEventBuilder.createGameDoubleElimination(
                    attackingPawn,
                    defendingPawn,
                    currentGamePlayer,
                    attackedGamePlayer,
                    gameTurns.get(index + 1).getGamePlayer()
            ));
        } else if (remainingHumanGamePlayersCount == 1) {
            events.add(gameEventBuilder.createGamePassiveWin(
                    attackingPawn,
                    defendingPawn,
                    attackedGamePlayer,
                    currentGamePlayer
            ));
        } else {
            events.add(gameEventBuilder.createGameHumansEliminated(
                    attackingPawn,
                    defendingPawn,
                    boardTileToMoveTo,
                    currentGamePlayer,
                    attackedGamePlayer));
        }
    }

    private void getFlagInvolvedEvent(int index, long aliveCount, Pawn attackingPawn, Pawn defendingPawn, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer, BoardTile boardTileToMoveTo, List<GameTurn> gameTurns, List<Event> events) {
        if (aliveCount > 1) {
            events.add(gameEventBuilder.createGameFlagKilled(
                    attackingPawn,
                    defendingPawn,
                    boardTileToMoveTo,
                    currentGamePlayer,
                    attackedGamePlayer,
                    gameTurns.get(index + 1).getGamePlayer()
            ));
        } else {
            events.add(gameEventBuilder.createGameFinalBlow(
                    attackingPawn,
                    defendingPawn,
                    boardTileToMoveTo,
                    currentGamePlayer,
                    attackedGamePlayer
            ));
        }
    }

    private void getAttritionEvent(int index, long aliveCount, Pawn attackingPawn, Pawn defendingPawn, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer, BoardTile boardTileToMoveTo, List<GameTurn> gameTurns, List<Event> events) {
        if (aliveCount > 1) {
            events.add(gameEventBuilder.createGameAttritionKill(
                    attackingPawn,
                    defendingPawn,
                    boardTileToMoveTo,
                    currentGamePlayer,
                    attackedGamePlayer,
                    gameTurns.get(index + 1).getGamePlayer()
            ));
        } else {
            events.add(gameEventBuilder.createGameAttritionVictory(
                    attackingPawn,
                    defendingPawn,
                    boardTileToMoveTo,
                    currentGamePlayer,
                    attackedGamePlayer
            ));
        }
    }
}

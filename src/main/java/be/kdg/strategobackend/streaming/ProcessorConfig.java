package be.kdg.strategobackend.streaming;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author Bart Wezenbeek
 * @version 1.0 23/02/2019 11:54 *
 */
@Configuration
public class ProcessorConfig {
    @Bean
    public ScheduledExecutorService scheduler(){
        return Executors.newSingleThreadScheduledExecutor();
    }
}

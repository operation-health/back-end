package be.kdg.strategobackend.streaming;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.streaming.event.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class SseProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(SseProcessor.class);

    @Value("${processor.timeout}")
    private long timeout;
    private final Map<Long, List<SseEmitter>> emitters = new HashMap<>();

    public SseEmitter getEmitter(Player player) {
        long id = player.getId();
        SseEmitter emitter = new SseEmitter(timeout);

        emitter.onCompletion(() -> unregisterEmitter(id, emitter));
        emitter.onError((e) -> unregisterEmitter(id, emitter)); //Could be unnecessary if onError calls onCompletion too...

        registerEmitter(id, emitter);
        return emitter;
    }

    public void push(Player player, Event event) {
        long id = player.getId();
        if (!emitters.containsKey(id)) return;
        List<SseEmitter> emitters = this.emitters.getOrDefault(id, new ArrayList<>());
        for (SseEmitter emitter : new ArrayList<>(emitters)) {
            try {
                emitter.send(event);
            } catch (IOException e) {
                LOGGER.info("Disconnecting from SseEmitter due to IOException: " + e.getMessage());
                unregisterEmitter(id, emitter);
            }
        }
    }

    public void push(Game game, Event event) {
        for (GamePlayer gamePlayer : game.getGamePlayers().stream().filter(gamePlayer -> !gamePlayer.isComputer()).collect(Collectors.toList())) {
            this.push(gamePlayer.getPlayer(), event);
            LOGGER.info(event.getPayload().toString());
        }
    }

    private void registerEmitter(long id, SseEmitter emitter) {
        List<SseEmitter> emitters = this.emitters.getOrDefault(id, new ArrayList<>());
        if (emitter == null) return;
        emitters.add(emitter);
        this.emitters.put(id, emitters);
    }

    private void unregisterEmitter(long id, SseEmitter emitter) {
        if (!this.emitters.containsKey(id)) return;
        List<SseEmitter> emitters = this.emitters.get(id);
        emitters.remove(emitter);
        this.emitters.put(id, emitters);
    }
}

package be.kdg.strategobackend.streaming.event.actions.chat;

import be.kdg.strategobackend.streaming.event.Payload;

import java.time.LocalDateTime;

public class ChatPrivateMessageReceived implements Payload {

    private String playerName;
    private LocalDateTime date;
    private String message;

}

package be.kdg.strategobackend.streaming.event.actions;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.rest.dto.game.GameDTO;
import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.game.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GameEventBuilder {
    private final ModelMapper modelMapper;

    @Autowired
    public GameEventBuilder(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Event createGameTransitionToPlaying(Game game, String playerName) {
        GameDTO gameDTO = modelMapper.map(game, GameDTO.class);
        gameDTO.getGamePlayers()
                .stream()
                .filter(gamePlayer -> gamePlayer.isComputer() || !gamePlayer.getPlayer().getPlayerName().equals(playerName))
                .forEach(gamePlayer -> gamePlayer.getPawns()
                        .forEach(pawn -> pawn.setRank(null)));
        return new Event(new GameTransitionToPlaying(
                gameDTO
        ));
    }

    public Event createGamePawnOrganised(long pawnId, int tileId) {
        return new Event(new GamePawnOrganised(
                pawnId,
                tileId
        ));
    }

    public Event createGamePlayerReadyChanged(GamePlayer gamePlayer) {
        return new Event(new GamePlayerReadyChanged(
                gamePlayer.getTeam(),
                gamePlayer.isReady()
        ));
    }

    public Event createGamePlayerAfk(GamePlayer currentGamePlayer, GamePlayer nextGamePlayer) {
        return new Event(new GamePlayerAfk(
                currentGamePlayer.getTeam(),
                nextGamePlayer.getTeam()
        ));
    }

    public Event createGamePawnMoved(Pawn pawn, BoardTile tile, GamePlayer currentGamePlayer, GamePlayer nextGamePlayer) {
        return new Event(new GamePawnMoved(
                pawn.getId(),
                tile.getTileId(),
                currentGamePlayer.getTeam(),
                nextGamePlayer.getTeam()
        ));
    }

    public Event createGamePawnAttacked(Pawn pawn, Pawn attackedPawn, BoardTile tile, GamePlayer currentGamePlayer, GamePlayer nextGamePlayer) {
        return new Event(new GamePawnAttacked(
                pawn.getId(),
                pawn.getRank(),
                pawn.isDead(),
                attackedPawn.getId(),
                attackedPawn.getRank(),
                attackedPawn.isDead(),
                tile.getTileId(),
                currentGamePlayer.getTeam(),
                nextGamePlayer.getTeam(),
                attackedPawn.getGamePlayer().getTeam()
        ));
    }

    public Event createGameDoubleElimination(Pawn pawn, Pawn attackedPawn, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer, GamePlayer nextGamePlayer) {
        return new Event(new GameDoubleElimination(
                pawn.getRank(),
                pawn.isDead(),
                attackedPawn.getRank(),
                attackedPawn.isDead(),
                currentGamePlayer.getTeam(),
                attackedGamePlayer.getTeam(),
                nextGamePlayer.getTeam()
        ));
    }

    public Event createGameFlagKilled(Pawn pawn, Pawn attackedPawn, BoardTile tile, GamePlayer currentGamePlayer, GamePlayer deadGamePlayer, GamePlayer nextGamePlayer) {
        return new Event(new GameFlagKilled(
                pawn.getId(),
                pawn.getRank(),
                pawn.isDead(),
                attackedPawn.getRank(),
                attackedPawn.isDead(),
                tile.getTileId(),
                currentGamePlayer.getTeam(),
                deadGamePlayer.getTeam(),
                nextGamePlayer.getTeam()
        ));
    }

    public Event createGameHumansEliminated(Pawn pawn, Pawn attackedPawn, BoardTile tile, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer) {
        return new Event(new GameHumansEliminated(
                pawn.getId(),
                pawn.getRank(),
                pawn.isDead(),
                attackedPawn.getRank(),
                attackedPawn.isDead(),
                tile.getTileId(),
                currentGamePlayer.getTeam(),
                currentGamePlayer.isDead(),
                attackedGamePlayer.getTeam(),
                attackedGamePlayer.isDead()
        ));
    }

    public Event createGamePassiveWin(Pawn pawn, Pawn attackedPawn, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer) {
        return new Event(new GamePassiveWin(
                pawn.getRank(),
                pawn.isDead(),
                attackedPawn.getRank(),
                attackedPawn.isDead(),
                currentGamePlayer.getTeam(),
                attackedGamePlayer.getTeam()
        ));
    }

    public Event createGameFinalBlow(Pawn pawn, Pawn attackedPawn, BoardTile tile, GamePlayer currentGamePlayer, GamePlayer deadGamePlayer) {
        return new Event(new GameFinalBlow(
                pawn.getId(),
                pawn.getRank(),
                pawn.isDead(),
                attackedPawn.getRank(),
                attackedPawn.isDead(),
                tile.getTileId(),
                currentGamePlayer.getTeam(),
                deadGamePlayer.getTeam()
        ));
    }

    public Event createGameAttritionKill(Pawn pawn, Pawn attackedPawn, BoardTile tile, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer, GamePlayer nextGamePlayer) {
        return new Event(new GameAttritionKill(
                pawn.getId(),
                pawn.getRank(),
                pawn.isDead(),
                attackedPawn.getRank(),
                attackedPawn.isDead(),
                tile.getTileId(),
                currentGamePlayer.getTeam(),
                currentGamePlayer.isDead(),
                attackedGamePlayer.getTeam(),
                nextGamePlayer.getTeam()
        ));
    }

    public Event createGameAttritionVictory(Pawn pawn, Pawn attackedPawn, BoardTile tile, GamePlayer currentGamePlayer, GamePlayer attackedGamePlayer) {
        return new Event(new GameAttritionVictory(
                pawn.getId(),
                pawn.getRank(),
                pawn.isDead(),
                attackedPawn.getRank(),
                attackedPawn.isDead(),
                tile.getTileId(),
                currentGamePlayer.getTeam(),
                currentGamePlayer.isDead(),
                attackedGamePlayer.getTeam()
        ));
    }
}

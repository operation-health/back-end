package be.kdg.strategobackend.streaming.event.actions.game;

import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GamePlayerReadyChanged implements Payload {
    private Team team;
    private boolean ready;
}

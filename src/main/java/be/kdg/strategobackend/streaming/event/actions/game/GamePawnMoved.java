package be.kdg.strategobackend.streaming.event.actions.game;

import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GamePawnMoved implements Payload {
    private long pawnId;
    private int tileId;
    private Team currentTeam;
    private Team nextTeam;
}

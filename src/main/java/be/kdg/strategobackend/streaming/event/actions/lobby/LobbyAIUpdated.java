package be.kdg.strategobackend.streaming.event.actions.lobby;

import be.kdg.strategobackend.rest.dto.game.GamePlayerDTO;
import be.kdg.strategobackend.streaming.event.Payload;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@Getter
@Setter
public class LobbyAIUpdated implements Payload {
    GamePlayerDTO gamePlayer;
}

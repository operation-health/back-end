package be.kdg.strategobackend.streaming.event;

public class ActionNotFoundException extends RuntimeException {

    public ActionNotFoundException(String message) {
        super(message);
    }
}

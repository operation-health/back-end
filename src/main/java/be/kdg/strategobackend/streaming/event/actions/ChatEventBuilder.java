package be.kdg.strategobackend.streaming.event.actions;

import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.chat.ChatGameMessageReceived;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ChatEventBuilder {

    public Event createChatGameMessageReceived(String playerName, LocalDateTime localDateTime, String message) {
        return new Event(new ChatGameMessageReceived(
                playerName,
                localDateTime,
                message
        ));
    }

    public Event createChatPrivateMessageReceived(String playerName, LocalDateTime localDateTime, String message) {
        return new Event(new ChatGameMessageReceived(
                playerName,
                localDateTime,
                message
        ));
    }

}

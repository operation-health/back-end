package be.kdg.strategobackend.streaming.event.actions.friend;

import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FriendRequest implements Payload {

    private String playerName;

}

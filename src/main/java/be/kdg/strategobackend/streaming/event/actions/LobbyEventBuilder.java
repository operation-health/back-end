package be.kdg.strategobackend.streaming.event.actions;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.rest.dto.game.GamePlayerDTO;
import be.kdg.strategobackend.rest.dto.lobby.LobbyBoardDTO;
import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.lobby.*;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class LobbyEventBuilder {

    private final ModelMapper modelMapper;

    public LobbyEventBuilder(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Event createLobbyPlayerJoined(GamePlayer gamePlayer) {
        return new Event(new LobbyPlayerJoined(
                modelMapper.map(gamePlayer, GamePlayerDTO.class)
        ));
    }

    public Event createLobbyAiJoined(GamePlayer gamePlayer) {
        return new Event(new LobbyAIJoined(
                modelMapper.map(gamePlayer, GamePlayerDTO.class)
        ));
    }

    public Event createLobbyPlayerLeft(String playerName) {
        return new Event(LobbyPlayerLeft.builder()
                .playerName(playerName)
                .build()
        );
    }

    public Event createLobbyHostLeft(String hostPlayerName, String newHostPlayerName) {
        return new Event(LobbyHostLeft.builder()
                .hostPlayerName(hostPlayerName)
                .newHostPlayerName(newHostPlayerName)
                .build()
        );
    }

    public Event createLobbyAiLeft(Team team) {
        return new Event(LobbyAILeft.builder()
                .team(team)
                .build()
        );
    }

    public Event createLobbyPlayerKicked(String playerName) {
        return new Event(LobbyPlayerKicked.builder()
                .playerName(playerName)
                .build()
        );
    }

    public Event createLobbyPlayerReadyChanged(String playerName, boolean ready) {
        return new Event(LobbyPlayerReadyChanged.builder()
                .playerName(playerName)
                .ready(ready)
                .build()
        );
    }

    public Event createLobbyBoardChanged(Board board) {
        return new Event(LobbyBoardChanged.builder()
                .board(modelMapper.map(board, LobbyBoardDTO.class))
                .build()
        );
    }

    public Event createLobbyAccessChanged(boolean access) {
        return new Event(LobbyAccessChanged.builder()
                .access(access)
                .build()
        );
    }

    public Event createLobbyPrepTimerChanged(long preparationTimer) {
        return new Event(LobbyPrepTimerChanged.builder()
                .preparationTimer(preparationTimer)
                .build()
        );
    }

    public Event createLobbyTurnTimerChanged(long turnTimer) {
        return new Event(LobbyTurnTimerChanged.builder()
                .turnTimer(turnTimer)
                .build()
        );
    }

    public Event createLobbyAfkTimerChanged(long afkTimer) {
        return new Event(LobbyAfkTimerChanged.builder()
                .afkTimer(afkTimer)
                .build()
        );
    }

    public Event createLobbyGameStarted() {
        return new Event(LobbyGameStarted.builder()
                .build()
        );
    }

    public Event createLobbyAiUpdated(GamePlayer gamePlayer) {
        return new Event(new LobbyAIUpdated(modelMapper.map(gamePlayer, GamePlayerDTO.class)));
    }
}

package be.kdg.strategobackend.streaming.event.actions;

import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.friend.FriendRemoved;
import be.kdg.strategobackend.streaming.event.actions.friend.FriendRequest;
import be.kdg.strategobackend.streaming.event.actions.friend.FriendRequestAccepted;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FriendEventBuilder {
    private final ModelMapper modelMapper;

    @Autowired
    public FriendEventBuilder(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Event createFriendRequest(String playerName) {
        return new Event(new FriendRequest(
                playerName
        ));
    }

    public Event createFriendRequestAccepted(String playerName) {
        return new Event(new FriendRequestAccepted(
                playerName
        ));
    }

    public Event createFriendRemoved(String playerName) {
        return new Event(new FriendRemoved(
                playerName
        ));
    }
}

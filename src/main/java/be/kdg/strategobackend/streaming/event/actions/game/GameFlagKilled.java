package be.kdg.strategobackend.streaming.event.actions.game;

import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.model.pawn.Rank;
import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameFlagKilled implements Payload {
    private long movedPawnId;
    private Rank movedPawnRank;
    private boolean movedPawnDead;
    private Rank attackedPawnRank;
    private boolean attackedPawnDead;
    private int tileId;
    private Team currentTeam;
    private Team attackedTeam;
    private Team nextTeam;
}

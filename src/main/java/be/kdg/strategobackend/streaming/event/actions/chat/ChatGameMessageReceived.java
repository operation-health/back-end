package be.kdg.strategobackend.streaming.event.actions.chat;

import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class ChatGameMessageReceived implements Payload {

    private String playerName;
    private LocalDateTime date;
    private String message;

}

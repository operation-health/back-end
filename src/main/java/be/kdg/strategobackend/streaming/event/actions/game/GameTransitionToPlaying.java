package be.kdg.strategobackend.streaming.event.actions.game;

import be.kdg.strategobackend.rest.dto.game.GameDTO;
import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameTransitionToPlaying implements Payload {
    private GameDTO game;
}

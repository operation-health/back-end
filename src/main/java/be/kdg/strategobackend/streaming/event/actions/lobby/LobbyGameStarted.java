package be.kdg.strategobackend.streaming.event.actions.lobby;

import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class LobbyGameStarted implements Payload {
    private boolean empty = false;
}

package be.kdg.strategobackend.streaming.event;

import lombok.Data;

@Data
public class Event {
    private Action action;
    private Payload payload;

    public Event(Payload payload) {
        this.payload = payload;
        this.action = Action.getAction(payload.getClass());
    }
}

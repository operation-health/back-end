package be.kdg.strategobackend.streaming.event.actions.game;

import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GamePawnOrganised implements Payload {
    private long pawnId;
    private int tileId;
}

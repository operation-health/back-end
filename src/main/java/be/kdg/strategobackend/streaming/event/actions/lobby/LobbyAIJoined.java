package be.kdg.strategobackend.streaming.event.actions.lobby;

import be.kdg.strategobackend.rest.dto.game.GamePlayerDTO;
import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LobbyAIJoined implements Payload {
    private GamePlayerDTO gamePlayer;
}

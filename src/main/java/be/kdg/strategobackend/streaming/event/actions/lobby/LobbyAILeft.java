package be.kdg.strategobackend.streaming.event.actions.lobby;

import be.kdg.strategobackend.model.gameplayer.Team;
import be.kdg.strategobackend.streaming.event.Payload;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LobbyAILeft implements Payload {
    private Team team;
}

package be.kdg.strategobackend.streaming.event;

import be.kdg.strategobackend.streaming.event.actions.chat.ChatGameMessageReceived;
import be.kdg.strategobackend.streaming.event.actions.chat.ChatPrivateMessageReceived;
import be.kdg.strategobackend.streaming.event.actions.friend.FriendRemoved;
import be.kdg.strategobackend.streaming.event.actions.friend.FriendRequest;
import be.kdg.strategobackend.streaming.event.actions.friend.FriendRequestAccepted;
import be.kdg.strategobackend.streaming.event.actions.game.*;
import be.kdg.strategobackend.streaming.event.actions.lobby.*;
import lombok.AllArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
public enum Action {

    /* FRIEND */
    FRIEND_REQUEST(FriendRequest.class),
    FRIEND_REQUEST_ACCEPTED(FriendRequestAccepted.class),
    FRIEND_REMOVED(FriendRemoved.class),

    /* CHAT */
    CHAT_GAME_MESSAGE_RECEIVED(ChatGameMessageReceived.class),
    CHAT_PRIVATE_MESSAGE_RECEIVED(ChatPrivateMessageReceived.class),

    /* LOBBY */
    LOBBY_PLAYER_JOINED(LobbyPlayerJoined.class),
    LOBBY_PLAYER_LEFT(LobbyPlayerLeft.class),
    LOBBY_HOST_LEFT(LobbyHostLeft.class),
    LOBBY_PLAYER_KICKED(LobbyPlayerKicked.class),
    LOBBY_PLAYER_READY_CHANGED(LobbyPlayerReadyChanged.class),
    LOBBY_AI_JOINED(LobbyAIJoined.class),
    LOBBY_AI_LEFT(LobbyAILeft.class),
    LOBBY_AI_UPDATED(LobbyAIUpdated.class),

    LOBBY_BOARD_CHANGED(LobbyBoardChanged.class),
    LOBBY_ACCESS_CHANGED(LobbyAccessChanged.class),
    LOBBY_PREP_TIMER_CHANGED(LobbyPrepTimerChanged.class),
    LOBBY_TURN_TIMER_CHANGED(LobbyTurnTimerChanged.class),
    LOBBY_AFK_TIMER_CHANGED(LobbyAfkTimerChanged.class),

    LOBBY_GAME_STARTED(LobbyGameStarted.class),

    /* GAME */
    GAME_TRANSITION_TO_PLAYING(GameTransitionToPlaying.class),
    GAME_PAWN_ORGANISED(GamePawnOrganised.class),
    GAME_PLAYER_READY_CHANGED(GamePlayerReadyChanged.class),
    GAME_PLAYER_AFK(GamePlayerAfk.class),
    GAME_PAWN_MOVED(GamePawnMoved.class),
    GAME_PAWN_ATTACKED(GamePawnAttacked.class),
    GAME_DOUBLE_ELIMINATION(GameDoubleElimination.class),
    GAME_FLAG_KILLED(GameFlagKilled.class),
    GAME_HUMANS_ELIMINATED(GameHumansEliminated.class),
    GAME_PASSIVE_WIN(GamePassiveWin.class),
    GAME_FINAL_BLOW(GameFinalBlow.class),
    GAME_ATTRITION_KILL(GameAttritionKill.class),
    GAME_ATTRITION_VICTORY(GameAttritionVictory.class);

    private Class<? extends Payload> clazz;

    public static Action getAction(Class<? extends Payload> clazz) {
        return Arrays.stream(values()).filter(a -> a.clazz == clazz)
                .findFirst()
                .orElseThrow(() -> new ActionNotFoundException("No Action found for class " + clazz));
    }
}

package be.kdg.strategobackend.streaming;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.game.GameState;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.notification.NotificationSender;
import be.kdg.strategobackend.service.GameService;
import be.kdg.strategobackend.service.GameTurnService;
import be.kdg.strategobackend.service.exception.*;
import be.kdg.strategobackend.service.manager.gamelogic.GameTurnTransition;
import be.kdg.strategobackend.service.manager.gamelogic.TurnManager;
import be.kdg.strategobackend.streaming.event.Action;
import be.kdg.strategobackend.streaming.event.Event;
import be.kdg.strategobackend.streaming.event.actions.GameEventBuilder;
import be.kdg.strategobackend.utility.SseSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@Transactional(isolation = Isolation.READ_UNCOMMITTED, propagation = Propagation.REQUIRES_NEW)
public class GameTimer {
    private static final Logger LOGGER = LoggerFactory.getLogger(GameTimer.class);

    private final GameService gameService;
    private final TurnManager turnManager;
    private final SseProcessor sseProcessor;
    private final SseSupport sseSupport;
    private final GameEventBuilder gameEventBuilder;
    private final GameTurnService gameTurnService;
    private final ScheduledExecutorService scheduler;
    private final ApplicationContext applicationContext;
    private final List<NotificationSender> notificationSenders;

    private final Map<Long, Future> futures = new HashMap<>();

    @Autowired
    public GameTimer(GameService gameService, SseProcessor sseProcessor, GameEventBuilder gameEventBuilder, TurnManager turnManager, SseSupport sseSupport, GameTurnService gameTurnService, ScheduledExecutorService scheduler, ApplicationContext applicationContext, List<NotificationSender> notificationSenders) {
        this.gameService = gameService;
        this.sseProcessor = sseProcessor;
        this.gameEventBuilder = gameEventBuilder;
        this.turnManager = turnManager;
        this.sseSupport = sseSupport;
        this.gameTurnService = gameTurnService;
        this.scheduler = scheduler;
        this.applicationContext = applicationContext;
        this.notificationSenders = notificationSenders;
    }

    @Scheduled(fixedDelayString = "${game.preparation.check.timer}")
    public void checkPreparationGames() {
        gameService.loadPreparing().stream()
                .filter(game -> Duration.between(game.getStartTime(), LocalDateTime.now()).getSeconds() >= game.getGameRules().getPreparationTimer())
                .forEach(game -> {
                    try {
                        game = gameService.startPlayingPhase(game);
                        this.startTurn(game);
                        sseSupport.sendPlayingPhasePackets(game);
                    } catch (GameException | GameTurnException e) {
                        LOGGER.error(e.getMessage());
                    }
                });
    }

    public void startTurnWithNewTransaction(long gameId) throws GameTurnException, GameException {
        applicationContext.getBean(this.getClass()).startTurn(gameId);
    }

    public void startTurn(long gameId) throws GameTurnException, GameException {
        this.startTurn(gameService.load(gameId));
    }

    public void startTurn(Game game) throws GameTurnException {
        Optional<GamePlayer> currentGamePlayerOptional = game.getGamePlayers().stream().filter(GamePlayer::isCurrentTurn).findFirst();
        GamePlayer currentGamePlayer = currentGamePlayerOptional.orElseThrow(() -> new GameTurnException("No game players were found with a turn while starting a turn."));

        this.notificationSenders.forEach(notificationSender -> notificationSender.sendTurnAlert(currentGamePlayer, game));

        Future future = futures.get(game.getId());
        if (future != null) future.cancel(false);
        if (game.getGameState() == GameState.FINISHED) futures.remove(game.getId());
        else {
            if (currentGamePlayer.isComputer()) {
                future = scheduler.schedule(() -> {
                    try {
                        this.startAiTurn(currentGamePlayer);
                    } catch (Exception e) {
                        LOGGER.error(e.getMessage());
                        e.printStackTrace();
                    }
                }, 1, TimeUnit.MILLISECONDS);
                futures.put(game.getId(), future);
            } else {
                future = scheduler.schedule(() -> {
                    try {
                        this.startRegularTurn(game);
                    } catch (GameTurnException | GameException e) {
                        LOGGER.error(e.getMessage());
                    }
                }, currentGamePlayer.isAfk() ? game.getGameRules().getAfkTimer() : game.getGameRules().getTurnTimer(), TimeUnit.SECONDS);
                futures.put(game.getId(), future);
            }
        }
    }

    private void startRegularTurn(Game game) throws GameTurnException, GameException {
        GameTurnTransition gameTurnTransition = turnManager.endTurn(game, true);

        this.notificationSenders.forEach(notificationSender -> notificationSender.sendTurnExpiredAlert(gameTurnTransition.getGameTurn().getGamePlayer(), game));

        gameTurnService.save(gameTurnTransition.getGameTurn());
        Event event = gameEventBuilder.createGamePlayerAfk(gameTurnTransition.getGameTurn().getGamePlayer(), gameTurnTransition.getNextPlayer());
        sseProcessor.push(game, event);
        this.startTurnWithNewTransaction(game.getId());
    }

    private void startAiTurn(GamePlayer currentGamePlayer) throws GameTurnException, InvalidGameStateException, BoardTileException, GameException, PawnException, InvalidBoardTileException, InvalidPawnException {
        GameTurn gameTurn = gameService.movePawnWithAI(currentGamePlayer);
        Game game = this.gameService.load(currentGamePlayer.getGame().getId());
        GameTurnTransition gameTurnTransition = gameTurnService.endTurn(game, gameTurn);
        Event event = sseSupport.getGameEvent(game, gameTurnTransition);

        this.notificationSenders.forEach(notificationSender -> notificationSender.sendTurnPlayedAlert(gameTurnTransition.getGameTurn().getGamePlayer(), game));

        sseProcessor.push(game, event);
        if (event.getAction() == Action.GAME_FINAL_BLOW || event.getAction() == Action.GAME_PASSIVE_WIN || event.getAction() == Action.GAME_HUMANS_ELIMINATED || event.getAction() == Action.GAME_ATTRITION_VICTORY) {
            this.endTurnSystem(game);
            gameService.endGame(game);
        } else {
            this.startTurnWithNewTransaction(game.getId());
        }
    }

    public void endTurnSystem(Game game) {
        Future future = futures.get(game.getId());
        if (future != null) future.cancel(false);
        if (game.getGameState() == GameState.FINISHED) futures.remove(game.getId());
    }

}

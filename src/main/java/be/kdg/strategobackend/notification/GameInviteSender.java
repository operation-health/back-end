package be.kdg.strategobackend.notification;

import be.kdg.strategobackend.model.player.Player;

import java.util.UUID;

public interface GameInviteSender{
    void sendGameInvite(Player playerToInvite, String host, UUID inviteUUID);
}

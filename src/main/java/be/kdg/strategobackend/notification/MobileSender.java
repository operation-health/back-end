package be.kdg.strategobackend.notification;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.player.Player;
import be.kdg.strategobackend.notification.firebase.FirebaseCloudMessagingSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class MobileSender implements NotificationSender, GameInviteSender {
    private final FirebaseCloudMessagingSender firebaseCloudMessagingSender;

    public MobileSender(FirebaseCloudMessagingSender firebaseCloudMessagingSender) {
        this.firebaseCloudMessagingSender = firebaseCloudMessagingSender;
    }

    @Override
    public void sendGameInvite(Player playerToInvite, String host, UUID inviteUUID) {
        if (playerToInvite.getNotificationSettings().isGameInvite()) {
            String body = host + " has invited you to a game. Click here to join his game.";
            firebaseCloudMessagingSender.sendNotification(playerToInvite.getFcmId(), body, "lobby/invite/" + inviteUUID, false);
        }
    }

    @Override
    public void sendTurnPlayedAlert(GamePlayer currentGamePlayer, Game game) {
        game.getGamePlayers().stream()
                .filter(gamePlayer -> !gamePlayer.isComputer())
                .filter(gamePlayer -> gamePlayer.getPlayer().getNotificationSettings().isTurnPlayed())
                .forEach(gamePlayer -> {
                    if (gamePlayer.getPlayer().getFcmId() != null) {
                        String body = currentGamePlayer.isComputer() ? "The AI" : currentGamePlayer.getPlayer().getPlayerName();
                        body += " has just finished their turn. Click here to reconnect to the game.";
                        firebaseCloudMessagingSender.sendNotification(gamePlayer.getPlayer().getFcmId(), body, "game", true);
                    }
                });
    }

    @Override
    public void sendTurnAlert(GamePlayer gamePlayer, Game game) {
        if (!gamePlayer.isComputer() && gamePlayer.getPlayer().getFcmId() != null && gamePlayer.getPlayer().getNotificationSettings().isYourTurn()) {
            String body = "It's your turn. Don't keep the other players waiting. Click here to reconnect to the game.";
            firebaseCloudMessagingSender.sendNotification(gamePlayer.getPlayer().getFcmId(), body, "game", true);
        }
    }

    @Override
    public void sendTurnExpiredAlert(GamePlayer gamePlayer, Game game) {
        if (!gamePlayer.isComputer() && gamePlayer.getPlayer().getFcmId() != null && gamePlayer.getPlayer().getNotificationSettings().isTurnExpired()) {
            String body = "Your turn has expired. Don't keep the other players waiting. Click here to reconnect to the game.";
            firebaseCloudMessagingSender.sendNotification(gamePlayer.getPlayer().getFcmId(), body, "game", true);
        }
    }
}

package be.kdg.strategobackend.notification;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;

public interface NotificationSender {
    void sendTurnPlayedAlert(GamePlayer currentGamePlayer, Game game);

    void sendTurnAlert(GamePlayer currentGamePlayer, Game game);

    void sendTurnExpiredAlert(GamePlayer currentGamePlayer, Game game);
}

package be.kdg.strategobackend.notification.firebase;

import be.kdg.strategobackend.notification.MobileSender;
import be.kdg.strategobackend.notification.firebase.dto.Notification;
import be.kdg.strategobackend.notification.firebase.dto.Push;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Component
public class FirebaseCloudMessagingSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(MobileSender.class);

    @Value("${notifications.firebase-api-key}")
    private String firebaseApiKey;
    @Value("${notifications.firebase-api-url}")
    private String firebaseApiUrl;

    @Async
    public void sendNotification(String fcmId, String body, String url, boolean isBackground) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "key=" + firebaseApiKey);
        headers.set("Content-Type", "application/json");

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("url", url);
        hashMap.put("background", String.valueOf(isBackground));

        Push push = new Push(fcmId, "high", hashMap,
                new Notification("default", "Stratego", body));
        HttpEntity<Push> request = new HttpEntity<>(push, headers);

        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.postForEntity(firebaseApiUrl, request, String.class);
        } catch (RestClientException ex) {
            LOGGER.error("The notification could not be sent.");
        }
    }

}

package be.kdg.strategobackend.notification.firebase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Push {
    private String to;
    private String priority;
    private Map<String, String> data;
    private Notification notification;
}


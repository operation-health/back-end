package be.kdg.strategobackend.notification;

import be.kdg.strategobackend.model.player.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class EmailSender implements GameInviteSender {
    private final JavaMailSender mailSender;

    @Value("${mail.service.from}")
    private String from;
    @Value("${mail.service.confirmationSubject}")
    private String confirmationSubject;
    @Value("${mail.service.confirmationMessage}")
    private String confirmationMessage;
    @Value("${mail.service.confirmationUrlFrontend}")
    private String confirmationUrl;
    @Value("${mail.service.passwordResetSubject}")
    private String passwordResetSubject;
    @Value("${mail.service.passwordResetMessage}")
    private String passwordResetMessage;
    @Value("${mail.service.passwordResetUrl}")
    private String passwordResetUrl;
    @Value("${mail.service.inviteSubject}")
    private String inviteSubject;
    @Value("${mail.service.inviteMessage}")
    private String inviteMessage;
    @Value("${mail.service.inviteUrl}")
    private String inviteUrl;

    @Autowired
    public EmailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    private void sendMessage(String to, String from, String subject, String text) throws MailException {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        message.setFrom(from);
        mailSender.send(message);
    }

    public void sendEmailConfirmation(String to, String token) {
        String url = this.confirmationUrl + token;
        sendMessage(to, from, confirmationSubject, confirmationMessage + " " + url);
    }

    public void sendPasswordReset(String to, String token) {
        String url = passwordResetUrl + token;
        sendMessage(to, from, passwordResetSubject, passwordResetMessage + " " + url);
    }

    @Override
    public void sendGameInvite(Player player, String host, UUID inviteUUID) {
        String message = inviteMessage.replace("{playerName}", host);
        String url = inviteUrl + inviteUUID;
        sendMessage(player.getEmail(), from, inviteSubject, message + " " + url);
    }
}

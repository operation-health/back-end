package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.game.GameState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GameRepository extends JpaRepository<Game, Long> {
    @Query("SELECT g FROM Game g WHERE g.inviteUUID = ?1 AND g.gameState = 'LOBBY'")
    Optional<Game> findLobbyByInviteUUID(UUID inviteUUID);

    @Query("SELECT g FROM Game g LEFT OUTER JOIN g.gamePlayers gp ON g.id = gp.game.id WHERE gp.player.id = ?1 AND g.gameState <> 'FINISHED' AND gp.dead = FALSE")
    Optional<Game> findCurrentGame(long playerId);

    @Query("SELECT g FROM Game g JOIN g.gamePlayers gp ON g.id = gp.game.id JOIN gp.player p ON gp.player.id = p.id WHERE p.playerName = ?1 AND g.gameState = 'FINISHED'")
    List<Game> findGamesByPlayerName(String playerName);

    @Query("SELECT g FROM Game g LEFT OUTER JOIN g.gamePlayers gp ON g.id = gp.game.id JOIN gp.player p ON gp.player.id = p.id WHERE p.playerName = ?1 AND g.gameState <> 'FINISHED'")
    Optional<Game> findCurrentGameByPlayerName(String playerName);

    @Query("SELECT g FROM Game g WHERE g.gameState = ?1 AND g.publicAccess = TRUE")
    List<Game> findGamesByGameStateAndPublicAccessTrue(GameState gameState);

    @Query("SELECT g FROM Game g WHERE g.gameState = ?1")
    List<Game> findGamesByGameState(GameState gameState);

    @Query("SELECT g FROM Game g WHERE g.gameState <> ?1")
    List<Game> findGamesByNotGameState(GameState gameState);
}

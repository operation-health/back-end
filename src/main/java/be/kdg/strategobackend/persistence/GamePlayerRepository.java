package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.gameplayer.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface GamePlayerRepository extends JpaRepository<GamePlayer, Long> {
    @Query("SELECT gp FROM GamePlayer gp WHERE gp.player.playerName = ?1 AND gp.game.gameState <> 'FINISHED'")
    Optional<GamePlayer> findGamePlayerByName(String playerName);

    @Query("SELECT gp FROM GamePlayer gp WHERE gp.game.id = ?1 AND gp.team = ?2")
    Optional<GamePlayer> findGamePlayerByGameIdAndTeam(long gameId, Team team);
}

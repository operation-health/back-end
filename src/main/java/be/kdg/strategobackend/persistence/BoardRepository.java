package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.board.Board;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BoardRepository extends JpaRepository<Board, Long> {
    Optional<Board> findByType(String boardType);
}

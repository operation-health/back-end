package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.board.BoardTile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface BoardTileRepository extends JpaRepository<BoardTile, Long> {
    @Query("SELECT bt FROM BoardTile bt JOIN bt.board b ON bt.board.id = b.id WHERE bt.board.id = ?1 AND bt.tileId = ?2")
    Optional<BoardTile> findByBoardIdAndTileId(long boardId, int tileId);
}

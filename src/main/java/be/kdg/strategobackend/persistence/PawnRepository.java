package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.pawn.Pawn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PawnRepository extends JpaRepository<Pawn, Long> {
    @Query("SELECT p FROM Pawn p " +
            "JOIN p.boardTile bt ON bt.id = p.boardTile.id " +
            "JOIN bt.board b ON b.id = bt.board.id " +
            "JOIN p.gamePlayer gp ON gp.id = p.gamePlayer.id " +
            "JOIN gp.game g ON g.id = gp.game.id " +
            "WHERE b.id = ?1 AND bt.tileId = ?2 AND g.id = ?3")
    Optional<Pawn> findByBoardIdAndBoardTileIdAndGameId(long boardId, int tileId, long gameId);
}

package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.game.GameRules;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRulesRepository extends JpaRepository<GameRules, Long> {
}

package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.game.InitialState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InitialStateRepository extends JpaRepository<InitialState, Long> {
}

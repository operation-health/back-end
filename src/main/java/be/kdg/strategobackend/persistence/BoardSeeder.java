package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.service.BoardService;
import be.kdg.strategobackend.service.BoardTileService;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class BoardSeeder {
    private static final Logger LOGGER = LoggerFactory.getLogger(BoardSeeder.class);

    private final ResourceLoader resourceLoader;
    private final Gson gson;
    private final BoardService boardService;
    private final BoardTileService boardTileService;

    @Value("${board.files.path}")
    private String filePath;

    @Autowired
    public BoardSeeder(ResourceLoader resourceLoader, Gson gson, BoardService boardService, BoardTileService boardTileService) {
        this.resourceLoader = resourceLoader;
        this.gson = gson;
        this.boardService = boardService;
        this.boardTileService = boardTileService;
    }

    @EventListener
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void seed(@SuppressWarnings("unused") ContextRefreshedEvent event) throws IOException {
        Resource[] boards = loadResources(filePath);
        List<String> installedBoardTypes = boardService.loadBoards().stream().map(Board::getType).collect(Collectors.toList());
        LOGGER.info("Installed Boards: {}", String.join(",", installedBoardTypes));

        for (Resource resource : boards) {
            Board board = gson.fromJson(streamToString(resource.getInputStream()), Board.class);
            if (installedBoardTypes.contains(board.getType())) continue;

            LOGGER.info("Registering new Board: " + board.getType());
            installedBoardTypes.add(board.getType());
            board = boardService.save(board);
            for (BoardTile boardTile : board.getBoardTiles()) boardTile.setBoard(board);
            boardTileService.saveAll(board.getBoardTiles());
        }
    }

    private Resource[] loadResources(String pattern) throws IOException {
        return ResourcePatternUtils.getResourcePatternResolver(resourceLoader).getResources(pattern);
    }

    private static String streamToString(InputStream stream) {
        String data;
        try {
            StringBuilder stringBuilder = new StringBuilder();
            int i;
            while ((i = stream.read()) != -1) stringBuilder.append((char) i);
            data = stringBuilder.toString();
        } catch (Exception e) {
            data = "No data streamed.";
        }
        return data;
    }
}

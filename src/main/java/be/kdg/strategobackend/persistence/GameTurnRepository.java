package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.history.GameTurn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameTurnRepository extends JpaRepository<GameTurn, Long> {
}

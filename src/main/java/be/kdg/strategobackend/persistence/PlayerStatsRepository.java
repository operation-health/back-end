package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.player.PlayerStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PlayerStatsRepository extends JpaRepository<PlayerStats, Long> {
    @Query("SELECT ps FROM PlayerStats ps WHERE ps.player.playerName = ?1")
    Optional<PlayerStats> findPlayerStatsByPlayerName(String playerName);
}

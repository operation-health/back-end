package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.player.NotificationSettings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationSettingsRepository extends JpaRepository<NotificationSettings, Long> {
}

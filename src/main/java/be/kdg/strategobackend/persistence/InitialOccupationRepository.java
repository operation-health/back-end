package be.kdg.strategobackend.persistence;


import be.kdg.strategobackend.model.game.InitialOccupation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InitialOccupationRepository extends JpaRepository<InitialOccupation, Long> {
}

package be.kdg.strategobackend.persistence;

import be.kdg.strategobackend.model.player.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public interface PlayerRepository extends JpaRepository<Player, Long> {
    @Query("SELECT p FROM Player p WHERE p.playerName = ?1 OR p.email = ?1")
    Optional<Player> findByPlayerNameOrEmail(String playerNameOrEmail);

    Optional<Player> findByPlayerName(String playerName);

    boolean existsByPlayerName(String playerName);

    boolean existsByEmail(String email);

    @Query("SELECT p FROM Player p WHERE p.playerName LIKE %?1%")
    List<Player> findPlayersLikePlayerName(String playerName);
}

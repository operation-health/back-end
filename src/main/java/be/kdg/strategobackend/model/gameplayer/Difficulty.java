package be.kdg.strategobackend.model.gameplayer;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Difficulty {
    EASY(3), MEDIUM(5), HARD(8), MASTER(10);

    private int difficulty;

    public int getDifficulty() {
        return difficulty;
    }

    public Difficulty getNext() {
        return this.ordinal() < Difficulty.values().length - 1
                ? Difficulty.values()[this.ordinal() + 1]
                : Difficulty.values()[0];
    }

    public Difficulty getPrevious() {
        return this.ordinal() > 0
                ? Difficulty.values()[this.ordinal() - 1]
                : Difficulty.values()[Difficulty.values().length - 1];
    }
}

package be.kdg.strategobackend.model.gameplayer;

import be.kdg.strategobackend.model.game.Game;
import be.kdg.strategobackend.model.history.GameTurn;
import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.model.player.Player;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@ToString(exclude = {"game", "player", "pawns", "gameTurns"})
@EqualsAndHashCode(exclude = {"pawns", "gameTurns"})
@NoArgsConstructor
@AllArgsConstructor
public class GamePlayer {
    @Id
    @GeneratedValue
    private long id;
    @Enumerated(EnumType.STRING)
    private Team team;
    private boolean ready;
    private boolean dead;
    private boolean afk;
    private boolean host;
    private boolean computer;
    private boolean currentTurn;
    private LocalDateTime turnStartTime;
    @Enumerated(EnumType.STRING)
    private Difficulty difficulty;

    @ManyToOne
    private Game game;
    @ManyToOne
    private Player player;

    @OneToMany(mappedBy = "gamePlayer", cascade = CascadeType.REMOVE)
    private List<Pawn> pawns;
    @OneToMany(mappedBy = "gamePlayer", cascade = CascadeType.REMOVE)
    private List<GameTurn> gameTurns;

    public GamePlayer(Team team, Player player, boolean host) {
        this.team = team;
        this.host = host;
        this.player = player;
    }

    public GamePlayer(Team team, Player player, Game game, boolean host) {
        this.team = team;
        this.host = host;
        this.game = game;
        this.player = player;
    }

    public GamePlayer(Team team, Game game) {
        this.team = team;
        this.computer = true;
        this.game = game;
        difficulty = Difficulty.EASY;
        ready = true;
    }
}

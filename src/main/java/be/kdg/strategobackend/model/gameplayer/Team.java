package be.kdg.strategobackend.model.gameplayer;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.model.game.Game;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@AllArgsConstructor
public enum Team {
    TEAM1(1), TEAM2(2), TEAM3(3), TEAM4(4);

    private int team;

    public static Team randomTeam(Board board) {
        return Team.values()[new Random().nextInt(board.getPlayerCount())];
    }

    public static Team randomTeam(Game game) {
        List<Team> teams = Arrays.stream(Team.values()).filter(team -> team.getTeam() <= game.getBoard().getPlayerCount()).collect(Collectors.toList());
        teams.removeAll(game.getGamePlayers().stream().map(GamePlayer::getTeam).collect(Collectors.toList()));
        return teams.get(new Random().nextInt(teams.size()));
    }

    public static Team getTeam(int team) {
        for (Team t : values()) {
            if (t.team == team) return t;
        }
        return null;
    }

    public int getTeam() {
        return team;
    }
}

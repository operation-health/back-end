package be.kdg.strategobackend.model.player;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@Entity
@Builder
@ToString(exclude = {"player"})
@NoArgsConstructor
@AllArgsConstructor
public class PlayerStats {
    @Id
    @GeneratedValue
    private long id;
    private int gamesPlayed;
    private int gamesWon;
    private boolean publicAccess = true;

    @OneToOne(mappedBy = "playerStats")
    private Player player;
}

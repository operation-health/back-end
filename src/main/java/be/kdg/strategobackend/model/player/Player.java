package be.kdg.strategobackend.model.player;

import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder
@ToString(exclude = {"playerStats", "friends", "gamePlayers"})
@EqualsAndHashCode(exclude = {"friends", "playerStats", "gamePlayers", "notificationSettings"})
@NoArgsConstructor
@AllArgsConstructor

public class Player {
    @Id
    @GeneratedValue
    private long id;
    @Column(unique = true)
    @Email
    private String email;
    @Column(unique = true)
    @Pattern(regexp = "^((?!@).)*$")
    private String playerName;
    private String password;
    private String profilePicture;
    private boolean emailConfirmed;
    private boolean hasPassword;
    private String fcmId;

    @OneToOne(optional = false)
    private PlayerStats playerStats;

    @OneToOne(optional = false)
    private NotificationSettings notificationSettings;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Player> friends;
    @OneToMany(mappedBy = "player")
    private Set<GamePlayer> gamePlayers;

    public Player(@Email String email, @Pattern(regexp = "^((?!@).)*$") String playerName, String password) {
        this.email = email;
        this.playerName = playerName;
        this.password = password;
        profilePicture = "flag.png";
    }
}

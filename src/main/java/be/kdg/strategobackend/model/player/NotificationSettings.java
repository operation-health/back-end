package be.kdg.strategobackend.model.player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class NotificationSettings {
    @Id
    @GeneratedValue
    private long id;

    private boolean gameInvite;
    private boolean message;
    private boolean turnPlayed;
    private boolean yourTurn;
    private boolean turnExpired;
}

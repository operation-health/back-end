package be.kdg.strategobackend.model.game;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@ToString(exclude = {"board", "gameRules", "initialState", "gamePlayers"})
@NoArgsConstructor
@AllArgsConstructor
public class Game {
    @Id
    @GeneratedValue
    private long id;
    private UUID inviteUUID;
    private boolean publicAccess;
    private LocalDateTime startTime;
    @Enumerated(EnumType.STRING)
    private GameState gameState;

    @OneToOne
    private Board board;
    @OneToOne(cascade = CascadeType.ALL, optional = false)
    private GameRules gameRules;
    @OneToOne(cascade = CascadeType.ALL)
    private InitialState initialState;

    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
    private List<GamePlayer> gamePlayers;

    public Game(UUID inviteUUID, boolean publicAccess, GameState gameState, Board board, GameRules gameRules, List<GamePlayer> gamePlayers) {
        this.inviteUUID = inviteUUID;
        this.publicAccess = publicAccess;
        this.gameState = gameState;
        this.board = board;
        this.gameRules = gameRules;
        this.gamePlayers = gamePlayers;
    }
}

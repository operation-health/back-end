package be.kdg.strategobackend.model.game;

public enum GameState {
    LOBBY, PREPARING, PLAYING, FINISHED
}

package be.kdg.strategobackend.model.game;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@Entity
@ToString(exclude = {"game"})
@NoArgsConstructor
@AllArgsConstructor
public class GameRules {
    @Id
    @GeneratedValue
    private long id;
    private long turnTimer;
    private long afkTimer;
    private long preparationTimer;

    @OneToOne(mappedBy = "gameRules")
    private Game game;

    public GameRules(long turnTimer, long afkTimer, long preparationTimer) {
        this.turnTimer = turnTimer;
        this.afkTimer = afkTimer;
        this.preparationTimer = preparationTimer;
    }
}

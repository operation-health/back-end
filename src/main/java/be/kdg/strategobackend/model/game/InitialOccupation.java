package be.kdg.strategobackend.model.game;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.pawn.Pawn;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@Entity
@ToString(exclude = {"boardTile", "pawn"})
@NoArgsConstructor
@AllArgsConstructor
public class InitialOccupation {
    @Id
    @GeneratedValue
    private long id;

    @OneToOne
    private Pawn pawn;
    @OneToOne
    private BoardTile boardTile;

    public InitialOccupation(Pawn pawn, BoardTile boardTile) {
        this.pawn = pawn;
        this.boardTile = boardTile;
    }
}

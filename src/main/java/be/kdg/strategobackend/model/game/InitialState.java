package be.kdg.strategobackend.model.game;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@ToString(exclude = {"game", "initialOccupations"})
@NoArgsConstructor
@AllArgsConstructor
public class InitialState {
    @Id
    @GeneratedValue
    private long id;

    public InitialState(Game game, List<InitialOccupation> initialOccupations) {
        this.game = game;
        this.initialOccupations = initialOccupations;
    }

    @OneToOne(mappedBy = "initialState")
    private Game game;

    @OneToMany(cascade = CascadeType.ALL)
    private List<InitialOccupation> initialOccupations;
}

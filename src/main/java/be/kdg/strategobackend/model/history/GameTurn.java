package be.kdg.strategobackend.model.history;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.pawn.Pawn;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Data
@Entity
@Builder
@ToString(exclude = {"movedPawn", "attackedPawn", "gamePlayer", "movedToTile", "movedFromTile"})
@NoArgsConstructor
@AllArgsConstructor
public class GameTurn implements Comparable<GameTurn> {
    @Id
    @GeneratedValue
    private long id;
    private LocalDateTime time;
    private boolean afkTurn;

    @ManyToOne
    private Pawn movedPawn;
    @ManyToOne
    private Pawn attackedPawn;
    @ManyToOne
    private GamePlayer gamePlayer;
    @ManyToOne
    private BoardTile movedToTile;
    @ManyToOne
    private BoardTile movedFromTile;

    public GameTurn(LocalDateTime time) {
        this.time = time;
    }

    @Override
    public int compareTo(GameTurn o) {
        return this.time.compareTo(o.time);
    }
}

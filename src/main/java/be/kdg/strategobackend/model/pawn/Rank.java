package be.kdg.strategobackend.model.pawn;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Rank {
    FLAG(0), SPY(1), SCOUT(2), MINER(3), SERGEANT(4), LIEUTENANT(5), CAPTAIN(6), MAJOR(7), COLONEL(8), GENERAL(9), MARSHAL(10), BOMB(11);

    private int rank;

    public int getRank() {
        return rank;
    }
}

package be.kdg.strategobackend.model.pawn;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.gameplayer.GamePlayer;
import be.kdg.strategobackend.model.history.GameTurn;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(exclude = {"boardTile", "gamePlayer", "attackedPawnGameTurns", "movedPawnGameTurns"})
@Entity
@Builder
@ToString(exclude = {"boardTile", "gamePlayer", "attackedPawnGameTurns", "movedPawnGameTurns"})
@NoArgsConstructor
@AllArgsConstructor
public class Pawn {
    @Id
    @GeneratedValue
    private long id;
    @Enumerated(EnumType.STRING)
    private Rank rank;
    private boolean dead;

    @OneToOne
    private BoardTile boardTile;
    @ManyToOne
    private GamePlayer gamePlayer;

    @OneToMany(mappedBy = "attackedPawn")
    private List<GameTurn> attackedPawnGameTurns;
    @OneToMany(mappedBy = "movedPawn")
    private List<GameTurn> movedPawnGameTurns;
}

package be.kdg.strategobackend.model.board;

import be.kdg.strategobackend.model.gameplayer.Team;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@ToString(exclude = {"board", "connectedTiles"})
@EqualsAndHashCode(exclude = {"team", "board", "connectedTiles"})
@NoArgsConstructor
@AllArgsConstructor
public class BoardTile {
    @Id
    @GeneratedValue
    private long id;
    private int tileId;
    @Enumerated(EnumType.STRING)
    private Team team;

    @ManyToOne(optional = false)
    private Board board;

    @ManyToMany()
    private List<BoardTile> connectedTiles;
}

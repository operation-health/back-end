package be.kdg.strategobackend.model.board;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Entity
@ToString(exclude = {"boardTiles"})
@NoArgsConstructor
@AllArgsConstructor
public class Board {
    @Id
    @GeneratedValue
    private long id;
    private String type;
    private int playerCount;
    private String imageUrl;

    @OneToMany(mappedBy = "board")
    private List<BoardTile> boardTiles;
}

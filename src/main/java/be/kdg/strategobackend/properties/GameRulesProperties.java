package be.kdg.strategobackend.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "game.default.game-rules")
public class GameRulesProperties {
    private long turnTimer;
    private long afkTimer;
    private long preparationTimer;
}

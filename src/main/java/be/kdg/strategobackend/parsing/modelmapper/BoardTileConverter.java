package be.kdg.strategobackend.parsing.modelmapper;

import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.rest.dto.shared.BoardTileDTO;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BoardTileConverter extends AbstractConverter<BoardTile, BoardTileDTO> {
    @Override
    protected BoardTileDTO convert(BoardTile source) {
        if (source == null) return null;

        BoardTileDTO boardTileDTO = new BoardTileDTO();
        boardTileDTO.setTileId(source.getTileId());
        List<Integer> connectedTiles = source.getConnectedTiles().stream().map(BoardTile::getTileId).collect(Collectors.toList());
        boardTileDTO.setConnectedTileIds(connectedTiles);

        return boardTileDTO;
    }
}

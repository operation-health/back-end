package be.kdg.strategobackend.parsing.modelmapper;

import be.kdg.strategobackend.model.pawn.Pawn;
import be.kdg.strategobackend.rest.dto.shared.PawnDTO;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class PawnConverter extends AbstractConverter<Pawn, PawnDTO> {

    private final BoardTileConverter boardTileConverter;

    public PawnConverter(BoardTileConverter boardTileConverter) {
        this.boardTileConverter = boardTileConverter;
    }

    @Override
    protected PawnDTO convert(Pawn source) {
        if (source == null) return null;

        return new PawnDTO(
                source.getId(),
                source.getRank(),
                source.isDead(),
                boardTileConverter.convert(source.getBoardTile())
        );
    }
}

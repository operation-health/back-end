package be.kdg.strategobackend.parsing;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.parsing.gson.BoardDeserializer;
import be.kdg.strategobackend.parsing.modelmapper.BoardTileConverter;
import be.kdg.strategobackend.parsing.modelmapper.PawnConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ParsingConfig {
    private final BoardDeserializer boardDeserializer;
    private final BoardTileConverter boardTileConverter;
    private final PawnConverter pawnConverter;

    @Autowired
    public ParsingConfig(BoardDeserializer boardDeserializer, BoardTileConverter boardTileConverter, PawnConverter pawnConverter) {
        this.boardDeserializer = boardDeserializer;
        this.boardTileConverter = boardTileConverter;
        this.pawnConverter = pawnConverter;
    }

    @Bean
    public Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(Board.class, boardDeserializer)
                .create();
    }

    @Bean
    public ModelMapper getModelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addConverter(boardTileConverter);
        modelMapper.addConverter(pawnConverter);
        return modelMapper;
    }
}

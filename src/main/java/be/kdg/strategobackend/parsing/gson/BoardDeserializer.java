package be.kdg.strategobackend.parsing.gson;

import be.kdg.strategobackend.model.board.Board;
import be.kdg.strategobackend.model.board.BoardTile;
import be.kdg.strategobackend.model.gameplayer.Team;
import com.google.gson.*;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BoardDeserializer implements JsonDeserializer<Board> {
    @Override
    public Board deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Map<Integer, BoardTile> map = new HashMap<>();

        for (JsonElement jsonBoardTileElement : jsonObject.getAsJsonArray("boardTiles")) {
            JsonObject jsonBoardTileObject = jsonBoardTileElement.getAsJsonObject();
            BoardTile boardTile = new BoardTile();
            int tileId = jsonBoardTileObject.get("tileId").getAsInt();
            boardTile.setTileId(tileId);

            if (jsonBoardTileObject.has("team"))
                boardTile.setTeam(Team.valueOf("TEAM" + jsonBoardTileObject.get("team").getAsInt()));

            map.put(tileId, boardTile);
        }

        for (JsonElement jsonBoardTileElement : jsonObject.getAsJsonArray("boardTiles")) {
            JsonObject jsonBoardTileObject = jsonBoardTileElement.getAsJsonObject();
            BoardTile boardTile = map.get(jsonBoardTileObject.get("tileId").getAsInt());
            List<BoardTile> connectedTiles = new ArrayList<>();

            for (JsonElement jsonConnectedElement : jsonBoardTileObject.getAsJsonArray("connectedTiles")) {
                int connectedTileId = jsonConnectedElement.getAsInt();
                connectedTiles.add(map.get(connectedTileId));
            }

            boardTile.setConnectedTiles(connectedTiles);
        }

        Board board = new Board();
        board.setType(jsonObject.get("type").getAsString());
        board.setPlayerCount(jsonObject.get("playerCount").getAsInt());
        board.setBoardTiles(new ArrayList<>(map.values()));

        return board;
    }
}

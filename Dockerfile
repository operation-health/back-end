FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD /build/libs/strategobackend-0.0.1-SNAPSHOT.jar app.jar
RUN adduser -D myuser
USER myuser
CMD java -Dserver.port=$PORT -Dspring.profiles.active=prod -jar app.jar